import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:flutter/foundation.dart';

import 'NavigateEvent.dart';

class SplashViewModel {
  final LocalStorage _localStorage;

  final ValueNotifier<NavigateEvent> navigationEvent = ValueNotifier(null);

  SplashViewModel(this._localStorage);

  void checkLogin() async {
    final accessToken = await _localStorage.getAccessToken();
    if (accessToken == null) {
      navigationEvent.value = NavigateEvent.LOGIN;
      return;
    }

    final userLocation = await _localStorage.getLocationLon();
    if (userLocation == null || userLocation == 0) {
      navigationEvent.value = NavigateEvent.LOCATION;
      return;
    }

    navigationEvent.value = NavigateEvent.MAIN;
  }
}
