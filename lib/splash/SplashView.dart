import 'dart:async';

import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/splash/NavigateEvent.dart';
import 'package:delivery_client/splash/SplashViewModel.dart';
import 'package:flutter/material.dart';

class SplashView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashViewState();
  }
}

class SplashViewState extends State<SplashView> {
  SplashViewModel _splashViewModel = SplashViewModel(getIt<LocalStorage>());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DecoratedBox(
          decoration: BoxDecoration(
              color: Color(0xffffa300),
              image: DecorationImage(image: AssetImage('assets/mops-02.png')))),
    );
  }

  @override
  void initState() {
    super.initState();
    _splashViewModel.checkLogin();
    _splashViewModel.navigationEvent.addListener(() {
      _navigate(_splashViewModel.navigationEvent.value);
    });
  }

  void _navigate(NavigateEvent event) {
    switch (event) {
      case NavigateEvent.MAIN:
        Navigator.of(context).pushReplacementNamed('/main');
        break;
      case NavigateEvent.LOCATION:
        Navigator.of(context).pushReplacementNamed('/map');
        break;
      case NavigateEvent.LOGIN:
        Navigator.of(context).pushReplacementNamed('/login');
        break;
    }
  }
}
