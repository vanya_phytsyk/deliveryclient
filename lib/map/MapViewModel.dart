import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/AddressRequest.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sentry/sentry.dart';

class MapViewModel {
  final WebService _webService;

  ValueNotifier<String> address = ValueNotifier('');
  ValueNotifier<LatLng> selectedLocation = ValueNotifier(null);
  ValueNotifier<LatLng> currentLocation =
  ValueNotifier(LatLng(55.7520, 37.6175));
  final _geocoder = Geocoder.local;
  ValueNotifier<bool> isLocationPermissionsDenied = ValueNotifier(false);
  final location = new Location();

  final LocalStorage _localStorage;

  final CartViewModel _cartViewModel;

  MapViewModel(this._localStorage, this._webService, this._cartViewModel);

  void initCurrentLocation() {
    location.hasPermission().then((hasPermission) {
      if (!hasPermission) {
        isLocationPermissionsDenied.value = true;
      } else {
        _loadLocation();
      }
    });
  }

  Future<void> _loadLocation() async {
    await justDelay();
    final currentLocation = await _getLocation();
    var latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
    this.currentLocation.value = latLng;
    this.selectedLocation.value = latLng;
    try {
      this.address.value = (await _getAddress(latLng)).first.addressLine;
      _saveLocation();
    } on Exception catch (e) {
      getIt<SentryClient>().captureException(exception: e);
      print(e);
    }
  }

  Future<void> justDelay() async {
    return Future.delayed(Duration(seconds: 1));
  }

  Future<LocationData> _getLocation() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
    } catch (e) {
      getIt<SentryClient>().captureException(exception: e);
      if (e.code == 'PERMISSION_DENIED') {
        return Future.error('permission_denied');
      }
      return Future.error('no location');
    }
    return currentLocation;
  }

  Future<List<Address>> _getAddress(LatLng latLng) {
    return _geocoder.findAddressesFromCoordinates(
        Coordinates(latLng.latitude, latLng.longitude));
  }

  void requestLocationPermission() async {
    final isGranted = await location.requestPermission();
    this.isLocationPermissionsDenied.value = !isGranted;
    if (isGranted) {
      await _loadLocation();
    }
  }

  void onMapCameraMove(LatLng targetLocation) {
    this.selectedLocation.value = targetLocation;
  }

  Future<void> onMapCameraMoveStop() async {
    if (this.selectedLocation.value == null) return;
    final addressLine =
        (await _getAddress(this.selectedLocation.value)).first.addressLine;
    this.address.value = addressLine;
    await _saveLocation();
    await _updateDeliveryPrice();
  }

  void onMyLocationClick() {
    this.currentLocation.notifyListeners();
  }

  _saveLocation() async {
    try {
      await _webService.updateAddress(AddressRequest.fromJsonMap({
        "address": this.address.value ?? "",
        "lng": this.selectedLocation.value.longitude,
        "lat": this.selectedLocation.value.latitude
      }));
    } catch (e) {
      getIt<SentryClient>().captureException(exception: e);
      return;
    }
    _localStorage.saveAddress(this.address.value);
    _localStorage.saveLocationLon(this.selectedLocation.value.longitude);
    _localStorage.saveLocationLat(this.selectedLocation.value.latitude);
  }

  _updateDeliveryPrice() async {
    await _cartViewModel.updateDeliveryCost();
  }
}
