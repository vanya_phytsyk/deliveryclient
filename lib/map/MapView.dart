import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/map/MapViewModel.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:math';

class MapView extends StatefulWidget {
  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  Completer<GoogleMapController> _controller = Completer();

  bool isPermissionDialogShown = false;

  void _onMapCreated(GoogleMapController controller) {
    if (!_controller.isCompleted) {
      _controller.complete(controller);
    } else {
      _controller = Completer()..complete(controller);
    }
  }

  @override
  void initState() {
    super.initState();
    viewModel.currentLocation.addListener(() {
      updateCamera(viewModel.currentLocation.value);
    });

    viewModel.isLocationPermissionsDenied.addListener(() {
      if (viewModel.isLocationPermissionsDenied?.value == true) {
        _showLocationPermissionDialog(context, viewModel);
        setState(() {
          isPermissionDialogShown = true;
        });
      } else if (isPermissionDialogShown) {
        setState(() {
          isPermissionDialogShown = false;
        });
        Navigator.pop(context);
      }
    });
  }

  GlobalKey _mapKey = GlobalKey();

  final MapViewModel viewModel = getIt<MapViewModel>()..initCurrentLocation();

  @override
  Widget build(BuildContext context) {
    final MapArguments arguments = ModalRoute.of(context).settings.arguments;
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
        ),
        Column(
          children: <Widget>[
            Material(
              elevation: 4.0,
              color: Colors.white,
              child: Container(
                height: 88.0,
                child: Stack(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(bottom: 12.0),
                        alignment: AlignmentDirectional.bottomCenter,
                        child: Text(
                          'Адрес доставки',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17.0,
                            fontWeight: FontWeight.w500,
                            fontFamily: 'Montserrat',
                          ),
                        )),
                    Visibility(
                      visible: arguments?.isModal ?? false,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.of(context).pop({
                            "address": viewModel.address,
                            "latLng": viewModel.selectedLocation.value
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(14.0),
                          child: Container(
                              alignment: AlignmentDirectional.bottomStart,
                              child: Image.asset(
                                  'assets/ion-chevron-left - Ionicons.png')),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(child: _createMap(viewModel)),
            Material(
              child: Container(
                width: double.infinity,
                alignment: AlignmentDirectional.topStart,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 10.0, top: 19.0),
                      alignment: AlignmentDirectional.topStart,
                      child: Text('Укажите адрес доставки',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Montserrat',
                          )),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 23.0, bottom: 15.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          color: Color(0xffDFDFDF)),
                      child: ListenableProvider<ValueNotifier<String>>(
                        builder: (BuildContext context) => viewModel.address,
                        child: Consumer<ValueNotifier<String>>(
                          builder: (BuildContext context,
                                  ValueNotifier<String> address,
                                  Widget child) =>
                              Row(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(
                                      left: 15.0, top: 9.0, bottom: 9.0),
                                  child: SvgPicture.asset(
                                    'assets/svg/address_marker.svg',
                                    height: 29.0,
                                    width: 21.0,
                                  )),
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(left: 15.0),
                                  child: Text(address.value,
                                      softWrap: true,
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Color(0xff1E2432),
                                        fontSize: 14.0,
                                        fontFamily: 'Montserrat',
                                      )),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SafeArea(
                      top: false,
                      child: GestureDetector(
                        onTap: () {
                          if (arguments?.isModal ?? false) {
                            Navigator.of(context).pop({
                              "address": viewModel.address.value,
                              "latLng": viewModel.selectedLocation.value
                            });
                          } else {
                            Navigator.of(context).pushReplacementNamed('/main');
                          }
                        },
                        child: Container(
                          height: 43.0,
                          margin: EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 8.0),
                          alignment: AlignmentDirectional.center,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Color(0xFFFFA300),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Text(
                            'Продолжить',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Dialog _createPermissionDialog(
      BuildContext context, MapViewModel mapViewModel) {
    return Dialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: 315.0,
              child: Stack(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          alignment: AlignmentDirectional.center,
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 30.0),
                          child: Image.asset('assets/lock-overturning.png')),
                      Container(
                        alignment: AlignmentDirectional.center,
                        margin:
                            EdgeInsets.only(left: 26.0, right: 26.0, top: 20.0),
                        child: Text(
                          'Службы геолокации',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 24.0,
                            letterSpacing: 0.43,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Montserrat',
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        alignment: AlignmentDirectional.center,
                        margin:
                            EdgeInsets.only(left: 22.0, right: 22.0, top: 26.0),
                        child: Text(
                          'Для определения вашего местоположения, пожалуйста включите службы геолокации',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17.0,
                            fontFamily: 'Montserrat',
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          mapViewModel.requestLocationPermission();
                        },
                        child: Container(
                          height: 44.0,
                          margin: EdgeInsets.only(
                              top: 26.0, left: 40.0, right: 40.0, bottom: 30.0),
                          alignment: AlignmentDirectional.center,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0xFFFFA300),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(22.0)),
                          child: Text(
                            'Включить службы',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Align(
                      alignment: AlignmentDirectional.topEnd,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context, null);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Image.asset('assets/close_dialog.png'),
                        ),
                      ))
                ],
              ),
            ),
          ],
        ));
  }

  void _showLocationPermissionDialog(
      BuildContext context, MapViewModel mapViewModel) {
    final dialog = _createPermissionDialog(context, mapViewModel);
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return dialog;
        });
  }

  Widget _createMap(MapViewModel mapViewModel) {
    return Stack(
      children: <Widget>[
        FutureBuilder<AndroidDeviceInfo>(
          future: DeviceInfoPlugin().androidInfo,
          builder: (BuildContext context,
              AsyncSnapshot<AndroidDeviceInfo> snapshot2) {
            return (snapshot2?.data?.device?.contains('meizu') == true)
                ? Transform.rotate(
                    child: Container(
                      child: GoogleMap(
                        myLocationEnabled: true,
                        myLocationButtonEnabled: !isPermissionDialogShown,
                        onMapCreated: (controller) async {
                          print('map created');
                          this._onMapCreated(controller);
                        },
                        onCameraIdle: () {
                          mapViewModel.onMapCameraMoveStop();
                        },
                        onCameraMove: (cameraPosition) {
                          mapViewModel.onMapCameraMove(cameraPosition.target);
                        },
                        initialCameraPosition: CameraPosition(
                            target: mapViewModel.currentLocation.value,
                            zoom: 18.0),
                      ),
                    ),
                    angle: 0,
                  )
                : Container(
                    child: GoogleMap(
                      key: _mapKey,
                      myLocationEnabled: false,
                      myLocationButtonEnabled: false,
                      rotateGesturesEnabled: false,
                      onMapCreated: (controller) async {
                        this._onMapCreated(controller);
                      },
                      onCameraIdle: () {
                        mapViewModel.onMapCameraMoveStop();
                      },
                      onCameraMove: (cameraPosition) {
                        mapViewModel.onMapCameraMove(cameraPosition.target);
                      },
                      initialCameraPosition: CameraPosition(
                          target: mapViewModel.currentLocation.value,
                          zoom: 18.0),
                    ),
                  );
          },
        ),
        Align(
          alignment: AlignmentDirectional.center,
          child: Image.asset('assets/map_marker.png'),
        ),
        Align(
            alignment: AlignmentDirectional.bottomEnd,
            child: FutureBuilder<AndroidDeviceInfo>(
              future: DeviceInfoPlugin().androidInfo,
              builder: (BuildContext context,
                      AsyncSnapshot<AndroidDeviceInfo> snapshot2) =>
                  Visibility(
                visible: (snapshot2?.data?.device?.contains('meizu') != true),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => mapViewModel.onMyLocationClick(),
                  child: Container(
                    margin: const EdgeInsets.all(8.0),
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        color: Color(0xFFFFA300)),
                    alignment: AlignmentDirectional.center,
                    child: SvgPicture.asset(
                      'assets/svg/target.svg',
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
              ),
            ))
      ],
    );
  }

  Future<void> updateCamera(LatLng position) async {
    if (position == null) return;
    try {
      final controller = await _controller.future;
      controller.moveCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: position, zoom: 18.0)));
    } catch (e) {
      print(e);
    }
  }
}

class MapArguments {
  final isModal;

  MapArguments(this.isModal);
}
