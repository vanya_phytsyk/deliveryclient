import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/orderassemble/OrderAssembleViewModel.dart';
import 'package:delivery_client/main/orders/OrdersViewModel.dart';
import 'package:delivery_client/main/profile/AttachedCardViewModel.dart';
import 'package:delivery_client/main/profile/InfoViewModel.dart';
import 'package:delivery_client/main/shops/ShopsViewModel.dart';
import 'package:delivery_client/map/MapViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:sentry/sentry.dart';

GetIt getIt = GetIt();

void registerDependencies() {
  getIt.registerLazySingleton<LocalStorage>(() => LocalStorage());
  getIt.registerLazySingleton<WebService>(
      () => WebService(getIt<LocalStorage>()));

  //viewModels singletones
  getIt.registerLazySingleton<CartViewModel>(
      () => CartViewModel(getIt<WebService>(), getIt<LocalStorage>()));
  getIt.registerLazySingleton<MapViewModel>(() => MapViewModel(
      getIt<LocalStorage>(), getIt<WebService>(), getIt<CartViewModel>()));
  getIt.registerLazySingleton<OrdersViewModel>(
      () => OrdersViewModel(getIt<WebService>()));
  getIt.registerLazySingleton<ShopsViewModel>(
      () => ShopsViewModel(getIt<WebService>()));
  getIt.registerLazySingleton<OrderAssembleViewModel>(() =>
      OrderAssembleViewModel(
          getIt<WebService>(), getIt<LocalStorage>(), getIt<CartViewModel>()));
  getIt.registerLazySingleton<InfoViewModel>(
      () => InfoViewModel(getIt<WebService>()));
  getIt.registerLazySingleton<AttachedCardViewModel>(
      () => AttachedCardViewModel(getIt<WebService>()));


  //error tracking
  getIt.registerLazySingleton<SentryClient>(() => SentryClient(
      dsn: "https://b16e06da5b644028a9eadf8f58a79e36@sentry.io/1757458"));
}
