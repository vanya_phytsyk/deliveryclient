class Order {
  final int id;
  final String number;
  final double productsCount;
  final double price;
  final String shopLogo;
  final OrderStatus orderStatus;

  Order(this.id, this.number, this.productsCount, this.price, this.shopLogo,
      this.orderStatus);

  Order copy({OrderStatus status}) {
    return Order(this.id, this.number, this.productsCount, this.price,
        this.shopLogo, status ?? this.orderStatus);
  }
}

enum OrderStatus {
  NEW,
  INWORK,
  WAITCOURIER,
  WAITDELIVERY,
  DELIVERY,
  COMPLETED,
  CANCELLED
}
