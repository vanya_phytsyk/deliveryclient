
import 'Product.dart';

class CartItem {
  final Product product;
  double amount;

  CartItem(this.product, this.amount);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is CartItem &&
              runtimeType == other.runtimeType &&
              product == other.product;

  @override
  int get hashCode => product.hashCode;


}