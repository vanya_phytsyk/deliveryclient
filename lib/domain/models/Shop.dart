class Shop {
  final int id;
  final String name;
  final String icon;
  final int inPrice;
  final int outPrice;

  Shop(this.id, this.name, this.icon, this.inPrice, this.outPrice);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Shop &&
              runtimeType == other.runtimeType &&
              id == other.id;

  @override
  int get hashCode => id.hashCode;




}