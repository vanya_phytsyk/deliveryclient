
class ProductCategory {
  final int id;
  final String name;
  final List<ProductCategory> subCategories = [];

  ProductCategory(this.id, this.name);
}