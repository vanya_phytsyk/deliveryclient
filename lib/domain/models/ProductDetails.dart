import 'Product.dart';

class ProductDetails {
  final Product product;
  final List<String> photos;
  final String description;
  final double weight;
  final String brand;
  final String producer;
  final String holdTermin;
  final String holdConditions;
  final String consist;

  ProductDetails(this.product, this.photos, this.description, this.weight, this.brand,
      this.producer, this.holdTermin, this.holdConditions, this.consist);
}
