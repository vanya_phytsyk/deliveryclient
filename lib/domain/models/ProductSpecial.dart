import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:tuple/tuple.dart';

import 'Product.dart';

class ProductSpecial {
  final int id;
  final String name;
  final num discount;
  final String description;
  final List<Tuple2<CartItem, num>> productsWithDiscounts;

  ProductSpecial(
      this.id, this.name, this.discount, this.description, this.productsWithDiscounts);
}
