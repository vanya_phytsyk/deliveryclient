class Product {
  final int id;
  final String name;
  final String photoUrl;
  final double price;
  final double discountPrice;
  final String unit;
  final double unitStep;

  Product(this.id, this.name, this.photoUrl, this.price, this.discountPrice, this.unit, this.unitStep);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Product &&
              runtimeType == other.runtimeType &&
              id == other.id;

  @override
  int get hashCode => id.hashCode;


}

