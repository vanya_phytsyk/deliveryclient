import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/login/LoginViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';
import 'countryCodes.dart';
import 'package:flutter/gestures.dart';
import 'package:toast/toast.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  Map _selectedCountryCode = codes[1]; //Russia
  final _phoneInputController = TextEditingController();
  String _currentPhoneInput = "";
  final _PHONE_NUMBER_MIN_SIZE = 8;
  bool _isTermsChecked = false;
  LoginViewModel _loginViewModel = LoginViewModel(getIt<WebService>());
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _phoneInputController.addListener(() {
      setState(() {
        _currentPhoneInput = _phoneInputController.text ?? "";
      });
    });
    _loginViewModel.loginResponse.addListener(() {
      final loginResponse = _loginViewModel.loginResponse.value;
      if (loginResponse.loginResponse.smsCheckToken != null) {
        Navigator.of(context).pushNamed('/verify', arguments: loginResponse);
      }
    });
    _loginViewModel.errorMessage.addListener(() {
      final errorMessage = _loginViewModel.errorMessage.value;
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Padding(
          child: Text(errorMessage),
          padding: EdgeInsets.only(bottom: 43.0),
        ),
      ));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _phoneInputController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
    return Material(
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage('assets/bg.png'))),
          ),
          Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: EdgeInsets.only(bottom: 50.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 60.0),
                      child: Center(
                        child: Image(
                          image: AssetImage('assets/MOps-logo.png'),
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 20.0),
                        child: Text(
                          'Вход',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 34.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold),
                        )),
                    Container(
                      margin:
                          EdgeInsets.only(left: 40.0, right: 40.0, top: 11.0),
                      child: Text(
                        'Введите свой номер телефона для входа или регистрации',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 17.0,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                    Container(
                      width: 295.0,
                      margin: EdgeInsets.only(top: 30.0),
                      decoration: BoxDecoration(
                          color: Color(0xFFEAECEF),
                          borderRadius:
                              BorderRadius.all(Radius.circular(22.0))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          DropdownButtonHideUnderline(
                            child: DropdownButton<Map>(
                              iconEnabledColor: Colors.black,
                              value: _selectedCountryCode,
                              items: codes.map((Map value) {
                                return DropdownMenuItem<Map>(
                                  value: value,
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                          margin: EdgeInsets.only(left: 9.0),
                                          width: 30.0,
                                          height: 30.0,
                                          child: ClipOval(
                                            child: Image.asset(
                                                'flags/${value['code'].toLowerCase()}.png',
                                                fit: BoxFit.fill),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          value["dial_code"] ?? "",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 17.0,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: 'Montserrat',
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  _selectedCountryCode = newValue;
                                });
                              },
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 11.0, bottom: 11.0),
                              width: 110.0,
                              child: TextField(
                                controller: _phoneInputController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(0.0),
                                    hintText: 'Телефон'),
                                expands: false,
                                maxLines: 1,
                                keyboardType: TextInputType.number,
                                autofocus: false,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Montserrat',
                                ),
                              )),
                          GestureDetector(
                            child: Container(
                              margin: EdgeInsets.only(right: 15.0),
                              child: Image.asset(
                                'assets/clear_number.png',
                                fit: BoxFit.fill,
                                width: 18.0,
                                height: 18.0,
                              ),
                            ),
                            onTap: () => {
                              setState(() {
                                _phoneInputController.text = "";
                              })
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10.0),
                      width: 270.0,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _isTermsChecked = !_isTermsChecked;
                              });
                            },
                            child: Container(
                                margin: EdgeInsets.only(right: 8.0, top: 3.0),
                                foregroundDecoration: _isTermsChecked
                                    ? BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/checked.png')))
                                    : null,
                                child: Image.asset(
                                  'assets/unchecked.png',
                                )),
                          ),
                          Flexible(
                            child: GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  _isTermsChecked = !_isTermsChecked;
                                });
                              },
                              child: RichText(
                                text: TextSpan(
                                    text:
                                        'Нажимая далее вы принимаете условия нашего',
                                    style: TextStyle(
                                      fontSize: 12.0,
                                      color: Color(0xffB8BBC6),
                                      fontFamily: 'Montserrat',
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: ' договора офферты',
                                          style: TextStyle(
                                              color: Color(0xFFFFA300)),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () => {}),
                                    ]),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: SafeArea(
              child: GestureDetector(
                onTap: () {
                  if (validate()) {
                    _loginViewModel.tryRegister(
                        _selectedCountryCode['dial_code'] + _currentPhoneInput);
                  }
                },
                child: Padding(
                  padding: EdgeInsets.only(bottom: keyboardHeight),
                  child: Container(
                    height: 43.0,
                    alignment: AlignmentDirectional.center,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Color(validate() ? 0xFFFFA300 : 0xFFBFBFBF)),
                    child: Text(
                      'Далее',
                      style: TextStyle(
                        color: validate() ? Colors.white : Colors.black,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          ListenableProvider.value(
            child: Consumer<ValueNotifier<bool>>(
              builder: (BuildContext context, ValueNotifier isProgress,
                      Widget child) =>
                  Visibility(
                visible: isProgress.value,
                child: Container(
                  color: Colors.black.withAlpha(200),
                  child: Align(
                    alignment: AlignmentDirectional.center,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xFFFFA300)),
                    ),
                  ),
                ),
              ),
            ),
            value: _loginViewModel.isProgress,
          )
        ],
      ),
    );
  }

  bool validate() =>
      _currentPhoneInput.length >= _PHONE_NUMBER_MIN_SIZE && _isTermsChecked;
}
