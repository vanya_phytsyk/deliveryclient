import 'package:delivery_client/codeVerify/VerifyPhoneView.dart';
import 'package:delivery_client/common/ToastNotifier.dart';
import 'package:delivery_client/data/web/WebException.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/LoginResponse.dart';
import 'package:flutter/cupertino.dart';

class LoginViewModel with ChangeNotifier, ToastNotifier {
  final WebService _webService;
  final ValueNotifier<bool> isProgress = ValueNotifier(false);
  final ValueNotifier<VerifyArguments> loginResponse = ValueNotifier(null);

  LoginViewModel(this._webService);

  void tryRegister(String number) async {
    isProgress.value = true;
    try {
      var login = await _webService.register(number);
      loginResponse.value = VerifyArguments(login, true);
    } on WebException catch (e) {
      if (e.errorResponse.code == 'ALREADY_REGISTERED_ERROR') {
        await _login(number);
      }
    } catch (e) {
      showToast(e.toString());
    } finally {
      isProgress.value = false;
    }
  }

  _login(String number) async {
    try {
      var login = await _webService.login(number);
      loginResponse.value = VerifyArguments(login, false);
    } catch (e) {
      showToast(e.toString());
    } finally {
      isProgress.value = false;
    }
  }
}
