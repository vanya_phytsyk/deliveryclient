
import 'package:flutter/cupertino.dart';

class ToastNotifier {
  final ValueNotifier<String> errorMessage = ValueNotifier('');

  showToast(String text) {
    if (errorMessage.value != text) {
      errorMessage.value = text;
    } else {
      errorMessage.notifyListeners();
    }
  }

}