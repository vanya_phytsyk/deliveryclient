import 'package:delivery_client/common/ToastNotifier.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/AuthResponse.dart';
import 'package:delivery_client/data/web/models/LoginResponse.dart';
import 'package:flutter/cupertino.dart';

import 'VerifyPhoneView.dart';

class VerifyViewModel with ChangeNotifier, ToastNotifier {
  final WebService _webService;
  final ValueNotifier<bool> isProgress = ValueNotifier(false);
  final ValueNotifier<bool> verifySuccess = ValueNotifier(false);

  VerifyViewModel(this._webService);

  void verifyPhone(VerifyArguments arguments, String verifySmsToken) async {
    isProgress.value = true;
    try {
      final isRegister = arguments.isRegister;
      AuthResponse login = isRegister
          ? await _webService.checkRegisterSmsCode(
              arguments.loginResponse, verifySmsToken)
          : await _webService.checkLoginSmsCode(
              arguments.loginResponse, verifySmsToken);
      this.verifySuccess.value = login.success && login.access.isNotEmpty;
    } catch (e) {
      showToast(e.toString());
    } finally {
      isProgress.value = false;
    }
  }

  resendSMSClick(VerifyArguments arguments) async {
    isProgress.value = true;

    try {
      final response = await _webService.resendSms(
          !arguments.isRegister, arguments.loginResponse.smsCheckToken);
    } catch (e) {
      print(e);
    }

    isProgress.value = false;
  }
}
