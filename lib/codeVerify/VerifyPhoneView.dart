import 'dart:async';

import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/LoginResponse.dart';
import 'package:delivery_client/di/di.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quiver/async.dart';
import 'package:toast/toast.dart';
import 'dart:developer';

import 'VerifyViewModel.dart';

class VerifyPhoneView extends StatefulWidget {
  @override
  _VerifyPhoneViewState createState() => _VerifyPhoneViewState();
}

class _VerifyPhoneViewState extends State<VerifyPhoneView> {
  static final _CODE_LENGTH = 4;

  List<String> _codeFromSMS = List<String>.filled(_CODE_LENGTH, '');
  List<TextEditingController> _inputControllers = [];
  List<FocusNode> _focusNodes = [];
  VerifyViewModel _verifyViewModel = VerifyViewModel(getIt<WebService>());
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _canResendSMS = false;

  @override
  void initState() {
    for (int i = 0; i < _CODE_LENGTH; i++) {
      var controller = TextEditingController();
      _inputControllers.add(controller);
      _focusNodes.add(FocusNode());
    }
    _verifyViewModel.verifySuccess.addListener(() {
      if (_verifyViewModel.verifySuccess.value) {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/map', (Route<dynamic> route) => false);
      }
    });
    _verifyViewModel.errorMessage.addListener(() {
      final errorMessage = _verifyViewModel.errorMessage.value;
      Toast.show(errorMessage, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      finished = false;
    });
    super.initState();
  }

  StreamSubscription listen;

  void _updateCountDown(VerifyArguments arguments) {
    if (listen != null || _canResendSMS) return;
    var duration2 = DateTime.fromMillisecondsSinceEpoch(
            arguments.loginResponse.resendActivation)
        .difference(DateTime.now());
    if (duration2.isNegative) return;
    CountdownTimer timer = CountdownTimer(duration2, Duration(seconds: 1));
    listen = timer.listen(null);
    listen.onDone(() {
      listen = null;
      setState(() {
        _canResendSMS = true;
      });
    });
  }

  @override
  void dispose() {
    _inputControllers.forEach((it) {
      it.dispose();
    });
    _focusNodes.forEach((it) {
      it.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
    final VerifyArguments arguments = ModalRoute.of(context).settings.arguments;
    _updateCountDown(arguments);
    return Material(
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage('assets/bg.png'))),
          ),
          Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 58.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 60.0),
                      child: Center(
                        child: Image(
                          image: AssetImage('assets/MOps-logo.png'),
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 20.0),
                        child: Text(
                          'Код СМС',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 34.0,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold),
                        )),
                    Container(
                      margin:
                          EdgeInsets.only(left: 40.0, right: 40.0, top: 11.0),
                      child: Text(
                        'Введите код доступа,который пришел на ваш номер телефона',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 17.0,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 24.0),
                      width: 294.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          createCodeLetter(context, 0),
                          createCodeLetter(context, 1),
                          createCodeLetter(context, 2),
                          createCodeLetter(context, 3)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 49.0, left: 7.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Padding(
                  child: Image.asset('assets/ion-chevron-left - Ionicons.png'),
                  padding: EdgeInsets.all(8.0),
                ),
              )),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: Visibility(
              visible: _canResendSMS,
              child: SafeArea(
                child: GestureDetector(
                  onTap: () {
                    _verifyViewModel.resendSMSClick(arguments);
                    setState(() {
                      _canResendSMS = false;
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.only(bottom: keyboardHeight),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      width: 315.0,
                      child: RichText(
                        text: TextSpan(
                            text: 'Не получили смс с кодом доступа?',
                            style: TextStyle(
                              color: Color(0xFFB8BBC6),
                              fontSize: 17.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Montserrat',
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                  text: ''
                                      'Отправить еще одну смс с кодом',
                                  style: TextStyle(color: Color(0xFFFFA300))),
                            ]),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          ListenableProvider.value(
            child: Consumer<ValueNotifier<bool>>(
              builder: (BuildContext context, ValueNotifier isProgress,
                      Widget child) =>
                  Visibility(
                visible: isProgress.value,
                child: Container(
                  color: Colors.black.withAlpha(200),
                  child: Align(
                    alignment: AlignmentDirectional.center,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xFFFFA300)),
                    ),
                  ),
                ),
              ),
            ),
            value: _verifyViewModel.isProgress,
          )
        ],
      ),
    );
  }

  Widget createCodeLetter(BuildContext context, int index) {
    return SMSCodeLetter(index, _inputControllers[index], _focusNodes[index],
        (index, letter) {
      _onCodeLetterInput(context, index, letter);
    });
  }

  void _onCodeLetterInput(BuildContext context, int index, String letter) {
    _codeFromSMS[index] = letter;
    if (!_codeFromSMS.any((char) {
      return char.isEmpty;
    })) {
      _onCodeSuccess(context);
    }
    final nextIndex = letter.isEmpty ? index - 1 : index + 1;
    if (8 < 0) return;
    if (nextIndex >= _CODE_LENGTH) return;
    FocusScope.of(context).requestFocus(_focusNodes[nextIndex]);
  }

  var finished = false;

  void _onCodeSuccess(BuildContext context) async {
    if (finished) return;
    finished = true;
    final VerifyArguments arguments = ModalRoute.of(context).settings.arguments;
    _verifyViewModel.verifyPhone(arguments, _codeFromSMS.join(''));
  }
}

class SMSCodeLetter extends StatefulWidget {
  var _index;

  var _controller;

  var _focusNode;

  var _onCodeLetterInput;

  SMSCodeLetter(
      this._index, this._controller, this._focusNode, this._onCodeLetterInput);

  @override
  State<StatefulWidget> createState() {
    return _SMSCodeState(this._index, this._controller, this._focusNode,
        this._onCodeLetterInput);
  }
}

class _SMSCodeState extends State<StatefulWidget> {
  String letter;

  var _index;

  var _controller;

  var _focusNode;

  var _onCodeLetterInput;

  _SMSCodeState(
      this._index, this._controller, this._focusNode, this._onCodeLetterInput);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      width: 60.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: (letter == null || letter.isEmpty)
            ? Color(0xFFEAECEF)
            : Color(0xFFFFA300),
      ),
      child: Theme(
        data: Theme.of(context).copyWith(splashColor: Colors.transparent),
        child: TextField(
          autofocus: _index == 0,
          maxLines: 1,
          maxLength: 1,
          maxLengthEnforced: true,
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
              border: InputBorder.none,
              counterStyle: TextStyle(
                height: double.minPositive,
              ),
              counterText: ""),
          style: TextStyle(
              color: Colors.white,
              fontSize: 30.0,
              fontWeight: FontWeight.w800,
              fontFamily: 'Montserrat'),
          controller: _controller,
          focusNode: _focusNode,
          onChanged: (letter) {
            setState(() {
              this.letter = letter;
            });
            _onCodeLetterInput(_index, letter);
          },
        ),
      ),
    );
  }
}

class VerifyArguments {
  final LoginResponse loginResponse;
  final bool isRegister;

  VerifyArguments(this.loginResponse, this.isRegister);
}
