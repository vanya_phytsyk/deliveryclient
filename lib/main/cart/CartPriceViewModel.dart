import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:flutter/foundation.dart';

class CartPriceViewModel with ChangeNotifier {
  double productsPrice = 0.0;
  double deliveryPrice = 0.0;
  double totalPrice = 0.0;

  void updatePrice(List<CartItem> cartItems, {double deliveryCost}) {
    this.deliveryPrice = deliveryCost;
    _recalculatePriceInner(cartItems);
    notifyListeners();
  }

  void _recalculatePriceInner(List<CartItem> cartItems) {
    final double productsPrice = cartItems.fold(
        0, (sum, item) => sum + ((item.product.discountPrice ?? item.product.price) * item.amount));
    final totalPrice = (productsPrice ?? 0) + (deliveryPrice ?? 0);
    this.productsPrice = productsPrice;
    this.totalPrice = totalPrice;
  }
}
