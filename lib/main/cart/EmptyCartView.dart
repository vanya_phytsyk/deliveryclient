import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main_screen.dart';

class EmptyCartView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 88.0),
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.center,
              width: 216.0,
              height: 216.0,
              decoration:
                  BoxDecoration(color: Colors.white, shape: BoxShape.circle),
              child: Image.asset('assets/cart-icon.png'),
            ),
            Container(
              margin: EdgeInsets.only(top: 40.0),
              child: Text('Ваша корзина пуста',
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff1E2432),
                    fontFamily: 'Montserrat',
                  )),
            ),
            Container(
                width: 318.0,
                margin: EdgeInsets.only(top: 15.0),
                child: Text(
                    'Перейдите в каталог нужного магазина, чтобы выбрать товары',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 17.0,
                      color: Color(0xff1E2432),
                      fontFamily: 'Montserrat',
                    ))),
            Container(
              width: 230.0,
              margin: EdgeInsets.only(top: 20.0),
              decoration: BoxDecoration(
                  color: Color(0xffFFA300),
                  borderRadius: BorderRadius.circular(5.0)),
              child: Consumer<BottomNavigationIndexHolder>(
                builder: (BuildContext context,
                    BottomNavigationIndexHolder indexHolder, Widget child) {
                  return GestureDetector(
                    onTap: () {
                      indexHolder.changeIndex(0);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0, bottom: 11.0),
                      child: Text('В каталог',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0,
                            color: Color(0xff0A1F44),
                          )),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
