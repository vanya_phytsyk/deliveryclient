import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/Shop.dart';
import 'package:delivery_client/main/cart/CartItemViewModel.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/orderassemble/OrderAssembleView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'CartPriceViewModel.dart';
import 'EmptyCartView.dart';

class CartView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildCloneableWidget>[
        ListenableProvider.value(value: getIt<CartViewModel>()),
        ListenableProvider.value(
            value: getIt<CartViewModel>().cartPriceViewModel),
        ListenableProvider.value(value: getIt<CartViewModel>().currentShop)
      ],
      child: Scaffold(
        backgroundColor: Color(0xffF2F2F2),
        body: Stack(
          children: <Widget>[
            Consumer<CartViewModel>(
              builder: (BuildContext context, CartViewModel viewModel,
                  Widget child) {
                return IndexedStack(
                  index: viewModel.cartItems.isEmpty ? 1 : 0,
                  children: <Widget>[child, EmptyCartView()],
                );
              },
              child: Padding(
                  padding: EdgeInsets.only(top: 88.0),
                  child: Consumer<CartViewModel>(builder: (BuildContext context,
                      CartViewModel viewModel, Widget child) {
                    return SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(
                                  top: 40.0, left: 16.0, bottom: 11.0),
                              alignment: AlignmentDirectional.centerStart,
                              child: Text(
                                '${viewModel.cartItems.length} товаров',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff909090),
                                  fontFamily: 'Montserrat',
                                ),
                              )),
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border(
                                    top: BorderSide(color: Color(0xffE2E2E2)),
                                    bottom:
                                        BorderSide(color: Color(0xffE2E2E2)))),
                            child: Column(
                              children: viewModel.cartItems
                                  .map((cartItem) =>
                                      ChangeNotifierProvider<CartItemViewModel>(
                                        key:
                                            Key(cartItem.product.id.toString()),
                                        child: _buildCartItemView(),
                                        builder: (BuildContext context) =>
                                            CartItemViewModel(cartItem),
                                      ))
                                  .toList(),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(
                                  top: 40.0, left: 16.0, bottom: 11.0),
                              alignment: AlignmentDirectional.centerStart,
                              child: Text(
                                'Стоимость',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Color(0xff909090),
                                  fontFamily: 'Montserrat',
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.only(bottom: 88.0),
                            width: double.infinity,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border(
                                  top: BorderSide(color: Color(0xffE2E2E2)),
                                )),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffE2E2E2)),
                                      )),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0,
                                        top: 10.0,
                                        bottom: 11.0,
                                        right: 15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'Стоимость товаров',
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: Color(0xffB8BBC6),
                                            fontFamily: 'Montserrat',
                                          ),
                                        ),
                                        Consumer<CartPriceViewModel>(
                                          builder: (BuildContext context,
                                              CartPriceViewModel value,
                                              Widget child) {
                                            return Text(
                                              '${value.productsPrice.toStringAsFixed(2)} ₽',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                color: Color(0xff1E2432),
                                                fontFamily: 'Montserrat',
                                              ),
                                            );
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffE2E2E2)),
                                      )),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0,
                                        top: 10.0,
                                        bottom: 11.0,
                                        right: 15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'Стоимость доставки',
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: Color(0xffB8BBC6),
                                            fontFamily: 'Montserrat',
                                          ),
                                        ),
                                        Consumer<CartPriceViewModel>(
                                          builder: (BuildContext context,
                                                  CartPriceViewModel value,
                                                  Widget child) =>
                                              Text(
                                            viewModel.deliveryCost
                                                .toStringAsFixed(2),
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432),
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffE2E2E2)),
                                      )),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0,
                                        top: 10.0,
                                        bottom: 11.0,
                                        right: 15.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'Итого',
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xffB8BBC6),
                                            fontFamily: 'Montserrat',
                                          ),
                                        ),
                                        Consumer<CartPriceViewModel>(
                                          builder: (BuildContext context,
                                                  CartPriceViewModel value,
                                                  Widget child) =>
                                              Text(
                                            '${value.totalPrice.toStringAsFixed(2)} ₽',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xff1E2432),
                                              fontFamily: 'Montserrat',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  })),
            ),
            Material(
              elevation: 2.0,
              color: Colors.white,
              child: Container(
                height: 88.0,
                child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(bottom: 12.0),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: AlignmentDirectional.bottomCenter,
                          child: Consumer<ValueNotifier<Shop>>(
                            builder: (BuildContext context,
                                    ValueNotifier<Shop> shop, Widget child) =>
                                Text(
                              'Корзина ${shop.value != null ? " (" + shop.value.name + ") " : ""}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 17.0,
                                fontWeight: FontWeight.w500,
                                fontFamily: 'Montserrat',
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: AlignmentDirectional.bottomEnd,
                          child: Builder(
                            builder: (BuildContext context) => GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                showConfirmationDialog(context);
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 4.0,
                                    right: 15.0,
                                    bottom: 4.0,
                                    top: 4.0),
                                child: SvgPicture.asset(
                                    'assets/svg/cart-clear.svg'),
                              ),
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ),
            Consumer<CartViewModel>(
              builder: (BuildContext context, CartViewModel viewModel,
                  Widget child) {
                if (viewModel.cartItems.isNotEmpty) {
                  return Align(
                    alignment: AlignmentDirectional.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: 15.0),
                      child: Material(
                        color: Colors.white,
                        elevation: 2.0,
                        child: Padding(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context, rootNavigator: true)
                                  .pushNamed('/orderAssemble',
                                      arguments: OrderAssembleArguments(
                                          viewModel.currentShop.value.id,
                                          viewModel
                                              .cartPriceViewModel.totalPrice));
                            },
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Color(0xff54B24C),
                                  borderRadius: BorderRadius.circular(5.0)),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 12.0, bottom: 11.0),
                                child: Consumer<CartPriceViewModel>(
                                  builder: (BuildContext context,
                                          CartPriceViewModel value,
                                          Widget child) =>
                                      RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      text: 'Оформить заказ на ',
                                      style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 17.0,
                                        color: Colors.white,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text:
                                                '${value.totalPrice.toStringAsFixed(2)} ₽',
                                            style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w700,
                                              fontSize: 17.0,
                                              color: Colors.white,
                                            ))
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          padding: EdgeInsets.only(
                              left: 10, right: 10, top: 13.0, bottom: 13.0),
                        ),
                      ),
                    ),
                  );
                } else {
                  return Container();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _buildCartItemView() {
    return Consumer<CartItemViewModel>(
      builder: (context, cartItem, _) => Dismissible(
        onDismissed: (_) {
          Provider.of<CartViewModel>(context, listen: false)
              .removeAllCartItems(cartItem.cartItem);
        },
        key: Key(cartItem.cartItem.product.id.toString()),
        background: Container(
          alignment: AlignmentDirectional.centerEnd,
          color: Color(0xffFD4653),
          child: Padding(
            child: Image.asset('assets/swipe_menu_delete_icon.png'),
            padding: EdgeInsets.all(32.0),
          ),
        ),
        direction: DismissDirection.endToStart,
        child: Container(
          height: 122.0,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(bottom: BorderSide(color: Color(0xffE2E2E2)))),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              children: <Widget>[
                Image.network(
                  cartItem.cartItem.product.photoUrl,
                  width: 80.0,
                  height: 80.0,
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            cartItem.cartItem.product.name,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 17.0,
                                color: Color(0xff1E2432)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 3),
                            child: Text(
                              '${cartItem.cartItem.product.discountPrice ?? cartItem.cartItem.product.price} ₽/${cartItem.cartItem.product.unit}',
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10.0,
                                  color: Color(0xffFF4D00)),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 11.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Container(
                                alignment: AlignmentDirectional.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    color: Color(0xffFFA300)),
                                child: GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
//                                    cartItem.incrementItemsAmount();
                                    Provider.of<CartViewModel>(context,
                                            listen: false)
                                        .addItemToCart(cartItem.cartItem);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Image.asset(
                                      'assets/increment_count_large.png',
                                      width: 12.0,
                                      height: 12.0,
                                    ),
                                  ),
                                )),
                            SizedBox(
                              width: 72.0,
                              child: Text(
                                  '~${cartItem.cartItem.amount.toStringAsFixed(2)} ${cartItem.cartItem.product.unit}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color(0xffFFA300),
                                      fontFamily: 'Montserrat',
                                      fontSize: 13.0)),
                            ),
                            Container(
                                alignment: AlignmentDirectional.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    color: Color(0xffFFA300)),
                                child: GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
//                                    cartItem.decrementItemsAmount();
                                    Provider.of<CartViewModel>(context,
                                            listen: false)
                                        .removeItemFromCart(cartItem.cartItem);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Image.asset(
                                      'assets/decrement_count_large.png',
                                      width: 12.0,
                                      height: 12.0,
                                    ),
                                  ),
                                )),
                            Container(
                              margin: EdgeInsets.only(left: 20.0),
                              child: Text(
                                '${cartItem.getPrice().toStringAsFixed(2)} ₽',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Color(0xff1E2432),
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15.0,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void showConfirmationDialog(BuildContext context) {
  TextStyle style = TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.w600, fontFamily: 'Montserrat');
  AlertDialog dialog = AlertDialog(
    title: Text(
      'Уверены?',
      style: style.copyWith(fontSize: 18.0),
    ),
    actions: <Widget>[
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Provider.of<CartViewModel>(context, listen: false).clearCart();
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Да',
            style: style,
          ),
        ),
      ),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => Navigator.pop(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Нет',
            style: style,
          ),
        ),
      )
    ],
  );
  showDialog(context: context, builder: (context) => dialog);
}
