import 'package:delivery_client/domain/models/CartItem.dart';

abstract class CartHolder {
  void addItemToCart(CartItem product);
  void removeItemFromCart(CartItem product);
}