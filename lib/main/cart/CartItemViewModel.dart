import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:flutter/foundation.dart';

class CartItemViewModel with ChangeNotifier {
  CartItem cartItem;

  CartItemViewModel(this.cartItem);

  double getPrice() {
    return (cartItem.product.discountPrice ??
        cartItem.product.price) * cartItem.amount;
  }

  void incrementItemsAmount() {
    cartItem.amount += cartItem.product.unitStep;
    if (cartItem.amount < 0.00000001) cartItem.amount = 0;
    notifyListeners();
  }

  void decrementItemsAmount() {
    cartItem.amount -= cartItem.product.unitStep;
    if (cartItem.amount < 0.0000001) cartItem.amount = 0;
    notifyListeners();
  }
}
