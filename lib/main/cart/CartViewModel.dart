import 'package:delivery_client/common/ToastNotifier.dart';
import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/data/web/WebException.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/domain/models/Shop.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import 'CartItemViewModel.dart';
import 'CartPriceViewModel.dart';

class CartViewModel with ChangeNotifier, ToastNotifier {
  ValueNotifier<Shop> currentShop = ValueNotifier(null);

  List<CartItem> cartItems = List();

  CartPriceViewModel cartPriceViewModel = CartPriceViewModel();
  ValueNotifier<int> cartAmount = ValueNotifier(0);

  WebService _webService;
  LocalStorage _localStorage;

  double deliveryCost = 0;

  CartViewModel(this._webService, this._localStorage) {
    _loadCart();
  }

  removeAllCartItems(CartItem cartItem) async {
    try {
      await _webService.removeAllItemsFromCart(cartItem.product.id);
    } catch (e) {
      return;
    }
    cartItems.removeWhere((it) => it.product.id == cartItem.product.id);
    _innerUpdate();
  }

  void updatePrice() {
    cartItems.removeWhere((item) => item.amount == 0);
    _innerUpdate();
  }

  clearCart() async {
    try {
      await _webService.clearCart();
    } catch (e) {
      return;
    }
    cartItems.clear();
    currentShop.value = null;
    _innerUpdate();
  }

  void _innerUpdate() {
    notifyListeners();
    cartPriceViewModel.updatePrice(cartItems, deliveryCost: deliveryCost);
    cartAmount.value = cartItems.length;
  }

  CartItem getCartItem(Product product) {
    return cartItems.singleWhere((item) => item.product == product,
        orElse: () => null);
  }

  addItemToCart(CartItem product) async {
    try {
      await _webService.addItemToCart(product.product.id, currentShop.value.id);
    } catch (e) {
      print(e);
      return;
    }
    var result = product;
    if (!cartItems.contains(product)) {
      product.amount = product.product.unitStep;
      cartItems.add(product);
    } else {
      result = cartItems.firstWhere((item) => item == product);
      result.amount += product.product.unitStep;
    }
    _innerUpdate();
  }

  removeItemFromCart(CartItem product) async {
    try {
      await _webService.removeItemFromCart(product.product.id);
    } catch (e) {
      print(e);
      return;
    }
    var result = product;
    if (cartItems.contains(product)) {
      result = cartItems.firstWhere((item) => item == product);
      result.amount -= product.product.unitStep;
      if (result.amount <= 0.00001) {
        cartItems.remove(result);
      }
    }
    _innerUpdate();
  }

  updateCurrentShop(Shop shop) async {
    if (currentShop.value != shop) {
      await clearCart();
      this.currentShop.value = shop;

      updateDeliveryCost();
    }
  }

  updateDeliveryCost() async {
    if (currentShop.value == null) return;
    final lat = await _localStorage.getLocationLat();
    final lng = await _localStorage.getLocationLon();
    try {
      final deliveryCostResponse =
          await _webService.getDeliveryCost(currentShop.value.id, lat, lng);
      this.deliveryCost = deliveryCostResponse.result.deliveryCost.toDouble() +
          deliveryCostResponse.result.tariffInArea.toDouble();
      updatePrice();
    } on WebException catch (e) {
      showToast(e.errorResponse.msg);
    }
  }

  void _loadCart() async {
    try {
      final cartResponse = await _webService.getCart();
      if (cartResponse.success) {
        final cartItems = cartResponse.records.map((record) => CartItem(
            Product(
                record.goodId,
                record.goodName,
                record.goodImg,
                record.goodPrice.toDouble(),
                record.goodDiscountPrice.toDouble(),
                record.goodUnit,
                record.goodUnitStep.toDouble()),
            record.goodCnt.toDouble()));
        this.cartItems.addAll(cartItems);
        this.currentShop.value = cartResponse.shop == null
            ? null
            : Shop(cartResponse.shop.sid, cartResponse?.shop?.name, null, null,
                null);
        this.deliveryCost = cartResponse.shop.deliveryPrice;
        _innerUpdate();
      }
    } catch (e) {
      print(e);
    }
  }
}
