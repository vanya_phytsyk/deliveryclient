import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/Category.dart';
import 'package:delivery_client/main/categories/CategoriesViewModel.dart';
import 'package:delivery_client/main/products/ProductsView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class CategoriesListView extends StatelessWidget {
  final CategoriesViewModel _categoriesViewModel;
  final List<ProductCategory> allCategories;

  CategoriesListView(this._categoriesViewModel, {Key key, this.allCategories})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListenableProvider<ValueNotifier<List<ProductCategory>>>(
      builder: (BuildContext context) => _categoriesViewModel.categories,
      child: Consumer<ValueNotifier<List<ProductCategory>>>(
        builder: (BuildContext context,
                ValueNotifier<List<ProductCategory>> categories,
                Widget child) =>
            ListView.builder(
                itemCount: categories.value?.length ?? 0,
                itemBuilder: (context, i) {
                  return Theme(
                    child: ExpansionTile(
                        leading: Container(
                            width: 40.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                                color: Color(0xff0A1F44),
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(8.0)),
                            child: SvgPicture.asset(
                              'assets/svg/groceries.svg',
                              width: 23.0,
                              height: 29.0,
                              fit: BoxFit.none,
                            )),
                        trailing: Text(''),
                        title: Text(
                          categories.value[i].name,
                          style: TextStyle(
                            color: Color(0xff0A1F44),
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 4.0),
                            alignment: AlignmentDirectional.centerStart,
                            child: IntrinsicHeight(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Container(
                                    width: 1.0,
                                    color: Color(0xff0A1F44),
                                    margin: EdgeInsets.only(left: 62.0),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: categories.value[i].subCategories
                                        .map((sub) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).pushNamed(
                                              '/products',
                                              arguments:
                                                  ShopCategoriesArguments(
                                                      _categoriesViewModel
                                                          .categories.value[i],
                                                      sub.id,
                                                      _categoriesViewModel
                                                          .shopId));
                                        },
                                        child: Container(
                                          width: 250.0,
                                          margin: EdgeInsets.only(left: 7.0),
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                top: 8.0, bottom: 8.0),
                                            child: Text(sub.name,
                                                style: TextStyle(
                                                  color: Color(0xff0A1F44),
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Montserrat',
                                                )),
                                          ),
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ]),
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                  );
                }),
      ),
    );
  }
}
