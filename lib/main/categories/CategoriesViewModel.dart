import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/Category.dart';
import 'package:flutter/foundation.dart';

class CategoriesViewModel {
  final int shopId;

  final WebService _webService;

  final categories = ValueNotifier<List<ProductCategory>>(null);
  var subCategories;

  CategoriesViewModel(this.shopId, this._webService) {
    _loadCategories();
  }

  _loadCategories() async {
    final categoriesResponse = await _webService.getCategories(shopId);

    subCategories = categoriesResponse.records
        .where((cat) => cat.cat_level == 1)
        .map((record) => ProductCategory(record.cat_id, record.cat_name))
        .toList();

    final categories = categoriesResponse.records
        .where((record) => record.cat_parent_id == 0)
        .map((record) {
      final subCategories = categoriesResponse.records
          .where((sub) => sub.cat_parent_id == record.cat_id)
          .map((sub) => ProductCategory(sub.cat_id, sub.cat_name))
          .toList();
      return ProductCategory(record.cat_id, record.cat_name)
        ..subCategories.addAll(subCategories);
    }).toList();

    this.categories.value = categories;
  }
}
