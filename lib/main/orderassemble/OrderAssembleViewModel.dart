import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/cart/CartResponse.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/src/material/time.dart';

class OrderAssembleViewModel with ChangeNotifier {
  final WebService _webService;
  final CartViewModel _cartViewModel;

  final LocalStorage _localStorage;
  final ValueNotifier<DateTime> deliveryTime = ValueNotifier(null);
  final ValueNotifier<bool> isProgress = ValueNotifier(false);

  ValueNotifier<String> address = ValueNotifier(null);
  double lon;
  double lat;

  int _shopId;

  set shopId(int shopId) {
    this._shopId = shopId;
    this.deliveryTime.value = null;
    this.isProgress.value = false;

    _loadCurrentAddress();
  }

  OrderAssembleViewModel(
      this._webService, this._localStorage, this._cartViewModel);

  void _loadCurrentAddress() async {
    address.value = await _localStorage.getAddress();
    lon = await _localStorage.getLocationLon();
    lat = await _localStorage.getLocationLat();
  }

  void updateAddress(Map mapResult) {
    address.value = mapResult["address"];
    lon = mapResult["lon"];
    lat = mapResult["lat"];
  }

  void updateDeliveryTime(TimeOfDay timeOfDay) {
    final now = new DateTime.now();
    deliveryTime.value = DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
  }

  Future<OrderConfirmResultBundle> onOrderProcessClick(Map orderData) async {
    try {
      isProgress.value = true;

      orderData['shopSID'] = _shopId;
      orderData['address'] = address.value;
      orderData['addressLat'] = lat;
      orderData['addressLng'] = lon;
      orderData['timeDelivery'] =
          deliveryTime.value?.millisecondsSinceEpoch ?? 0;

      final orderConfirmRequestBody =
          orderData.map((key, value) => MapEntry(key, value.toString()));

      final orderConfirmResponse =
          await _webService.confirmOrder(orderConfirmRequestBody);
      if (!orderConfirmResponse.success) {
        return OrderConfirmResultBundle(OrderConfirmResult.ERROR);
      }

      final orderId = orderConfirmResponse.result;
      final paymentCardsResponse = await _webService.getPaymentCards();
      if (!paymentCardsResponse.success ||
          paymentCardsResponse.records.isEmpty) {
        return OrderConfirmResultBundle(OrderConfirmResult.NO_CARD,
            orderId: orderId);
      }

      final cardId = paymentCardsResponse.records.first.sid;
      final paymentResponse =
          await _webService.orderPayment(orderId, cardId.toString());
      if (!paymentResponse.success) {
        return OrderConfirmResultBundle(OrderConfirmResult.ERROR);
      } else {
        await _cartViewModel.clearCart();
        return OrderConfirmResultBundle(OrderConfirmResult.SUCCESS,
            orderId: orderId);
      }
    } catch (e) {
      return OrderConfirmResultBundle(OrderConfirmResult.ERROR);
    } finally {
      isProgress.value = false;
    }
  }

  Future<OrderConfirmResultBundle> onCardAttached(
      int cardId, int orderId) async {
    final paymentResponse =
        await _webService.orderPayment(orderId, cardId.toString());
    if (!paymentResponse.success) {
      return OrderConfirmResultBundle(OrderConfirmResult.ERROR);
    } else {
      await _cartViewModel.clearCart();
      return OrderConfirmResultBundle(OrderConfirmResult.SUCCESS,
          orderId: orderId);
    }
  }
}

class OrderConfirmResultBundle {
  final OrderConfirmResult orderConfirmResult;
  final int orderId;

  OrderConfirmResultBundle(this.orderConfirmResult, {this.orderId});
}

enum OrderConfirmResult { SUCCESS, NO_CARD, ERROR }
