import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main_screen.dart';

class OrderFailedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: Padding(
        padding: const EdgeInsets.only(top: 88.0),
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: AlignmentDirectional.center,
                width: 216.0,
                height: 216.0,
                decoration:
                BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                child: Image.asset('assets/icon-failed.png'),
              ),
              Container(
                margin: EdgeInsets.only(top: 40.0),
                alignment: AlignmentDirectional.center,
                child: Text('Какая-то ошибка :)',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff1E2432),
                      fontFamily: 'Montserrat',
                    )),
              ),
              Container(
                  width: 318.0,
                  margin: EdgeInsets.only(top: 15.0),
                  child: Text(
                      'Возможно на вашей карте недостаточно средств или вы ошиблись при вводе данных.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17.0,
                        color: Color(0xff1E2432),
                        fontFamily: 'Montserrat',
                      ))),
              Container(
                  width: 230.0,
                  margin: EdgeInsets.only(top: 20.0),
                  decoration: BoxDecoration(
                      color: Color(0xffFFA300),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0, bottom: 11.0),
                      child: Text('Повторить снова',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0,
                            color: Color(0xff0A1F44),
                          )),
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
