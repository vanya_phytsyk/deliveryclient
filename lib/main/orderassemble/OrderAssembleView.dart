import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/order/OrderDetailsView.dart';
import 'package:delivery_client/main/orderassemble/OrderAssembleViewModel.dart';
import 'package:delivery_client/main/orderassemble/cardattach/CardAttachView.dart';
import 'package:delivery_client/main/orders/OrdersViewModel.dart';
import 'package:delivery_client/map/MapView.dart';
import 'package:delivery_client/map/MapViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import 'OrderSuccessView.dart';

class OrderAssembleView extends StatefulWidget {
  @override
  _OrderAssembleViewState createState() => _OrderAssembleViewState();
}

class _OrderAssembleViewState extends State<OrderAssembleView> {
  final _formKey = GlobalKey<FormState>();

  final _formData = <String, dynamic>{};

  @override
  Widget build(BuildContext context) {
    OrderAssembleArguments arguments =
        ModalRoute.of(context).settings.arguments;
    getIt<OrderAssembleViewModel>().shopId = arguments.shopId;

    var inputDecoration = InputDecoration(
        filled: true,
        fillColor: Colors.white,
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color(0x4D000000), width: 1.0)),
        contentPadding: EdgeInsets.all(10.0),
        hintText: 'Квартира',
        hintStyle: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 11.0,
            color: Color(0xff909090)));

    var boxDecoration = BoxDecoration(
      color: Colors.white,
      border: BorderDirectional(
          bottom: BorderSide(color: Color(0x4D000000), width: 1.0)),
    );

    var textStyle = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff1E2432));

    return ListenableProvider.value(
      value: getIt<OrderAssembleViewModel>(),
      child: Consumer<OrderAssembleViewModel>(
        builder: (context, OrderAssembleViewModel viewModel, _) {
          var textValidator = (value) {
            if (value.isEmpty) {
              return "пусто";
            }
            return null;
          };
          return Scaffold(
            backgroundColor: Color(0xffF2F2F2),
            body: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 88.0),
                  child: SingleChildScrollView(
                    reverse: true,
                    child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10.0, right: 10.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Адрес доставки',
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 12.0,
                                        color: Color(0xff909090)),
                                  ),
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () async {
                                      final mapResult = await Navigator.of(
                                              context,
                                              rootNavigator: true)
                                          .pushNamed('/map',
                                              arguments: MapArguments(true));
                                      viewModel.updateAddress(mapResult);
                                    },
                                    child: Text('Изменить',
                                        style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 12.0,
                                            color: Color(0xffFFA300))),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5.0),
                              decoration: boxDecoration,
                              child: Row(
                                children: <Widget>[
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () {
                                      Navigator.of(context, rootNavigator: true)
                                          .pushNamed('/map',
                                              arguments: MapArguments(true));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0,
                                          top: 9.0,
                                          bottom: 8.0,
                                          right: 10.0),
                                      child: Image.asset(
                                          'assets/map_marker_icon.png'),
                                    ),
                                  ),
                                  Expanded(
                                    child: ListenableProvider<
                                        ValueNotifier<String>>(
                                      builder: (context) => viewModel.address,
                                      child: Consumer<ValueNotifier<String>>(
                                        builder: (BuildContext context,
                                                ValueNotifier<String> address,
                                                Widget child) =>
                                            TextFormField(
                                          focusNode:
                                              FocusNode(canRequestFocus: false),
                                          controller: TextEditingController(
                                              text: address.value ?? ""),
                                          style: textStyle,
                                          enableInteractiveSelection: false,
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding:
                                                  EdgeInsets.all(10.0),
                                              hintStyle: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 11.0,
                                                  color: Color(0xff909090))),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                        child: TextFormField(
//                                      validator: textValidator,
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.center,
                                      style: textStyle,
                                      decoration: inputDecoration.copyWith(
                                          hintText: 'Квартира'),
                                      onSaved: (value) {
                                        _formData['addressFlat'] = value;
                                      },
                                    )),
                                  ),
                                  Expanded(
                                    child: Container(
                                        margin: EdgeInsets.only(left: 11.0),
                                        width: 78.0,
                                        child: TextFormField(
//                                          validator: textValidator,
                                          style: textStyle,
                                          keyboardType: TextInputType.number,
                                          textAlign: TextAlign.center,
                                          decoration: inputDecoration.copyWith(
                                              hintText: 'Этаж'),
                                          onSaved: (value) {
                                            _formData['addressFloor'] = value;
                                          },
                                        )),
                                  ),
                                  Expanded(
                                    child: Container(
                                        margin: EdgeInsets.only(left: 11.0),
                                        width: 78.0,
                                        child: TextFormField(
//                                          validator: textValidator,
                                          style: textStyle,
                                          keyboardType: TextInputType.number,
                                          textAlign: TextAlign.center,
                                          decoration: inputDecoration.copyWith(
                                              hintText: 'Подъезд'),
                                          onSaved: (value) {
                                            _formData['addressPorch'] = value;
                                          },
                                        )),
                                  ),
                                  Expanded(
                                    child: Container(
                                        margin: EdgeInsets.only(left: 11.0),
                                        width: 78.0,
                                        child: TextFormField(
//                                          validator: textValidator,
                                          style: textStyle,
                                          keyboardType: TextInputType.number,
                                          textAlign: TextAlign.center,
                                          decoration: inputDecoration.copyWith(
                                              hintText: 'Домофон'),
                                          onSaved: (value) {
                                            _formData['addressIntercom'] =
                                                value;
                                          },
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20.0),
                              width: double.infinity,
                              child: TextFormField(
//                                validator: textValidator,
                                style: textStyle,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.start,
                                decoration: inputDecoration.copyWith(
                                    hintText: 'Примечание к адресу'),
                                onSaved: (value) {
                                  _formData['addressDescription'] = value;
                                },
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    top: 5.0, left: 6.0, right: 6.0),
                                child: Text(
                                  'Например: “Последний до слева в переулке, в конце переулка тупик”',
                                  style: TextStyle(
                                    fontSize: 11.0,
                                    color: Color(0xffBBBBBB),
                                    fontStyle: FontStyle.italic,
                                    fontFamily: 'Montserrat',
                                  ),
                                )),
                            Container(
                              margin: EdgeInsets.only(top: 20.0),
                              width: double.infinity,
                              child: TextFormField(
                                validator: textValidator,
                                style: textStyle,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.start,
                                decoration: inputDecoration.copyWith(
                                    hintText: 'Ваше имя*'),
                                onSaved: (value) {
                                  _formData['clientName'] = value;
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 20.0, left: 6.0, right: 6.0),
                              alignment: AlignmentDirectional.centerStart,
                              child: Text(
                                'Время доставки',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    color: Color(0xff909090)),
                              ),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () async {
                                final timeOfDay = await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.now());
                                viewModel.updateDeliveryTime(timeOfDay);
                              },
                              child: Container(
                                height: 40.0,
                                margin: EdgeInsets.only(top: 5.0),
                                width: double.infinity,
                                decoration: boxDecoration,
                                alignment: AlignmentDirectional.centerStart,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      ListenableProvider<
                                          ValueNotifier<DateTime>>(
                                        builder: (context) =>
                                            viewModel.deliveryTime,
                                        child:
                                            Consumer<ValueNotifier<DateTime>>(
                                          builder: (context,
                                                  ValueNotifier<DateTime>
                                                      deliveryTime,
                                                  widget) =>
                                              Text(
                                                  deliveryTime.value == null
                                                      ? 'Ближайшее время'
                                                      : DateFormat.Hm().format(
                                                          deliveryTime.value),
                                                  style: TextStyle(
                                                    fontSize: 11.0,
                                                    color: Color(0xff1E2432),
                                                    fontFamily: 'Montserrat',
                                                  )),
                                        ),
                                      ),
                                      SvgPicture.asset(
                                          'assets/svg/arrow_dropdown.svg')
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20.0),
                              width: double.infinity,
                              child: TextFormField(
//                                validator: textValidator,
                                style: textStyle,
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.start,
                                decoration: inputDecoration.copyWith(
                                    hintText: 'Пожелания к заказу'),
                                onSaved: (value) {
                                  _formData['comment'] = value;
                                },
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                    top: 5.0,
                                    left: 6.0,
                                    right: 6.0,
                                    bottom: 88.0),
                                child: Text(
                                  'Например: “Бананы поспелее, а зубную щетку синего цвета”.',
                                  style: TextStyle(
                                    fontSize: 11.0,
                                    color: Color(0xffBBBBBB),
                                    fontStyle: FontStyle.italic,
                                    fontFamily: 'Montserrat',
                                  ),
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Material(
                  elevation: 2.0,
                  color: Colors.white,
                  child: Container(
                    height: 88.0,
                    child: Container(
                      alignment: AlignmentDirectional.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => Navigator.pop(context),
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Image.asset('assets/app_bar_back.png'),
                            ),
                          ),
                          Text(
                            'Оформление заказа',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(15.0),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: AlignmentDirectional.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 15.0),
                    child: Material(
                      color: Colors.white,
                      elevation: 2.0,
                      child: Padding(
                        child: GestureDetector(
                          onTap: () async {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              final orderConfirmResult = await viewModel
                                  .onOrderProcessClick(_formData);
                              if (orderConfirmResult.orderConfirmResult ==
                                  OrderConfirmResult.NO_CARD) {
                                final cardId = await Navigator.of(context,
                                        rootNavigator: true)
                                    .pushNamed('/cardAttach');
                                final orderId = orderConfirmResult.orderId;
                                final paymentResult = await viewModel
                                    .onCardAttached(cardId, orderId);
                                if (paymentResult.orderConfirmResult ==
                                    OrderConfirmResult.SUCCESS) {
                                  _moveToSuccessPage(context, paymentResult);
                                } else {
                                  _moveToFailedPage(context);
                                }
                              } else if (orderConfirmResult
                                      .orderConfirmResult ==
                                  OrderConfirmResult.SUCCESS) {
                                _moveToSuccessPage(context, orderConfirmResult);
                              } else {
                                _moveToFailedPage(context);
                              }
                            }
                          },
                          child: SafeArea(
                            top: false,
                            child: Container(
                              height: 44.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Color(0xff54B24C),
                                  borderRadius: BorderRadius.circular(5.0)),
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12.0, bottom: 11.0),
                                  child: ListenableProvider.value(
                                    value: getIt<CartViewModel>(),
                                    child: Consumer<CartViewModel>(
                                      builder: (BuildContext context,
                                              CartViewModel cartViewModel,
                                              Widget widget) =>
                                          RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(
                                          text: 'К оплате ',
                                          style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 17.0,
                                            color: Colors.white,
                                          ),
                                          children: <TextSpan>[
                                            TextSpan(
                                                text:
                                                    '${cartViewModel.cartPriceViewModel.totalPrice.toStringAsFixed(2)} ₽',
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 17.0,
                                                  color: Colors.white,
                                                ))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        padding: EdgeInsets.only(
                            left: 10, right: 10, top: 13.0, bottom: 13.0),
                      ),
                    ),
                  ),
                ),
                ListenableProvider<ValueNotifier<bool>>(
                  child: Consumer<ValueNotifier<bool>>(
                    builder: (BuildContext context,
                            ValueNotifier<bool> isProgress, Widget child) =>
                        Visibility(
                      visible: isProgress.value,
                      child: Container(
                        color: Colors.black.withAlpha(200),
                        child: Align(
                          alignment: AlignmentDirectional.center,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Color(0xFFFFA300)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  builder: (context) => viewModel.isProgress,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  void _moveToFailedPage(BuildContext context) {
    Navigator.of(context).pushReplacementNamed("/orderFailed");
  }

  void _moveToSuccessPage(
      BuildContext context, OrderConfirmResultBundle orderConfirmResult) {
    Navigator.of(context).pushReplacementNamed("/orderSuccess",
        arguments: OrderSuccessArguments(orderConfirmResult.orderId));
    getIt<OrdersViewModel>().loadOrders();
  }
}

class OrderAssembleArguments {
  final int shopId;
  final double price;

  OrderAssembleArguments(this.shopId, this.price);
}
