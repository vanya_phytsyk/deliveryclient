import 'package:delivery_client/main/order/OrderDetailsView.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderSuccessView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final OrderSuccessArguments arguments =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: Padding(
        padding: const EdgeInsets.only(top: 88.0),
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: AlignmentDirectional.center,
                width: 216.0,
                height: 216.0,
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                child: Image.asset('assets/icon-check.png'),
              ),
              Container(
                margin: EdgeInsets.only(top: 40.0),
                alignment: AlignmentDirectional.center,
                child: Text('Заказ №${arguments.orderId}\n успешно оформлен',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff1E2432),
                      fontFamily: 'Montserrat',
                    )),
              ),
              Container(
                  width: 318.0,
                  margin: EdgeInsets.only(top: 15.0),
                  child: Text(
                      'Когда курьер доставит ваш заказ покажите ему QR код для оплаты',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17.0,
                        color: Color(0xff1E2432),
                        fontFamily: 'Montserrat',
                      ))),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  OrderDetailsArguments args =
                      OrderDetailsArguments(arguments.orderId);
                  Navigator.of(context)
                      .pushReplacementNamed('/orderDetails', arguments: args);
//                  Navigator.of(context).pop();
                },
                child: Container(
                    width: 230.0,
                    margin: EdgeInsets.only(top: 20.0),
                    decoration: BoxDecoration(
                        color: Color(0xffFFA300),
                        borderRadius: BorderRadius.circular(5.0)),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0, bottom: 11.0),
                      child: Text('Детали заказа',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0,
                            color: Color(0xff0A1F44),
                          )),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class OrderSuccessArguments {
  final int orderId;

  OrderSuccessArguments(this.orderId);
}
