import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/main/profile/AttachedCardViewModel.dart';

class CardAttachViewModel {
  final WebService _webService;
  final AttachedCardViewModel _attachedCardViewModel;

  CardAttachViewModel(this._webService, this._attachedCardViewModel);

  Future<CardAttachResult> onProcessCardData(
      Map<String, String> formData) async {
    try {
      final response = await _webService.addPaymentCard(formData['number'],
          formData['valid'], formData['cvv']);
      this._attachedCardViewModel.loadCard();
      return CardAttachResult()..cardId = response.result;
    } catch (e) {
      return CardAttachResult()..isError = true;
    }
  }
}

class CardAttachResult {
  bool isError = false;
  int cardId;
}
