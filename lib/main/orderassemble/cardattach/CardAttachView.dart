import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/orderassemble/cardattach/CardAttachViewModel.dart';
import 'package:delivery_client/main/profile/AttachedCardViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class CardAttachView extends StatefulWidget {
  @override
  _CardAttachViewState createState() => _CardAttachViewState();
}

class _CardAttachViewState extends State<CardAttachView> {
  final _formKey = GlobalKey<FormState>();

  final _formData = <String, String>{};

  final CardAttachViewModel _cardAttachViewModel =
      CardAttachViewModel(getIt<WebService>(), getIt<AttachedCardViewModel>());

  final numberTextController =
      MaskedTextController(mask: '0000 0000 0000 0000');
  final cvvTextController = MaskedTextController(mask: '000');
  final validTextController = MaskedTextController(mask: '00/00');

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.white,
      border: BorderDirectional(
          bottom: BorderSide(color: Color(0x4D000000), width: 1.0)),
    );
    var textStyle = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff1E2432));
    var hintStyle2 = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff909090));

    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            reverse: true,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 88.0),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 25.0, left: 15.0, right: 15.0),
                    child: Text(
                      'С карты будет списан 1 ₽ для проверки ее данных. Сумма будет возвращена сразу после проверки.\n\nМы не принимаем карты Maestro, так как карты данного типа не поддерживают рекуррентные платежи.',
                      style: TextStyle(
                          color: Color(0xff909090),
                          fontSize: 12.0,
                          fontFamily: 'Montserrat'),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0),
                  child: Text(
                    'Необходимая сумма для оплаты вашего заказа списывается с карты, когда заказ доставлен курьерской службой по указанному адресу.',
                    style: TextStyle(
                        color: Color(0xffFD4653),
                        fontSize: 14.0,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Montserrat'),
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Container(
                    margin: EdgeInsets.only(top: 24.0, left: 15.0, right: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: Container(
                              decoration: boxDecoration,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center,
                                    onSaved: (value) {
                                      _formData['number'] =
                                          numberTextController.text;
                                    },
                                    style: textStyle,
                                    controller: numberTextController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        contentPadding: EdgeInsets.all(10.0),
                                        hintText: 'XXXX XXXX XXXX XXXX',
                                        hintStyle: hintStyle2),
                                  )),
                                  Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                      child:
                                          Image.asset('assets/credit-card.png'))
                                ],
                              )),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                              margin: EdgeInsets.only(left: 11.0),
                              width: 78.0,
                              decoration: boxDecoration,
                              child: TextFormField(
                                style: textStyle,
                                onSaved: (value) {
                                  _formData['valid'] = validTextController.text;
                                },
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                controller: validTextController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(10.0),
                                    hintText: 'MM/ГГ',
                                    hintStyle: hintStyle2),
                              )),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                              margin: EdgeInsets.only(left: 11.0),
                              width: 78.0,
                              decoration: boxDecoration,
                              child: TextFormField(
                                style: textStyle,
                                onSaved: (value) {
                                  _formData['cvv'] = cvvTextController.text;
                                },
                                keyboardType: TextInputType.number,
                                controller: cvvTextController,
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.all(10.0),
                                    hintText: 'CVV',
                                    hintStyle: hintStyle2),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0, bottom: 88.0),
                  child: Image.asset('assets/credit_cards_accepted.png'),
                )
              ],
            ),
          ),
          Material(
            elevation: 2.0,
            color: Colors.white,
            child: Container(
              height: 88.0,
              child: Container(
                alignment: AlignmentDirectional.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => Navigator.pop(context),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Image.asset('assets/app_bar_back.png'),
                      ),
                    ),
                    Text(
                      'Оформление заказа',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15.0),
                    )
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: SafeArea(
              child: Container(
                margin: EdgeInsets.only(top: 15.0),
                child: Material(
                  color: Colors.white,
                  elevation: 2.0,
                  child: Padding(
                    child: GestureDetector(
                      onTap: () async {
                        _formKey.currentState.save();
                        if (_formData['number'] != null &&
                            _formData['valid'] != null &&
                            _formData['cvv'] != null) {
                          final result = await _cardAttachViewModel
                              .onProcessCardData(_formData);
                          if (result.isError) {
                            Navigator.of(context, rootNavigator: true)
                                .pushNamed('/orderFailed');
                          } else {
                            Navigator.of(context).pop(result.cardId);
                          }
                        }
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0xff54B24C),
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Padding(
                          padding:
                              const EdgeInsets.only(top: 12.0, bottom: 11.0),
                          child: Text(
                            'Далее',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w600,
                              fontSize: 17.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    padding: EdgeInsets.only(
                        left: 10, right: 10, top: 13.0, bottom: 13.0),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
