import 'dart:ui';

import 'package:delivery_client/domain/models/Order.dart';
import 'package:flutter/material.dart';

class OrderItemViewModel {
  Order order;

  OrderItemViewModel(this.order);

  static Color getStatusColor(OrderStatus orderStatus) {
    switch (orderStatus) {
      case OrderStatus.NEW:
        return Color(0xff54B24C);
      case OrderStatus.INWORK:
        return Color(0xffFFA300);
      case OrderStatus.COMPLETED:
      default:
        return Color(0xff1E2432);
    }
  }

  static Color getBackgroundColor(OrderStatus orderStatus) {
    switch (orderStatus) {
      case OrderStatus.COMPLETED:
        return Color(0xffD1FFD0);
      case OrderStatus.CANCELLED:
        return Color(0xffFFD0D0);
      case OrderStatus.NEW:
      default:
        return Colors.white;
    }
  }

  static String getDeliveryStatusText(OrderStatus orderStatus) {
    switch (orderStatus) {
      case OrderStatus.COMPLETED:
        return 'Доставлен';
      case OrderStatus.CANCELLED:
        return 'Отменен';
      case OrderStatus.NEW:
        return 'Новый';
      default:
        return 'На доставке';
    }
  }
}
