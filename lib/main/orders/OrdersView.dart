import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/order/OrderDetailsView.dart';
import 'package:delivery_client/main/orders/OrderItemViewModel.dart';
import 'package:delivery_client/main/orders/OrdersViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'EmptyOrdersView.dart';

class OrdersView extends StatelessWidget {
  OrdersViewModel _ordersViewModel = getIt<OrdersViewModel>();

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0xffF2F2F2),
      child: ListenableProvider<ValueNotifier<List<OrderItemViewModel>>>(
        builder: (BuildContext context) {
          return _ordersViewModel.orders;
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 88.0, bottom: 5.0),
              child: Consumer<ValueNotifier<List<OrderItemViewModel>>>(
                  builder: (_t, ValueNotifier<List<OrderItemViewModel>> orders,
                          _) =>
                      IndexedStack(
                        index: orders.value?.isNotEmpty == true ? 0 : 1,
                        children: <Widget>[
                          ListView.builder(
                            padding: EdgeInsets.zero,
                            itemCount: orders.value?.length ?? 0,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () async {
                                  OrderDetailsArguments arguments =
                                      OrderDetailsArguments(
                                          orders.value[index].order.id);
                                  await Navigator.of(context,
                                          rootNavigator: true)
                                      .pushNamed('/orderDetails',
                                          arguments: arguments);
                                },
                                child: Container(
                                    margin: EdgeInsets.only(
                                        top: 5.0, left: 5.0, right: 5.0),
                                    decoration: BoxDecoration(
                                        color: OrderItemViewModel
                                            .getBackgroundColor(orders
                                                .value[index]
                                                .order
                                                .orderStatus),
                                        border: Border.all(
                                            color: Color(0xffEAEAEA)),
                                        borderRadius:
                                            BorderRadius.circular(3.0)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Container(
                                            margin:
                                                EdgeInsets.only(right: 10.0),
                                            width: 134.0,
                                            height: 60.0,
                                            child: Image.network(orders
                                                .value[index].order.shopLogo),
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Заказ №${orders.value[index].order.number}',
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontFamily: 'Montserrat',
                                                    color: Color(0xff1E2432)),
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Container(
                                                    width: 95.0,
                                                    margin: EdgeInsets.only(
                                                        right: 2.0),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                            'Товаров:'.padRight(
                                                                40, '.'),
                                                            softWrap: false,
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff1E2432),
                                                              fontSize: 11.0,
                                                              fontFamily:
                                                                  'Montserrat',
                                                            )),
                                                        Text(
                                                            'Итого:'.padRight(
                                                                40, '.'),
                                                            softWrap: false,
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff1E2432),
                                                              fontSize: 11.0,
                                                              fontFamily:
                                                                  'Montserrat',
                                                            )),
                                                        Text(
                                                            'Статус:'.padRight(
                                                                40, '.'),
                                                            softWrap: false,
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff1E2432),
                                                              fontSize: 11.0,
                                                              fontFamily:
                                                                  'Montserrat',
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    verticalDirection:
                                                        VerticalDirection.down,
                                                    children: <Widget>[
                                                      Text(
                                                          orders
                                                              .value[index]
                                                              .order
                                                              .productsCount
                                                              .toStringAsFixed(
                                                                  0),
                                                          softWrap: false,
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xff1E2432),
                                                            fontSize: 11.0,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontFamily:
                                                                'Montserrat',
                                                          )),
                                                      Text(
                                                          orders.value[index]
                                                              .order.price
                                                              .toStringAsFixed(
                                                                  2) + ' ₽',
                                                          softWrap: false,
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            color: Color(
                                                                0xff1E2432),
                                                            fontSize: 11.0,
                                                            fontFamily:
                                                                'Montserrat',
                                                          )),
                                                      Text(
                                                          OrderItemViewModel
                                                              .getDeliveryStatusText(
                                                                  orders
                                                                      .value[
                                                                          index]
                                                                      .order
                                                                      .orderStatus),
                                                          softWrap: false,
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            color: OrderItemViewModel
                                                                .getStatusColor(orders
                                                                    .value[
                                                                        index]
                                                                    .order
                                                                    .orderStatus),
                                                            fontSize: 11.0,
                                                            fontFamily:
                                                                'Montserrat',
                                                          )),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    )),
                              );
                            },
                          ),
                          EmptyOrdersView()
                        ],
                      )),
            ),
            Material(
              elevation: 2.0,
              color: Colors.white,
              child: Container(
                height: 88.0,
                child: Container(
                    margin: EdgeInsets.only(bottom: 12.0),
                    alignment: AlignmentDirectional.bottomCenter,
                    child: Text(
                      'Мои заказы',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
