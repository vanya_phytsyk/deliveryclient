import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/Order.dart';
import 'package:flutter/foundation.dart';
import 'dart:math';

import 'OrderItemViewModel.dart';

class OrdersViewModel with ChangeNotifier {
  ValueNotifier<List<OrderItemViewModel>> orders = ValueNotifier(null);

  final WebService _webService;

  OrdersViewModel(this._webService) {
    loadOrders();
  }

  void loadOrders() async {
    final response = await _webService.getOrders();

    final orders = response.records
        .map((record) => Order(
            record.sid,
            record.sid.toString(),
            record.goodsCount.toDouble(),
            record.goodsPrice.toDouble() + record.deliveryCost.toDouble(),
            record.shopLogo,
            getOrderStatus(record.status)))
        .map((order) => OrderItemViewModel(order))
        .toList()
          ..sort(
              (order1, order2) => order2.order.id.compareTo(order1.order.id));
    this.orders.value = orders;
  }

  static OrderStatus getOrderStatus(String statusString) {
    switch (statusString) {
      case "NEW":
        return OrderStatus.NEW;
      case "CANCELLED":
        return OrderStatus.CANCELLED;
      case "COMPLETED":
        return OrderStatus.COMPLETED;
      case "INWORK":
        return OrderStatus.INWORK;
      case "WAITCOURIER":
        return OrderStatus.WAITCOURIER;
      case "WAITDELIVERY":
        return OrderStatus.WAITDELIVERY;
      default:
        return OrderStatus.DELIVERY;
    }
  }
}
