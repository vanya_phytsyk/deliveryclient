import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Category.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/domain/models/ProductSpecial.dart';
import 'package:delivery_client/main/cart/CartHolder.dart';
import 'package:delivery_client/main/cart/CartView.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/discountproducts/DiscountProductsViewModel.dart';
import 'package:delivery_client/main/main_screen.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsView.dart';
import 'package:delivery_client/main/products/ProductsView.dart';
import 'package:delivery_client/main/products/ProductsViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class DiscountProductsView extends StatefulWidget {
  @override
  _DiscountProductsViewState createState() => _DiscountProductsViewState();
}

class _DiscountProductsViewState extends State<DiscountProductsView> {
  int _currentIndex = 0;

  DiscountProductsViewModel _productsViewModel;

  @override
  Widget build(BuildContext context) {
    DiscountProductsArguments arguments =
        ModalRoute.of(context).settings.arguments;
    if (_productsViewModel == null) {
      _productsViewModel = DiscountProductsViewModel(
          arguments, getIt<WebService>(), getIt<CartViewModel>());

      _currentIndex = arguments.discounts
          .indexWhere((sub) => sub.id == arguments.selectedDiscountId);
    }

    return DefaultTabController(
      initialIndex: _currentIndex,
      child: Builder(
        builder: (BuildContext context) {
          var controller = DefaultTabController.of(context);
          controller.addListener(() {
            if (controller.index != _currentIndex &&
                !controller.indexIsChanging) {
              setState(() {
                _currentIndex = controller.index;
              });
            }
          });
          return Stack(
            children: <Widget>[
              TabBarView(
                children: <Widget>[
                  ...arguments.discounts
                      .map((cat) => buildProductsGridView(cat.id))
                ],
              ),
              Material(
                elevation: 2.0,
                color: Colors.white,
                child: Container(
                  height: 115.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Padding(
                          child: Row(
                            children: <Widget>[
                              Image.asset('assets/app_bar_back.png'),
                              Container(
                                margin: EdgeInsets.only(left: 28.0),
                                child: Text(
                                  'Акции',
                                  style: TextStyle(
                                      color: Color(0xff1E2432),
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Montserrat'),
                                ),
                              )
                            ],
                          ),
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 8.0),
                        ),
                      ),
                      TabBar(
                        indicatorColor: Colors.transparent,
                        isScrollable: true,
                        tabs: <Widget>[
                          ...arguments.discounts.map((discount) =>
                              buildTab(
                                  discount.name,
                                  arguments.discounts
                                          .indexOf(discount) ==
                                      _currentIndex))
                        ],
                        onTap: (index) {
                          setState(() {
                            this._currentIndex = index;
                          });
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
      length: arguments.discounts.length,
    );
  }

  Widget buildProductsGridView(int categoryId) {
    return ListenableProvider<ValueNotifier<List<CartItem>>>(
      builder: (BuildContext context) =>
          _productsViewModel.products[categoryId],
      child: Consumer<ValueNotifier<List<CartItem>>>(
        builder: (BuildContext context, ValueNotifier<List<CartItem>> products,
                Widget child) =>
            Stack(
          children: <Widget>[
            Padding(
              key: UniqueKey(),
              padding: EdgeInsets.only(top: 115.0),
              child: GridView.builder(
                itemCount: products.value?.length ?? 0,
                itemBuilder: (BuildContext context, int index) =>
                    ProductCardView(products.value[index], _productsViewModel),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 0.95),
              ),
            ),
            Center(
              child: Visibility(
                visible: products.value?.isEmpty ?? false,
                child: Text('Нет итемов',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 12.0,
                        color: Color(0xff1E2432))),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildTab(String text, bool isSelected) => Container(
      constraints: BoxConstraints(minHeight: 30.0, maxWidth: 110),
      margin: EdgeInsets.only(bottom: 4.0),
      alignment: AlignmentDirectional.center,
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.visible,
        textAlign: TextAlign.center,
        maxLines: 2,
        style: TextStyle(
            fontSize: 12.0,
            fontFamily: 'Montserrat',
            color: isSelected ? Color(0xffFFA300) : Color(0xff1E2432)),
      ));
}

class DiscountProductsArguments {
  final List<ProductSpecial> discounts;
  final int selectedDiscountId;

  DiscountProductsArguments(this.discounts, this.selectedDiscountId);

}
