import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/main/cart/CartHolder.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/foundation.dart';

import 'DiscountProductsView.dart';

class DiscountProductsViewModel with ChangeNotifier implements CartHolder {
  final DiscountProductsArguments _arguments;
  final WebService _webService;

  final Map<int, ValueNotifier<List<CartItem>>> products = {};

  final CartViewModel _cartViewModel;

  DiscountProductsViewModel(this._arguments, this._webService, this._cartViewModel) {
    _arguments.discounts
        .forEach((discount) => products[discount.id] = ValueNotifier(null));
    _loadProducts();
    _cartViewModel.addListener(() {
      _loadProducts();
    });
  }

  _loadProducts() {
    _arguments.discounts
        .forEach((discount) => _loadProductsForDiscount(discount.id));
  }

  _loadProductsForDiscount(int discountId) async {
    final productsResponse =
        await _webService.getDiscountProducts(discountId);

    final categoryProducts = productsResponse.records
        .map((record) => Product(
            record.goodId,
            record.goodName,
            record.goodImg,
            record.goodPrice.toDouble(),
            record.goodDiscountPrice.toDouble(),
            record.goodUnit,
            record.goodUnitStep.toDouble()))
        .map((product) =>
            _cartViewModel.getCartItem(product) ?? CartItem(product, 0))
        .toList();
    if (products[discountId].value == categoryProducts) {
      products[discountId].notifyListeners();
    } else {
      products[discountId].value = categoryProducts;
    }
  }

  void addItemToCart(CartItem product) {
    _cartViewModel.addItemToCart(product);
  }

  void removeItemFromCart(CartItem product) {
    _cartViewModel.removeItemFromCart(product);
  }
}
