import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/domain/models/ProductDetails.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/cupertino.dart';

class ProductDetailsViewModel {
  ValueNotifier<ProductDetails> productDetails = ValueNotifier(null);

  final WebService _webService;

  final CartViewModel _cartViewModel;

  ProductDetailsViewModel(this._webService, this._cartViewModel) {}

  CartItem cartItem;

  int _productId;

  loadProduct(int productId) async {
    this._productId = productId;
    cartItem = _getCartItem();

    final response = await _webService.getProductDetails(productId);

    final weight = double.parse(response.result.goodAttrs
        .firstWhere((att) => att.attrId == 18)
        .attrValue);

    final description = response.result.goodAttrs
        .firstWhere((att) => att.attrId == 19)
        .attrValue;

    final brand = response.result.brandName;

    final producer = response.result.goodAttrs
        .firstWhere((att) => att.attrId == 21)
        .attrValue;

    final holdTermin = response.result.goodAttrs
        .firstWhere((att) => att.attrId == 22)
        .attrValue;

    final holdConditions = response.result.goodAttrs
        .firstWhere((att) => att.attrId == 23)
        .attrValue;

    final price = double.parse(response.result.goodAttrs
        .firstWhere((att) => att.attrId == 20)
        .attrValue).toDouble();

    final unit = response.result.goodAttrs
        .firstWhere((att) => att.attrId == 24)
        .attrValue;

    final unitStep = double.parse(response.result.goodAttrs
        .firstWhere((att) => att.attrId == 25)
        .attrValue);

    var product = Product(response.result.goodId, response.result.goodName,
        response.result.goodImg, price, response.result.goodDiscountPrice.toDouble(), unit, unitStep);
    final productDetails = ProductDetails(
        product,
        response.result.goodImages.map((image) => image.photoUrl).toList(),
        description,
        weight,
        brand,
        producer,
        holdTermin,
        holdConditions,
        description);

    cartItem ??= CartItem(product, 0);

    this.productDetails.value = productDetails;
  }

  CartItem _getCartItem() {
    if (_productId == null) return null;
    return _cartViewModel.cartItems.firstWhere(
        (item) => item.product.id == _productId,
        orElse: () => null);
  }

  void addItemToCart() {
    if (cartItem == null) return;
    _cartViewModel.addItemToCart(cartItem);
  }

  void removeItemFromCart() {
    if (cartItem == null) return;
    _cartViewModel.removeItemFromCart(cartItem);
  }
}
