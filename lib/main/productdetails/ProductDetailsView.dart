import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/ProductDetails.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsViewModel.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../main_screen.dart';

class ProductDetailsView extends StatelessWidget {
  final ProductDetailsViewModel _productDetailsViewModel =
      ProductDetailsViewModel(getIt<WebService>(), getIt<CartViewModel>());

  @override
  Widget build(BuildContext context) {
    ProductArguments productArguments =
        ModalRoute.of(context).settings.arguments;
    _productDetailsViewModel.loadProduct(productArguments._productId);

    return ListenableProvider.value(
      value: _productDetailsViewModel.productDetails,
      child: Consumer<ValueNotifier<ProductDetails>>(
        builder: (BuildContext context, ValueNotifier<ProductDetails> product,
                Widget child) =>
            Scaffold(
          backgroundColor: Color(0xffF7F8FA),
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                if (product.value != null)
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Material(
                          elevation: 1.0,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 55.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 200.0,
                                  child: DefaultTabController(
                                      length: product.value?.photos?.length ?? 0,
                                      child: Stack(
                                        children: <Widget>[
                                          if (product.value != null)
                                            TabBarView(
                                              children: product.value?.photos
                                                  ?.map((photo) =>
                                                      Image.network(photo))
                                                  ?.toList(),
                                            ),
                                          Align(
                                              alignment: AlignmentDirectional
                                                  .bottomCenter,
                                              child: TabPageSelector(
                                                color: Colors.white,
                                                indicatorSize: 10.0,
                                                selectedColor: Color(0xffFFA300),
                                              )),
                                        ],
                                      )),
                                ),
                                Container(
                                    margin: EdgeInsets.only(top: 15.0),
                                    width: 280.0,
                                    child: Text(
                                      product.value?.product?.name,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Color(0xff1E2432),
                                        fontFamily: 'Montserrat',
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    )),
                                Container(
                                  margin: EdgeInsets.only(top: 5.0),
                                  child: Text(
                                      "${product.value?.weight?.toString()} ${product.value.product.unit}",
                                      style: TextStyle(
                                        color: Color(0xffB8BBC6),
                                        fontFamily: 'Montserrat',
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500,
                                      )),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(top: 14.0, bottom: 17.0),
                                  child: Text(
                                      '${product.value?.product?.discountPrice ?? product.value?.product?.price} ₽/${product.value.product.unit}',
                                      style: TextStyle(
                                        color: Color(0xff1E2432),
                                        fontFamily: 'Montserrat',
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w600,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(
                                top: 40.0, left: 16.0, bottom: 11.0),
                            alignment: AlignmentDirectional.centerStart,
                            child: Text(
                              'Описание',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Color(0xff909090),
                                fontFamily: 'Montserrat',
                              ),
                            )),
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  top: BorderSide(color: Color(0xffE2E2E2)),
                                  bottom:
                                      BorderSide(color: Color(0xffE2E2E2)))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Text(
                              product.value?.description,
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff1E2432),
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 40.0, left: 16.0),
                            alignment: AlignmentDirectional.centerStart,
                            child: Text(
                              'Подробнее',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Color(0xff909090),
                                fontFamily: 'Montserrat',
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.only(top: 11.0, bottom: 96.0),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  top: BorderSide(color: Color(0xffE2E2E2)))),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              buildInfoItem('Бренд', product.value?.brand),
                              buildInfoItem(
                                  'Производитель', product.value?.producer),
                              buildInfoItem(
                                  'Срок хранения', product.value?.holdTermin),
                              buildInfoItem('Условия хранения',
                                  product.value?.holdConditions),
                              buildInfoItem('Состав', product.value?.consist),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop(BackResult(false));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, left: 20.0),
                        child: Image.asset(
                            'assets/close_details.png'),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        Navigator.of(context, rootNavigator: true)
                            .pop(BackResult(true));
                      },
                      child: Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, right: 20.0),
                          child: ListenableProvider.value(
                            value:
                            getIt<CartViewModel>().cartAmount,
                            child: Consumer<ValueNotifier<int>>(
                              builder: (BuildContext context,
                                  ValueNotifier<int>
                                  cartAmount,
                                  Widget child) =>
                                  Badge(
                                      badgeColor:
                                      Color(0xffFF4D00),
                                      position: BadgePosition
                                          .bottomLeft(left: -7.0),
                                      showBadge:
                                      cartAmount.value > 0,
                                      badgeContent: Text(
                                        cartAmount.value
                                            .toString(),
                                        style: TextStyle(
                                            fontSize: 10.0,
                                            color: Colors.white,
                                            fontFamily:
                                            'Montserrat',
                                            fontWeight:
                                            FontWeight.w500),
                                      ),
                                      child: SvgPicture.asset(
                                        'assets/svg/cart.svg',
                                        color: Color(0xffFFA300),
                                      )),
                            ),
                          )),
                    ),
                  ],
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                ),

                Align(
                  alignment: AlignmentDirectional.bottomCenter,
                  child: Material(
                    elevation: 1.0,
                    color: Colors.white,
                    child: Container(
                        height: 70.0,
                        child: BottonSection(
                          _productDetailsViewModel,
                          product.value,
                          key: UniqueKey(),
                        )),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container buildInfoItem(String title, String text) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Color(0xffE2E2E2)))),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 15.0),
            width: double.infinity,
            child: Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Color(0xffFFA300),
                fontSize: 10.0,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0, top: 1.0),
            width: double.infinity,
            child: Text(
              text,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Color(0xff1E2432),
                fontSize: 12.0,
              ),
            ),
          ),
        ],
      ),
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
    );
  }
}

class BottonSection extends StatefulWidget {
  final ProductDetailsViewModel _productDetailsViewModel;
  final ProductDetails _productDetails;

  BottonSection(
    this._productDetailsViewModel,
    this._productDetails, {
    Key key,
  }) : super(key: key);

  @override
  _BottonSectionState createState() =>
      _BottonSectionState(_productDetailsViewModel, _productDetails);
}

class _BottonSectionState extends State<BottonSection> {
  var _itemsInCart = 0.0;

  final ProductDetailsViewModel _productDetailsViewModel;
  final ProductDetails _productDetails;

  _BottonSectionState(this._productDetailsViewModel, this._productDetails) {
    _itemsInCart = _productDetailsViewModel.cartItem?.amount ?? 0.0;
  }

  @override
  Widget build(BuildContext context) {
    if (_itemsInCart == 0) {
      return Padding(
        child: GestureDetector(
          onTap: () {
            setState(() {
              _itemsInCart += _productDetails.product.unitStep;
              _productDetailsViewModel.addItemToCart();
            });
          },
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xffFFA300),
                borderRadius: BorderRadius.circular(5.0)),
            child: Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 11.0),
              child: Text('Добавить в корзину',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600,
                    fontSize: 17.0,
                    color: Color(0xff0A1F44),
                  )),
            ),
          ),
        ),
        padding: EdgeInsets.only(left: 10, right: 10, top: 13.0, bottom: 13.0),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  _itemsInCart += _productDetails.product.unitStep;
                  _productDetailsViewModel.addItemToCart();
                });
              },
              child: Container(
                  alignment: AlignmentDirectional.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: Color(0xffFFA300)),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Image.asset(
                      'assets/increment_count_large.png',
                      width: 12.0,
                      height: 12.0,
                    ),
                  )),
            ),
            Container(
              width: 107.0,
              alignment: AlignmentDirectional.center,
              child: Text(
                  '${_itemsInCart.toStringAsFixed(2)} ${_productDetails?.product?.unit ?? ''}',
                  style: TextStyle(
                      color: Color(0xffFFA300),
                      fontFamily: 'Montserrat',
                      fontSize: 17.0)),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _itemsInCart -= _productDetails.product.unitStep;
                  _productDetailsViewModel.removeItemFromCart();
                  if (_itemsInCart <= 0.00001) {
                    _itemsInCart = 0.0;
                  }
                });
              },
              child: Container(
                  alignment: AlignmentDirectional.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: Color(0xffFFA300)),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Image.asset(
                      'assets/decrement_count_large.png',
                      width: 12.0,
                      height: 12.0,
                    ),
                  )),
            ),
            Expanded(
              child: Text(
                '${((_productDetails?.product?.discountPrice ?? _productDetails?.product?.price ?? 0) * _itemsInCart).toStringAsFixed(2)} ₽',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xff1E2432),
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600,
                  fontSize: 17.0,
                ),
              ),
            )
          ],
        ),
      );
    }
  }
}

class ProductArguments {
  final int _productId;

  ProductArguments(this._productId);
}

class BackResult {
  final bool showCart;

  BackResult(this.showCart);
}
