import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsView.dart';
import 'package:delivery_client/main/products/ProductsView.dart';
import 'package:delivery_client/main/products/ProductsViewModel.dart';
import 'package:delivery_client/main/productsspecial/ProductsSpecialView.dart';
import 'package:delivery_client/main/search/SearchViewModel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  final _searchQuery = new TextEditingController();

  SearchViewModel _searchViewModel;

  @override
  void initState() {
    super.initState();
    _searchQuery.addListener(_onSearchChanged);
  }

  void _onSearchChanged() {
    final text = _searchQuery.text;
    _searchViewModel.search(text);
  }

  @override
  Widget build(BuildContext context) {
    final SearchArguments shopArguments =
        ModalRoute.of(context).settings.arguments;
    _searchViewModel ??= SearchViewModel(
        getIt<WebService>(), getIt<CartViewModel>(), shopArguments.shopId);

    return Scaffold(
      backgroundColor: Color(0xffF7F8FA),
      body: Material(
        child: Stack(
          children: <Widget>[
            Material(
              elevation: 2.0,
              color: Colors.white,
              child: Container(
                height: 92.0,
                alignment: AlignmentDirectional.bottomCenter,
                child: Container(
                  height: 34.0,
                  margin: EdgeInsets.only(left: 15.0, bottom: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Theme(
                          data: ThemeData(primaryColor: Color(0xff8B8A8A)),
                          child: TextField(
                            controller: _searchQuery,
                            decoration: InputDecoration(
                              prefixIcon: SvgPicture.asset(
                                'assets/svg/search-loop.svg',
                                width: 16.0,
                                height: 16.0,
                                fit: BoxFit.none,
                              ),
                              filled: true,
                              fillColor: Color(0xffEDEDED),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xff8B8A8A), width: 1.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              contentPadding: EdgeInsets.all(8.0),
                            ),
                            cursorColor: Color(0xff8B8A8A),
                            style: TextStyle(
                                color: Color(0xff1E2432),
                                fontSize: 14.0,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () => Navigator.of(context).pop(),
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 18.0, right: 18.0),
                          child: Text(
                            'Отменить',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xffFFA300),
                                fontSize: 14.0,
                                fontFamily: 'Montserrat'),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 115.0),
              child: ListenableProvider<ValueNotifier<List<CartItem>>>(
                builder: (context) => _searchViewModel.products,
                child: Consumer<ValueNotifier<List<CartItem>>>(
                  builder: (BuildContext context,
                          ValueNotifier<List<CartItem>> products,
                          Widget widget) =>
                      GridView.builder(
                    padding: EdgeInsets.all(5.0),
                    itemBuilder: (BuildContext context, int index) {
                      return ProductCardView(
                          products.value[index], _searchViewModel);
                    },
                    itemCount: products.value?.length ?? 0,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 0.95,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SearchArguments {
  final int shopId;

  SearchArguments(this.shopId);
}
