import 'dart:async';

import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/data/web/models/search/search_response_entity.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/main/cart/CartHolder.dart';
import 'package:delivery_client/main/cart/CartView.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/cupertino.dart';

class SearchViewModel implements CartHolder {
  final WebService _webService;
  final CartViewModel _cartViewModel;

  final int _shopId;

  ValueNotifier<List<CartItem>> products = ValueNotifier(null);

  Timer _debounce;

  SearchViewModel(this._webService, this._cartViewModel, this._shopId);

  search(String text) async {
    if (text.isEmpty) return;
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 1000), () async {
      final searchResponse = await _webService.searchProducts(_shopId, text);
      _processSearchResponse(searchResponse);
    });
  }

  void _processSearchResponse(SearchResponse searchResponse) {
    final products = searchResponse.records.map((record) {
      final price = double.parse(
              record.goodAttrs.firstWhere((att) => att.attrId == 20).attrValue)
          .toDouble();

      final unit =
          record.goodAttrs.firstWhere((att) => att.attrId == 24).attrValue;

      final unitStep = double.parse(
          record.goodAttrs.firstWhere((att) => att.attrId == 25).attrValue);

      final product = Product(
          record.goodId,
          record.goodName,
          record.goodImg,
          price,
          record.goodDiscountPrice?.toDouble()??0,
          unit,
          unitStep);
      final itemInCart =
          _cartViewModel.getCartItem(product) ?? CartItem(product, 0.0);

      return itemInCart;
    }).toList();

    this.products.value = products;
  }

  void addItemToCart(CartItem product) {
    _cartViewModel.addItemToCart(product);
  }

  void removeItemFromCart(CartItem product) {
    _cartViewModel.removeItemFromCart(product);
  }
}
