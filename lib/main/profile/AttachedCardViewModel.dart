import 'package:delivery_client/data/web/WebService.dart';
import 'package:flutter/cupertino.dart';

class AttachedCardViewModel {
  ValueNotifier<String> cardNumber = ValueNotifier(null);

  int cardId;

  final WebService _webService;

  AttachedCardViewModel(this._webService) {
    loadCard();
  }

  loadCard() async {
    final cardResponse = await _webService.getPaymentCards();
    final cardNumber = cardResponse.records.first.number;
    cardId = cardResponse.records.first.sid;
    this.cardNumber.value = cardNumber;
  }

  removeCard() async {
    try {
      cardNumber.value = null;
      cardId = null;
      final response = await _webService.removePaymentCard(cardId);
    } catch(e) {
      print(e);
    }

  }


}
