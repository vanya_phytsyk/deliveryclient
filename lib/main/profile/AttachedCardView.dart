import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/profile/AttachedCardViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:provider/provider.dart';

import 'AttachedCardViewModel.dart';

class AttachedCardView extends StatelessWidget {
  AttachedCardViewModel _attachedCardViewModel = getIt<AttachedCardViewModel>();

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.white,
      border: BorderDirectional(
          bottom: BorderSide(color: Color(0x4D000000), width: 1.0)),
    );
    var textStyle = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff1E2432));
    var hintStyle2 = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff909090));
    return Scaffold(
      backgroundColor: Color(0xffFDFDFD),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                child: Image.asset('assets/close_details.png'),
              ),
            ),
            Align(
              alignment: AlignmentDirectional.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 24.0, left: 20.0),
                child: Text('Платежная карта',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 17.0,
                        fontWeight: FontWeight.w600)),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Container(
                    margin: EdgeInsets.only(top: 96.0, left: 15.0),
                    decoration: boxDecoration,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 10.0, top: 9.0, bottom: 9.0),
                            child: ListenableProvider.value(
                              value: _attachedCardViewModel.cardNumber,
                              child: Consumer<ValueNotifier<String>>(
                                builder: (BuildContext context,
                                        ValueNotifier<String> cardNumber,
                                        Widget widget) =>
                                    Text(cardNumber.value ?? '',
                                        softWrap: false,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Color(0xff909090),
                                            fontSize: 14.0,
                                            fontFamily: 'Montserrat')),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(right: 10.0, left: 10.0),
                            child: Image.asset('assets/credit-card.png'))
                      ],
                    ),
                  ),
                ),
                Expanded(
                    flex: 5,
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        Navigator.pop(context);
                        await _attachedCardViewModel.removeCard();
                      },
                      child: Container(
                        padding:
                            EdgeInsets.only(top: 96.0, right: 10.0, left: 20.0),
                        child: Text(
                          'Открепить',
                          style: TextStyle(
                              color: Color(0xffFFA300),
                              fontSize: 14.0,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'Montserrat'),
                        ),
                      ),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
