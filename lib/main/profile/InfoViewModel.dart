import 'package:delivery_client/data/web/WebService.dart';
import 'package:flutter/cupertino.dart';

class InfoViewModel {
  ValueNotifier<String> policy = ValueNotifier(null);
  ValueNotifier<String> offer = ValueNotifier(null);

  final WebService _webService;

  InfoViewModel(this._webService) {
    loadInfo();
  }

  loadInfo() async {
    final policyResponse = await _webService.getPrivacyText();
    policy.value = policyResponse.result;
    final offerResponse = await _webService.getOfferText();
    offer.value = offerResponse.result;
  }
}
