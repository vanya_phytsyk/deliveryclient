import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/profile/InfoViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:provider/provider.dart';

class InfoView extends StatelessWidget {
  final _infoViewModel = getIt<InfoViewModel>();

  final InfoType infoType;

  InfoView({Key key, this.infoType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var boxDecoration = BoxDecoration(
      color: Colors.white,
      border: BorderDirectional(
          bottom: BorderSide(color: Color(0x4D000000), width: 1.0)),
    );
    var textStyle = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff1E2432));
    var hintStyle2 = TextStyle(
        fontFamily: 'Montserrat', fontSize: 14.0, color: Color(0xff909090));

    return ListenableProvider<ValueNotifier<String>>(
      builder: (context) =>
          infoType == InfoType.POLICY ? _infoViewModel.policy : _infoViewModel.offer,
      child: Scaffold(
        backgroundColor: Color(0xffFDFDFD),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  Navigator.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                  child: Image.asset('assets/close_details.png'),
                ),
              ),
              Align(
                alignment: AlignmentDirectional.topCenter,
                child: Padding(
                  padding: const EdgeInsets.only(top: 24.0, left: 20.0),
                  child: Text('Информация',
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17.0,
                          fontWeight: FontWeight.w600)),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 88.0, left: 31.0, right: 31.0),
                child: Consumer<ValueNotifier<String>>(
                  builder: (BuildContext context, ValueNotifier<String> text,
                          Widget widget) =>
                      SingleChildScrollView(
                        child: Text(text.value ?? '',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 14.0,
                                color: Color(0xff1E2432))),
                      ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

enum InfoType {
  POLICY, OFFER
}