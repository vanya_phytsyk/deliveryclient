import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/orderassemble/cardattach/CardAttachViewModel.dart';
import 'package:delivery_client/main/profile/AttachedCardViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: Stack(
        children: <Widget>[
          Material(
            elevation: 2.0,
            color: Colors.white,
            child: Container(
              height: 88.0,
              child: Container(
                margin: EdgeInsets.only(bottom: 12.0),
                alignment: AlignmentDirectional.bottomCenter,
                child: Text(
                  'Профиль',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.white,
            margin: EdgeInsets.only(top: 130.0),
            child: ListenableProvider<ValueNotifier<String>>(
              builder: (context) => getIt<AttachedCardViewModel>().cardNumber,
              child: Consumer<ValueNotifier<String>>(
                builder: (BuildContext context,
                        ValueNotifier<String> cardNumber, Widget widget) =>
                    Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    if (cardNumber.value != null)
                      _createMenuItem(
                          context, 'assets/profile_card.png', 'Платежная карта',
                          clickRoute: '/attachedCard'),
                    _createMenuItem(context, 'assets/profile_offer.png',
                        'Политика конфиденциальности',
                        clickRoute: '/info'),
                    _createMenuItem(
                        context, 'assets/profile_offer.png', 'Договор оферты',
                        clickRoute: '/offer'),
                    _createMenuItem(context, '', 'Выход',
                        lastItem: true,
                        iconBg: Color(0xffFD4653),
                        onClick: () => logout(context)),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _createMenuItem(BuildContext context, String iconAsset, String text,
      {String clickRoute,
      bool lastItem = false,
      Color iconBg = const Color(0xff007AFF),
      Function() onClick}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (clickRoute != null) {
          Navigator.of(context).pushNamed(clickRoute);
        } else if (onClick != null) {
          onClick();
        }
      },
      child: Container(
        decoration: BoxDecoration(
            border: BorderDirectional(
                top: BorderSide(color: Color(0xffE2E2E2), width: 1.0),
                bottom: lastItem
                    ? BorderSide(color: Color(0xffE2E2E2), width: 1.0)
                    : BorderSide.none)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 29.0,
                  height: 29.0,
                  margin: EdgeInsets.only(
                      left: 16, top: 4.5, bottom: 4.5, right: 15.0),
                  decoration: BoxDecoration(
                      color: iconBg, borderRadius: BorderRadius.circular(6.0)),
                  child: Image.asset(iconAsset),
                ),
                Text(
                  text,
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 17.0),
                ),
              ],
            ),
            Container(
                margin: EdgeInsets.only(right: 15.0),
                child: Image.asset('assets/Fw.png'))
          ],
        ),
      ),
    );
  }

  logout(BuildContext context) async {
    await getIt<LocalStorage>().clear();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }
}
