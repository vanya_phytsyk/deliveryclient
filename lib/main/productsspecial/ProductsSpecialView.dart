import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/domain/models/ProductSpecial.dart';
import 'package:delivery_client/domain/models/Shop.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/categories/CategoriesList.dart';
import 'package:delivery_client/main/categories/CategoriesViewModel.dart';
import 'package:delivery_client/main/discountproducts/DiscountProductsView.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsView.dart';
import 'package:delivery_client/main/productsspecial/ProductSpecialsViewModel.dart';
import 'package:delivery_client/main/search/SearchView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

import '../main_screen.dart';

class ProductsSpecialView extends StatefulWidget {
  @override
  _ProductsSpecialViewState createState() => _ProductsSpecialViewState();
}

class _ProductsSpecialViewState extends State<ProductsSpecialView> {
  var _selectedTabIndex = 0;

  CategoriesListView _categoriesListView;

  ProductSpecialsViewModel _productSpecialsViewModel;

  int _shopId;

  set shopId(value) {
    _shopId = value;
    _productSpecialsViewModel ??= ProductSpecialsViewModel(
        getIt<WebService>(), _shopId, getIt<CartViewModel>());
  }

  @override
  Widget build(BuildContext context) {
    final ShopArguments arguments = ModalRoute.of(context).settings.arguments;
    shopId = arguments.shop.id;

    _categoriesListView ??= CategoriesListView(
        CategoriesViewModel(arguments.shop.id, getIt<WebService>()));

    return ListenableProvider<ValueNotifier<List<ProductSpecial>>>(
      builder: (context) => _productSpecialsViewModel.productSpecials,
      child: Container(
        color: Color(0xffF7F8FA),
        child: Stack(
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.topCenter,
              margin: EdgeInsets.only(top: 88.0),
              child: GridView.count(
                  crossAxisSpacing: 12.0,
                  mainAxisSpacing: 12.0,
                  padding: EdgeInsets.all(15.0),
                  crossAxisCount: 2,
                  children: List.generate(10, (index) {
                    return Text('');
                  })),
            ),
            Material(
              elevation: 2.0,
              color: Colors.white,
              child: Container(
                height: 88.0,
                child: Stack(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(bottom: 12.0),
                        alignment: AlignmentDirectional.bottomStart,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(left: 15.0),
                                  width: 10.0,
                                  height: 18.0,
                                  child: SvgPicture.asset(
                                      'assets/svg/top-back.svg')),
                              Container(
                                margin: EdgeInsets.only(left: 8.0),
                                child: Text(
                                  'Магазины',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                    letterSpacing: 0.0,
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Montserrat',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(bottom: 12.0),
                      alignment: AlignmentDirectional.bottomCenter,
                      child: Image.network(
                        arguments.shop.icon ?? '',
                        height: 20.0,
                        width: 73.0,
                      ),
                    ),
                    Align(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.of(context).pushNamed('/search',
                              arguments: SearchArguments(_shopId));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Поиск',
                                style: TextStyle(
                                    color: Color(0xff0A1F44),
                                    fontSize: 14.0,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600),
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 4.5),
                                  child: SvgPicture.asset(
                                    'assets/svg/search-loop.svg',
                                    width: 16.0,
                                    height: 16.0,
                                  ))
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 88.0),
              child: Column(
                children: <Widget>[
                  new TabIndicator(
                    onTabChanged: (index) {
                      setState(() {
                        _selectedTabIndex = index;
                      });
                    },
                  ),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        Visibility(
                          visible: _selectedTabIndex == 0,
                          child: Container(
                            margin: EdgeInsets.only(top: 8.0),
                            child:
                                Consumer<ValueNotifier<List<ProductSpecial>>>(
                              builder: (BuildContext context,
                                      ValueNotifier<List<ProductSpecial>>
                                          productSpecial,
                                      Widget widget) =>
                                  ListView.builder(
                                key: UniqueKey(),
                                itemCount: productSpecial.value?.length ?? 0,
                                padding: EdgeInsets.only(),
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ProductItemList(
                                            _productSpecialsViewModel,
                                            productSpecial.value[index]),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: _selectedTabIndex == 1,
                          child: Container(
                            margin: EdgeInsets.only(top: 8.0),
                            child: _categoriesListView,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TabIndicator extends StatefulWidget {
  TabIndicator({Key key, this.onTabChanged}) : super(key: key);

  final Function onTabChanged;

  @override
  _TabIndicatorState createState() =>
      _TabIndicatorState()..onTabChanged = onTabChanged;
}

class _TabIndicatorState extends State<TabIndicator> {
  int _selectedTabIndex = 0;

  Function onTabChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36.0,
      margin: EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: Color(0xffE7E9ED), width: 1.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _selectedTabIndex = 0;
                  onTabChanged(_selectedTabIndex);
                });
              },
              child: Container(
                height: 36.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    color: _selectedTabIndex == 0
                        ? Color(0xffFFA300)
                        : Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(5.0),
                        topLeft: Radius.circular(5.0))),
                child: Text(
                  'Cкидки и акции',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontFamily: 'Montserrat',
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _selectedTabIndex = 1;
                  onTabChanged(_selectedTabIndex);
                });
              },
              child: Container(
                alignment: AlignmentDirectional.center,
                height: 36.0,
                decoration: BoxDecoration(
                    color: _selectedTabIndex == 1
                        ? Color(0xffFFA300)
                        : Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(5.0),
                        topRight: Radius.circular(5.0))),
                child: Text(
                  'Каталог товаров',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontFamily: 'Montserrat',
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ProductItemList extends StatelessWidget {
  ProductSpecial _productSpecial;

  final ProductSpecialsViewModel _productSpecialsViewModel;

  ProductItemList(this._productSpecialsViewModel, this._productSpecial);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Navigator.of(context).pushNamed("/discountProducts",
                  arguments: DiscountProductsArguments(
                      _productSpecialsViewModel.productSpecials.value,
                      _productSpecial.id));
            },
            child: Container(
              padding: EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    _productSpecial.name,
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Text('СМ. ВСЕ',
                      style: TextStyle(
                        fontSize: 10.0,
                        color: Color(0xffFFA300),
                      ))
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            height: 160.0,
            child: ListView.builder(
              itemCount: _productSpecial?.productsWithDiscounts?.length ?? 0,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) => ProductItem(
                  _productSpecialsViewModel,
                  _productSpecial?.productsWithDiscounts[index]),
            ),
          )
        ],
      ),
    );
  }
}

class ProductItem extends StatefulWidget {
  final Tuple2<CartItem, num> _productWithDiscount;

  final ProductSpecialsViewModel _productSpecialsViewModel;

  ProductItem(this._productSpecialsViewModel, this._productWithDiscount);

  @override
  _ProductItemState createState() =>
      _ProductItemState(_productSpecialsViewModel, _productWithDiscount);
}

class _ProductItemState extends State<ProductItem> {
  double _productsCountInCart = 0.0;
  final Tuple2<CartItem, num> _productWithDiscount;
  final ProductSpecialsViewModel _productSpecialsViewModel;

  _ProductItemState(this._productSpecialsViewModel, this._productWithDiscount) {
    _productsCountInCart = _productWithDiscount.item1.amount;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 1.0),
      color: Colors.white,
      width: 160.0,
      height: 160.0,
      child: Stack(
        children: <Widget>[
          if (_productsCountInCart > 0)
            Align(
              alignment: AlignmentDirectional.topEnd,
              child: Padding(
                padding: const EdgeInsets.only(top: 5.0, right: 6.0),
                child: Text(
                  '${_productWithDiscount.item1.product.discountPrice ?? _productWithDiscount.item1.product.price} ₽/${_productWithDiscount.item1.product.unit}',
                  style: TextStyle(
                    color: Color(0xffB8BBC6),
                    fontSize: 10.0,
                    decoration: TextDecoration.lineThrough,
                  ),
                ),
              ),
            ),
          Align(
              alignment: AlignmentDirectional.topCenter,
              child: Consumer<BottomNavigationIndexHolder>(
                builder: (context, holder, widget) => GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () async {
                    final backResult =
                        await Navigator.of(context, rootNavigator: true)
                            .pushNamed("/productDetails",
                                arguments: ProductArguments(
                                    _productWithDiscount.item1.product.id));
                    if (backResult is BackResult && backResult.showCart) {
                      holder.changeIndex(1);
                    }
                  },
                  child: Container(
                      margin: EdgeInsets.only(top: 15.0),
                      child: Image.network(
                        _productWithDiscount.item1.product?.photoUrl ?? '',
                        height: 100.0,
                      )),
                ),
              )),
          Container(
              alignment: AlignmentDirectional.bottomCenter,
              margin: EdgeInsets.only(bottom: 8.0, left: 10.0, right: 10.0),
              child: _productsCountInCart == 0
                  ? _createNormalItemFooter(
                      _productSpecialsViewModel, _productWithDiscount)
                  : _createInCartItemFooter(
                      _productSpecialsViewModel, _productWithDiscount))
        ],
      ),
    );
  }

  Widget _createNormalItemFooter(
      ProductSpecialsViewModel productSpecialsViewModel,
      Tuple2<CartItem, num> product) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              '${_productWithDiscount.item1.product.discountPrice ?? _productWithDiscount.item1.product.price} ₽/${_productWithDiscount.item1.product.unit}',
              style: TextStyle(
                color: Color(0xffB8BBC6),
                fontSize: 10.0,
                decoration: TextDecoration.lineThrough,
              ),
            ),
            Text(
              '${_productWithDiscount.item2.toStringAsFixed(1)} ₽/${_productWithDiscount.item1.product.unit}',
              style: TextStyle(color: Color(0xffFF4D00), fontSize: 15.0),
            ),
          ],
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              _productsCountInCart = product.item1.product.unitStep;
              _productSpecialsViewModel.addItemToCart(product.item1);
            });
          },
          child: Container(
            height: 32.0,
            width: 32.0,
            decoration: ShapeDecoration(
                shape: CircleBorder(), color: Color(0xffFFA300)),
            child: SizedBox(
              width: 16.0,
              height: 14.0,
              child: SvgPicture.asset('assets/svg/add-to-cart.svg',
                  fit: BoxFit.none),
            ),
          ),
        ),
      ],
    );
  }

  Widget _createInCartItemFooter(
      ProductSpecialsViewModel productSpecialsViewModel,
      Tuple2<CartItem, num> product) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            setState(() {
              _productsCountInCart += product.item1.product.unitStep;
              _productSpecialsViewModel.addItemToCart(product.item1);
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
                height: 24.0,
                width: 24.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: Color(0xffFFA300)),
                child: Image.asset(
                  'assets/increment_count.png',
                  width: 12.0,
                  height: 12.0,
                )),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                '${(product.item2.toDouble() * _productsCountInCart).toStringAsFixed(1)} ₽',
                style: TextStyle(color: Color(0xffFF4D00), fontSize: 14.0),
              ),
              Text(
                ' ${_productsCountInCart.toStringAsFixed(1)} ${product.item1.product.unit}',
                style: TextStyle(color: Color(0xffB8BBC6), fontSize: 11.0),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              _productsCountInCart -= product.item1.product.unitStep;
              if (_productsCountInCart < 0.0000001) {
                _productsCountInCart = 0;
              }
              _productSpecialsViewModel.removeItemFromCart(product.item1);
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
                height: 24.0,
                width: 24.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: Color(0xffFFA300)),
                child: Image.asset(
                  'assets/decrement_count.png',
                  width: 12.0,
                  height: 12.0,
                )),
          ),
        )
      ],
    );
  }
}

class ShopArguments {
  final Shop shop;

  ShopArguments(this.shop);
}
