import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/domain/models/ProductSpecial.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:tuple/tuple.dart';

class ProductSpecialsViewModel {
  final WebService _webService;
  final CartViewModel _cartViewModel;

  final int _shopId;

  final ValueNotifier<List<ProductSpecial>> productSpecials =
      ValueNotifier(null);

  ProductSpecialsViewModel(
      this._webService, this._shopId, this._cartViewModel) {
    _loadSpecials();
    _cartViewModel.addListener(() {
      _loadSpecials();
    });
  }

  void _loadSpecials() async {
    try {
      final response = await _webService.getProductSpecials(_shopId);

      final productSpecials = response.records
          .map((record) => ProductSpecial(
              record.sid,
              record.name,
              record.discount,
              record.description,
              record.goods.map((good) {
                var product = Product(
                    good.goodId,
                    good.goodName,
                    good.goodImg,
                    good.goodPrice.toDouble(),
                    good.goodDiscountPrice.toDouble(),
                    good.goodUnit,
                    good.goodUnitStep.toDouble());
                return Tuple2(
                    _cartViewModel.getCartItem(product) ?? CartItem(product, 0),
                    good.goodDiscountPrice.toDouble());
              }).toList()))
          .toList();

      this.productSpecials.value = productSpecials;
    } catch (e) {
      print(e);
    }
  }

  void addItemToCart(CartItem product) {
    _cartViewModel.addItemToCart(product);
  }

  void removeItemFromCart(CartItem product) {
    _cartViewModel.removeItemFromCart(product);
  }
}
