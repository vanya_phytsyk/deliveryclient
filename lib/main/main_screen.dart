import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/profile/ProfileView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'cart/CartView.dart';
import 'orders/OrdersView.dart';
import 'shops/ShopNavigator.dart';
import 'shops/ShopsView.dart';
import 'package:badges/badges.dart';

class MainScreen extends StatelessWidget {
  static final List<Widget> _screens = <Widget>[
    ShopView(),
    CartView(),
    OrdersView(),
    ProfileView(),
  ];

  @override
  Widget build(BuildContext context) {
    return ListenableProvider.value(
      value: BottomNavigationIndexHolder(),
      child: Consumer<BottomNavigationIndexHolder>(
        builder: (BuildContext context, BottomNavigationIndexHolder indexHolder,
            Widget child) {
          return Scaffold(
            body: _screens[indexHolder.index],
            bottomNavigationBar: BottomNavigationBar(
              showUnselectedLabels: true,
              type: BottomNavigationBarType.fixed,
              backgroundColor: Colors.white,
              items: <BottomNavigationBarItem>[
                createMenuItem(
                    indexHolder.index == 0, 'Каталог', 'assets/svg/shops.svg'),
                createMenuItem(
                    indexHolder.index == 1, 'Корзина', 'assets/svg/cart.svg',
                    showCartBadge: true),
                createMenuItem(indexHolder.index == 2, 'Мои заказы',
                    'assets/svg/orders.svg'),
                createMenuItem(indexHolder.index == 3, 'Профиль',
                    'assets/svg/profile.svg'),
              ],
              currentIndex: indexHolder.index,
              selectedItemColor: Color(0xffFFA300),
              onTap: (index) {
                indexHolder.changeIndex(index);
              },
            ),
          );
        },
      ),
    );
  }

  BottomNavigationBarItem createMenuItem(
      bool isActive, String title, String icon,
      {showCartBadge = false}) {
    return BottomNavigationBarItem(
      icon: ListenableProvider.value(
        value: getIt<CartViewModel>().cartAmount,
        child: Consumer<ValueNotifier<int>>(
          builder: (BuildContext context, ValueNotifier<int> cartAmount,
                  Widget child) =>
              Badge(
                  badgeColor: Color(0xffFF4D00),
                  position: BadgePosition.topRight(right: -7.0),
                  showBadge: showCartBadge && cartAmount.value > 0,
                  badgeContent: Text(
                    cartAmount.value.toString(),
                    style: TextStyle(
                        fontSize: 10.0,
                        color: Colors.white,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w500),
                  ),
                  child: SvgPicture.asset(
                    icon,
                    color: isActive ? Color(0xffFFA300) : Color(0xff8E8E93),
                  )),
        ),
      ),
      title: Text(
        title,
        overflow: TextOverflow.visible,
        softWrap: false,
        style: TextStyle(
            color: isActive ? Color(0xffFFA300) : Color(0xff8E8E93),
            fontSize: 14.0,
            fontFamily: 'Montserrat'),
      ),
    );
  }
}

class BottomNavigationIndexHolder with ChangeNotifier {
  int index = 0;

  void changeIndex(int newIndex) {
    this.index = newIndex;
    notifyListeners();
  }
}
