import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/Shop.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/productsspecial/ProductsSpecialView.dart';
import 'package:delivery_client/main/shops/ShopsViewModel.dart';
import 'package:delivery_client/map/MapView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class ShopsView extends StatefulWidget {
  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<ShopsView> {
  final viewModel = getIt<ShopsViewModel>();

  @override
  void initState() {
    var cartViewModel = getIt<CartViewModel>();
    cartViewModel.errorMessage.addListener(() {
      final error = cartViewModel.errorMessage.value;
      if (error != null) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Магазин слишком далеко'),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true)
                        .pushNamed('/map', arguments: MapArguments(true));
                  },
                  behavior: HitTestBehavior.translucent,
                  child: Text('Сменить локацию')),
            ],
          ),
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListenableProvider.value(
      value: viewModel.shops,
      child: Container(
        color: Color(0xffF7F8FA),
        child: Stack(
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.topCenter,
              margin: EdgeInsets.only(top: 88.0),
              child: Consumer<ValueNotifier<List<Shop>>>(
                builder: (BuildContext context, ValueNotifier<List<Shop>> shops,
                        Widget child) =>
                    GridView.count(
                        childAspectRatio: 0.95,
                        //remake it
                        crossAxisSpacing: 12.0,
                        mainAxisSpacing: 12.0,
                        padding: EdgeInsets.all(15.0),
                        crossAxisCount: 2,
                        children:
                            List.generate(shops.value?.length ?? 0, (index) {
                          var shop = shops.value[index];
                          return GestureDetector(
                              onTap: () {
                                final cartViewModel = getIt<CartViewModel>();
                                var currentShop =
                                    cartViewModel.currentShop.value;
                                final isTheSameShop =
                                    currentShop == null || currentShop == shop;
                                final isCartEmpty =
                                    cartViewModel.cartItems.isEmpty;
                                if (isTheSameShop || isCartEmpty) {
                                  cartViewModel.updateCurrentShop(shop);
                                  Navigator.of(context).pushNamed(
                                      '/shopProducts',
                                      arguments: ShopArguments(shop));
                                } else {
                                  showConfirmationDialog(context, shop);
                                }
                              },
                              child: ShopCard(shop));
                        })),
              ),
            ),
            Material(
              elevation: 2.0,
              color: Colors.white,
              child: Container(
                height: 88.0,
                child: Container(
                    margin: EdgeInsets.only(bottom: 12.0),
                    alignment: AlignmentDirectional.bottomCenter,
                    child: Text(
                      'Магазины',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Montserrat',
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ShopCard extends StatelessWidget {
  final Shop _shop;

  ShopCard(this._shop);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 4.0),
                height: 50.0,
                child: Image.network(_shop.icon ?? ""),
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0),
                child: Text(
                  _shop.name,
                  softWrap: false,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Montserrat',
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Padding(
                  padding: const EdgeInsets.only(left: 7.0, right: 7.0),
                  child: Text(
                    'от ${_shop.outPrice}₽ до ${_shop.inPrice}₽',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12.0,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

void showConfirmationDialog(BuildContext context, Shop shop) {
  TextStyle style = TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.w600, fontFamily: 'Montserrat');
  AlertDialog dialog = AlertDialog(
    title: Text(
      'Уверены? Корзина будет утрачена',
      style: style.copyWith(fontSize: 18.0),
    ),
    actions: <Widget>[
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          getIt<CartViewModel>().updateCurrentShop(shop);
          Navigator.of(context, rootNavigator: true).pop();
          Navigator.of(context)
              .pushNamed('/shopProducts', arguments: ShopArguments(shop));
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Да',
            style: style,
          ),
        ),
      ),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Нет',
            style: style,
          ),
        ),
      )
    ],
  );
  showDialog(context: context, builder: (context) => dialog);
}
