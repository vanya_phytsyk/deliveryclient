import 'package:delivery_client/main/discountproducts/DiscountProductsView.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsView.dart';
import 'package:delivery_client/main/products/ProductsView.dart';
import 'package:delivery_client/main/productsspecial/ProductsSpecialView.dart';
import 'package:delivery_client/main/search/SearchView.dart';
import 'package:flutter/material.dart';

import 'ShopsView.dart';

class ShopNavigatorRoutes {
  static const String shops = '/';
  static const String shopProducts = '/shopProducts';
  static const String products = '/products';
  static const String search = '/search';
  static const String discountProducts = '/discountProducts';
}

class ShopView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var routeBuilders = _routeBuilders(context);

    return Navigator(
        initialRoute: ShopNavigatorRoutes.shops,
        onGenerateRoute: (RouteSettings settings) => MaterialPageRoute(
            settings: settings,
            builder: (BuildContext context) =>
                routeBuilders[settings.name](context)));
  }
}

Map<String, WidgetBuilder> _routeBuilders(BuildContext context) {
  return {
    ShopNavigatorRoutes.shops: (context) => ShopsView(),
    ShopNavigatorRoutes.shopProducts: (context) => ProductsSpecialView(),
    ShopNavigatorRoutes.products: (context) => ProductsView(),
    ShopNavigatorRoutes.search: (context) => SearchView(),
    ShopNavigatorRoutes.discountProducts: (context) => DiscountProductsView(),
  };
}
