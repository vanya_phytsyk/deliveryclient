import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/Shop.dart';
import 'package:flutter/foundation.dart';

class ShopsViewModel {
  final WebService _webService;

  final shops = ValueNotifier<List<Shop>>(null);

  ShopsViewModel(this._webService) {
    loadShops();
  }

  void loadShops() async {
    final shopsResponse = await _webService.getShops();

    final shops = shopsResponse.records.map((record) =>
        Shop(record.sid, record.name, record.info.logoUrl, record.tariff.inArea, record.tariff.outArea)).toList();

    this.shops.value = shops;
  }
}
