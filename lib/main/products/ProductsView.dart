import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Category.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/main/cart/CartHolder.dart';
import 'package:delivery_client/main/cart/CartView.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:delivery_client/main/main_screen.dart';
import 'package:delivery_client/main/productdetails/ProductDetailsView.dart';
import 'package:delivery_client/main/products/ProductsViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class ProductsView extends StatefulWidget {
  @override
  _ProductsViewState createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  int _currentIndex = 0;

  ProductsViewModel _productsViewModel;

  @override
  Widget build(BuildContext context) {
    ShopCategoriesArguments arguments =
        ModalRoute.of(context).settings.arguments;
    if (_productsViewModel == null) {
      _productsViewModel = ProductsViewModel(
          arguments, getIt<WebService>(), getIt<CartViewModel>());

      _currentIndex = arguments.category.subCategories
          .indexWhere((sub) => sub.id == arguments.selectedCategoryId);
    }

    return DefaultTabController(
      initialIndex: _currentIndex,
      child: Builder(
        builder: (BuildContext context) {
          var controller = DefaultTabController.of(context);
          controller.addListener(() {
            if (controller.index != _currentIndex &&
                !controller.indexIsChanging) {
              setState(() {
                _currentIndex = controller.index;
              });
            }
          });
          return Stack(
            children: <Widget>[
              TabBarView(
                children: <Widget>[
                  ...arguments.category.subCategories
                      .map((cat) => buildProductsGridView(cat.id))
                ],
              ),
              Material(
                elevation: 2.0,
                color: Colors.white,
                child: Container(
                  height: 115.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Padding(
                          child: Row(
                            children: <Widget>[
                              Image.asset('assets/app_bar_back.png'),
                              Container(
                                margin: EdgeInsets.only(left: 28.0),
                                child: Text(
                                  arguments.category.name,
                                  style: TextStyle(
                                      color: Color(0xff1E2432),
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'Montserrat'),
                                ),
                              )
                            ],
                          ),
                          padding: EdgeInsets.only(
                              left: 15.0, right: 15.0, bottom: 8.0),
                        ),
                      ),
                      TabBar(
                        indicatorColor: Colors.transparent,
                        isScrollable: true,
                        tabs: <Widget>[
                          ...arguments.category.subCategories.map((cat) =>
                              buildTab(
                                  cat.name,
                                  arguments.category.subCategories
                                          .indexOf(cat) ==
                                      _currentIndex))
                        ],
                        onTap: (index) {
                          setState(() {
                            this._currentIndex = index;
                          });
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
      length: arguments.category.subCategories.length,
    );
  }

  Widget buildProductsGridView(int categoryId) {
    return ListenableProvider<ValueNotifier<List<CartItem>>>(
      builder: (BuildContext context) =>
          _productsViewModel.products[categoryId],
      child: Consumer<ValueNotifier<List<CartItem>>>(
        builder: (BuildContext context, ValueNotifier<List<CartItem>> products,
                Widget child) =>
            Stack(
          children: <Widget>[
            Padding(
              key: UniqueKey(),
              padding: EdgeInsets.only(top: 115.0),
              child: GridView.builder(
                itemCount: products.value?.length ?? 0,
                itemBuilder: (BuildContext context, int index) =>
                    ProductCardView(products.value[index], _productsViewModel),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 0.95),
              ),
            ),
            Center(
              child: Visibility(
                visible: products.value?.isEmpty ?? false,
                child: Text('Нет итемов',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 12.0,
                        color: Color(0xff1E2432))),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildTab(String text, bool isSelected) => Container(
      constraints: BoxConstraints(minHeight: 30.0, maxWidth: 110),
      margin: EdgeInsets.only(bottom: 4.0),
      alignment: AlignmentDirectional.center,
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.visible,
        textAlign: TextAlign.center,
        maxLines: 2,
        style: TextStyle(
            fontSize: 12.0,
            fontFamily: 'Montserrat',
            color: isSelected ? Color(0xffFFA300) : Color(0xff1E2432)),
      ));
}

class ProductCardView extends StatefulWidget {
  final CartItem _product;
  final CartHolder _cartHolder;

  ProductCardView(this._product, this._cartHolder);

  @override
  _ProductCardViewState createState() =>
      _ProductCardViewState(_product, _cartHolder);
}

class _ProductCardViewState extends State<ProductCardView> {
  double _itemsInCart = 0;

  CartItem _product;

  CartHolder _cartHolder;

  _ProductCardViewState(this._product, this._cartHolder) {
    _itemsInCart = _product.amount;
  }

  @override
  Widget build(BuildContext context) {
    return _itemsInCart == 0
        ? _buildCardView()
        : _buildCountSelectionCardView();
  }

  Widget _buildCardView() {
    return Consumer<BottomNavigationIndexHolder>(
      builder: (context, holder, widget) => GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          final backResult = await Navigator.of(context, rootNavigator: true)
              .pushNamed("/productDetails",
                  arguments: ProductArguments(_product.product.id));
          if (backResult is BackResult && backResult.showCart) {
            holder.changeIndex(1);
          }
        },
        child: Card(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 82.0,
              child: Image.network(_product.product.photoUrl),
              alignment: AlignmentDirectional.center,
            ),
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0),
              child: Text(
                _product.product.name,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 12.0,
                    color: Color(0xff1E2432)),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 14.0, bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: Text(
                      '${_product.product.price.toStringAsFixed(2)} ₽/${_product.product.unit}',
                      style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Montserrat',
                          color: Color(0xff1E2432)),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _itemsInCart += _product.product.unitStep;
                        _cartHolder.addItemToCart(_product);
                      });
                    },
                    child: Container(
                      height: 34.0,
                      width: 34.0,
                      decoration: ShapeDecoration(
                          shape: CircleBorder(), color: Color(0xffFFA300)),
                      child: SizedBox(
                        width: 16.0,
                        height: 14.0,
                        child: SvgPicture.asset('assets/svg/add-to-cart.svg',
                            fit: BoxFit.none),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        )),
      ),
    );
  }

  Widget _buildCountSelectionCardView() {
    return Card(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 10.0),
          child: Row(
            children: <Widget>[
              Consumer<BottomNavigationIndexHolder>(
                builder: (context, holder, widget) => GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () async {
                    final backResult = await Navigator.of(context,
                            rootNavigator: true)
                        .pushNamed("/productDetails",
                            arguments: ProductArguments(_product.product.id));
                    if (backResult is BackResult && backResult.showCart) {
                      holder.changeIndex(1);
                    }
                  },
                  child: Container(
                    height: 45.0,
                    width: 81.0,
                    margin: EdgeInsets.only(left: 5.0),
                    child: Image.network(_product.product.photoUrl),
                  ),
                ),
              ),
              Text(
                  '${_product.product.discountPrice ?? _product.product.price} ₽/${_product.product.unit}',
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Montserrat',
                      color: Color(0xff1E2432)))
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(_product.product.name,
                style: TextStyle(
                    fontSize: 12.0,
                    fontFamily: 'Montserrat',
                    color: Color(0xff1E2432)))),
        Text(
            '${(_product.product.discountPrice ?? _product.product.price * _itemsInCart).toStringAsFixed(2)} ₽',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 13.0,
                fontWeight: FontWeight.w500,
                fontFamily: 'Montserrat',
                color: Color(0xff1E2432))),
        Container(
          margin: EdgeInsets.only(top: 5.0),
          child: Text(
            'за ${_itemsInCart.toStringAsFixed(2)} ${_product.product.unit}',
            style: TextStyle(
              color: Color(0xffB8BBC6),
              fontSize: 12.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 4.0, left: 15.0, right: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  setState(() {
                    _itemsInCart += _product.product.unitStep;
                    _cartHolder.addItemToCart(_product);
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Container(
                      height: 24.0,
                      width: 24.0,
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Color(0xffFFA300)),
                      child: Image.asset(
                        'assets/increment_count.png',
                        width: 12.0,
                        height: 12.0,
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      '${_itemsInCart.toStringAsFixed(2)} ${_product.product.unit}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xffFFA300),
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  setState(() {
                    _itemsInCart -= _product.product.unitStep;
                    _cartHolder.removeItemFromCart(_product);
                    if (_itemsInCart <= 0.00001) {
                      _itemsInCart = 0.0;
                    }
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Container(
                      height: 24.0,
                      width: 24.0,
                      alignment: AlignmentDirectional.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Color(0xffFFA300)),
                      child: Image.asset(
                        'assets/decrement_count.png',
                        width: 12.0,
                        height: 12.0,
                      )),
                ),
              )
            ],
          ),
        )
      ],
    ));
  }
}

class ShopCategoriesArguments {
  final int shopId;
  final ProductCategory category;
  final int selectedCategoryId;

  ShopCategoriesArguments(this.category, this.selectedCategoryId, this.shopId);
}
