import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/main/cart/CartHolder.dart';
import 'package:delivery_client/main/cart/CartViewModel.dart';
import 'package:flutter/foundation.dart';

import 'ProductsView.dart';

class ProductsViewModel with ChangeNotifier implements CartHolder {
  final ShopCategoriesArguments _arguments;
  final WebService _webService;

  final Map<int, ValueNotifier<List<CartItem>>> products = {};

  final CartViewModel _cartViewModel;

  ProductsViewModel(this._arguments, this._webService, this._cartViewModel) {
    _arguments.category.subCategories
        .forEach((sub) => products[sub.id] = ValueNotifier(null));
    _loadProducts();
    _cartViewModel.addListener(() {
      _loadProducts();
    });
  }

  _loadProducts() {
    _arguments.category.subCategories
        .forEach((sub) => _loadProductsForCategory(sub.id));
  }

  _loadProductsForCategory(int categoryId) async {
    final productsResponse =
        await _webService.getProducts(_arguments.shopId, categoryId);

    final categoryProducts = productsResponse.records
        .map((record) => Product(
            record.goodId,
            record.goodName,
            record.goodImg,
            record.goodPrice.toDouble(),
            record.goodDiscountPrice.toDouble(),
            record.goodUnit,
            record.goodUnitStep.toDouble()))
        .map((product) =>
            _cartViewModel.getCartItem(product) ?? CartItem(product, 0))
        .toList();
    if (products[categoryId].value == categoryProducts) {
      products[categoryId].notifyListeners();
    } else {
      products[categoryId].value = categoryProducts;
    }
  }

  void addItemToCart(CartItem product) {
    _cartViewModel.addItemToCart(product);
  }

  void removeItemFromCart(CartItem product) {
    _cartViewModel.removeItemFromCart(product);
  }
}
