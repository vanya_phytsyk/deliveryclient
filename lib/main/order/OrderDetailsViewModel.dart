import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/domain/models/CartItem.dart';
import 'package:delivery_client/domain/models/Order.dart';
import 'package:delivery_client/domain/models/Product.dart';
import 'package:delivery_client/main/cart/CartPriceViewModel.dart';
import 'package:delivery_client/main/orders/OrderItemViewModel.dart';
import 'package:delivery_client/main/orders/OrdersViewModel.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class OrderDetailsViewModel extends ChangeNotifier {
  List<CartItem> cartItems = List();

  CartPriceViewModel orderPriceViewModel = CartPriceViewModel();

  final WebService _webservice;

  bool isProgress = false;

  int _orderId;

  OrderStatus orderStatus;
  OrdersViewModel _ordersViewModel;

  OrderDetailsViewModel(this._orderId, this._webservice, this._ordersViewModel) {
    getOrderDetails(_orderId);
  }

  getOrderDetails(int orderId) async {
    isProgress = true;
    notifyListeners();
    final orderDetailsResponse = await _webservice.getOrderDetails(orderId);
    final cartItems = orderDetailsResponse.result.cart.records
        .map((record) => CartItem(
            Product(
                record.goodId,
                record.goodName,
                record.goodImg,
                record.goodDiscountPrice?.toDouble() ??
                    record.goodPrice.toDouble(),
                record.goodDiscountPrice.toDouble(),
                record.goodUnit,
                record.goodUnitStep.toDouble()),
            record.goodCnt.toDouble()))
        .toList();
    this.cartItems.addAll(cartItems);
    orderPriceViewModel.updatePrice(cartItems,
        deliveryCost: orderDetailsResponse.result.cart.shop.deliveryPrice);
    orderStatus = OrdersViewModel.getOrderStatus(orderDetailsResponse.result.status);
    isProgress = false;
    notifyListeners();
  }

  onCancelOrderClick() async {
    isProgress = true;
    notifyListeners();
    try {
      await _webservice.cancelOrder(_orderId);
      this.orderStatus = OrderStatus.CANCELLED;
      _ordersViewModel.loadOrders();
    } catch (e) {
      print(e);
    } finally {
      isProgress = false;
      notifyListeners();
    }
  }
}
