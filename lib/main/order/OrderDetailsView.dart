import 'package:delivery_client/data/web/WebService.dart';
import 'package:delivery_client/di/di.dart';
import 'package:delivery_client/domain/models/Order.dart';
import 'package:delivery_client/main/order/OrderDetailsViewModel.dart';
import 'package:delivery_client/main/orders/OrderItemViewModel.dart';
import 'package:delivery_client/main/orders/OrdersViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'DashedSeparator.dart';

import 'package:qr_flutter/qr_flutter.dart';

class OrderDetailsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final OrderDetailsArguments arguments =
        ModalRoute.of(context).settings.arguments;

    return ListenableProvider<OrderDetailsViewModel>(
      child: Consumer<OrderDetailsViewModel>(
        builder: (BuildContext context, OrderDetailsViewModel viewModel,
                Widget child) =>
            Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                IndexedStack(
                  index: viewModel.isProgress ? 1 : 0,
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 88.0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(top: 20.0, left: 20.0),
                                    child:
                                        Image.asset('assets/close_details.png'),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 24.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        'Заказ №${arguments.orderId}',
                                        style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 20.0,
                                            color: Color(0xff1E2432)),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 17.0),
                                          child: RichText(
                                            text: TextSpan(
                                                text: 'Статус: ',
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text:
                                                          '${OrderItemViewModel.getDeliveryStatusText(viewModel.orderStatus)}',
                                                      style: TextStyle(
                                                          color: OrderItemViewModel
                                                              .getStatusColor(
                                                                  viewModel
                                                                      .orderStatus),
                                                          fontWeight:
                                                              FontWeight.w600))
                                                ],
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 20.0,
                                                    color: Color(0xff919191))),
                                          )),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (_) => _createQRPayDialog(
                                            context,
                                            arguments.orderId.toString()));
                                  },
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(top: 27.0, right: 20.0),
                                    child: Image.asset('assets/qr-code.png'),
                                  ),
                                )
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 19.0, left: 20.0, right: 20.0),
                              child: Column(
                                  children: viewModel.cartItems.map((item) {
                                return Column(
                                  children: <Widget>[
                                    DashedSeparator(),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                            width: 40.0,
                                            height: 40.0,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Color(0xffEFEFEF),
                                                    width: 1.0)),
                                            margin: EdgeInsets.only(
                                                right: 10.0,
                                                top: 9.0,
                                                bottom: 9.0),
                                            child: Image.network(
                                                item.product.photoUrl)),
                                        Container(
                                          width: 180.0,
                                          child: Text(item.product.name,
                                              style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 16.0,
                                                  color: Color(0xff1E2432))),
                                        ),
                                        Expanded(
                                          child: Container(
                                            alignment:
                                                AlignmentDirectional.topEnd,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Text(
                                                    '~${item.amount.toStringAsFixed(2)} ${item.product.unit}',
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 13.0,
                                                        color:
                                                            Color(0xffB8BBC6))),
                                                Text(
                                                    '${item.product.price.toStringAsFixed(2)} ₽',
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 15.0,
                                                        color:
                                                            Color(0xff1E2432))),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    if (viewModel.cartItems.last == item)
                                      DashedSeparator(),
                                  ],
                                );
                              }).toList()),
                            ),
                            Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 18.0, left: 20.0, right: 20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Стоимость товаров',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432))),
                                      Text(
                                          '${viewModel.orderPriceViewModel.productsPrice.toStringAsFixed(2)} ₽',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432))),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 22.0, left: 20.0, right: 20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Стоимость доставки',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432))),
                                      Text(
                                          '${viewModel.orderPriceViewModel.deliveryPrice.toStringAsFixed(2)} ₽',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432))),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 22.0, left: 20.0, right: 20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Итого',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xff1E2432))),
                                      Text(
                                          '${viewModel.orderPriceViewModel.totalPrice.toStringAsFixed(2)} ₽',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w700,
                                              fontSize: 15.0,
                                              color: Color(0xff1E2432))),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: AlignmentDirectional.center,
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFFFFA300)),
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: AlignmentDirectional.bottomCenter,
                  child: Visibility(
                    visible: viewModel.orderStatus != OrderStatus.CANCELLED,
                    child: Material(
                      color: Colors.white,
                      elevation: 4.0,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 13.0, bottom: 13.0, left: 10.0, right: 10.0),
                        child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0xffFD4653),
                              borderRadius: BorderRadius.circular(5.0)),
                          child: GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () {
                              showConfirmationDialog(context);
//                            showDialog(
//                                context: context,
//                                builder: (_) => _createChangeOrderDialog(
//                                    context,
//                                    viewModel.orderViewModel.order.number));
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 12.0, bottom: 11.0),
                              child: Text('Отменить заказ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 17.0,
                                    color: Colors.white,
                                  )),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      builder: (BuildContext context) {
        return OrderDetailsViewModel(
            arguments.orderId, getIt<WebService>(), getIt<OrdersViewModel>());
      },
    );
  }
}

Dialog _createQRPayDialog(BuildContext context, String orderNumber) {
  return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            alignment: AlignmentDirectional.center,
            margin: EdgeInsets.only(top: 30.0, left: 13.0, right: 13.0),
            child: Text(
              'Оплата заказа №$orderNumber',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff1E2432)),
            ),
          ),
          Container(
              height: 200.0,
              margin: EdgeInsets.only(top: 35.0, left: 43.0, right: 43.0),
              child: QrImage(
                padding: EdgeInsets.zero,
                version: 1,
                foregroundColor: Color(0xff444444),
                data: '342342',
              )),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xffFFA300),
                borderRadius: BorderRadius.circular(5.0)),
            margin:
                EdgeInsets.only(top: 35.0, left: 43, right: 43.0, bottom: 20.0),
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 11.0),
                child: Text('Готово',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                      color: Color(0xff0A1F44),
                    )),
              ),
            ),
          ),
        ],
      ));
}

Dialog _createChangeOrderDialog(BuildContext context, String orderNumber) {
  return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.center,
              margin: EdgeInsets.only(top: 30.0, left: 13.0, right: 13.0),
              child: Text(
                'Изменения заказа №$orderNumber',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff1E2432)),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 24.0, right: 15.0, left: 15.0),
              child: Column(
                children: <Widget>[
                  DashedSeparator(),
                  Row(
                    children: <Widget>[
                      Container(
                          width: 30.0,
                          height: 30.0,
                          margin: EdgeInsets.only(
                              right: 10.0, top: 5.0, bottom: 5.0),
                          child: Image.asset('assets/kolbasa.png')),
                      Container(
                        width: 116.0,
                        child: Text('Супер колбаса от колбаскина',
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 12.0,
                                color: Color(0xff1E2432))),
                      ),
                      Expanded(
                        child: Container(
                          alignment: AlignmentDirectional.topEnd,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text('-0.3 шт',
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 11.0,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xffFD4653))),
                              Container(
                                margin: EdgeInsets.only(top: 2.0),
                                child: Text('-150,00 ₽',
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 11.0,
                                        color: Color(0xffFD4653))),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  DashedSeparator(),
                  Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Старое итого',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    color: Color(0xff1E2432))),
                            Text('8050,00 ₽',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    color: Color(0xff1E2432))),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Стоимость изменений',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    color: Color(0xff1E2432))),
                            Text('-450,00 ₽',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    color: Color(0xffFD4653))),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 27.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Новое итого',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff1E2432))),
                            Text('7650,00 ₽',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff1E2432))),
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0xff54B24C),
                            borderRadius: BorderRadius.circular(5.0)),
                        child: GestureDetector(
                          onTap: () {},
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 12.0, bottom: 11.0),
                            child: Text('Принять изменения',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 17.0,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15.0, bottom: 30.0),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0xffFD4653),
                            borderRadius: BorderRadius.circular(5.0)),
                        child: GestureDetector(
                          onTap: () {},
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 12.0, bottom: 11.0),
                            child: Text('Отменить заказ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 17.0,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ));
}

void showConfirmationDialog(BuildContext context) {
  TextStyle style = TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.w600, fontFamily: 'Montserrat');
  AlertDialog dialog = AlertDialog(
    title: Text(
      'Уверены?',
      style: style.copyWith(fontSize: 18.0),
    ),
    actions: <Widget>[
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Navigator.pop(context);
          Provider.of<OrderDetailsViewModel>(context, listen: false)
              .onCancelOrderClick();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Да',
            style: style,
          ),
        ),
      ),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => Navigator.pop(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Нет',
            style: style,
          ),
        ),
      )
    ],
  );
  showDialog(context: context, builder: (context) => dialog);
}

class OrderDetailsArguments {
  final int orderId;

  OrderDetailsArguments(this.orderId);
}
