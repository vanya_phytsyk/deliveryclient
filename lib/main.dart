import 'dart:async';
import 'dart:io';

import 'package:delivery_client/main/search/SearchView.dart';
import 'package:delivery_client/map/MapView.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sentry/sentry.dart';
import 'codeVerify/VerifyPhoneView.dart';
import 'di/di.dart';
import 'main/cart/CartView.dart';
import 'main/main_screen.dart';
import 'main/order/OrderDetailsView.dart';
import 'main/orderassemble/OrderAssembleView.dart';
import 'main/orderassemble/OrderFailedView.dart';
import 'main/orderassemble/OrderSuccessView.dart';
import 'main/orderassemble/cardattach/CardAttachView.dart';
import 'main/productdetails/ProductDetailsView.dart';
import 'main/profile/AttachedCardView.dart';
import 'main/profile/InfoView.dart';
import 'main/shops/ShopsView.dart';
import 'splash/SplashView.dart';
import 'package:delivery_client/login/LoginView.dart';

void main() {
  registerDependencies();
  runZoned<Future<void>>(() async {
    runApp(MyApp());
  }, onError: (error, stackTrace) {
    // Whenever an error occurs, call the `_reportError` function. This sends
    // Dart errors to the dev console or Sentry depending on the environment.
    _reportError(error, stackTrace);
  });
}

final mapView = MapView();
final loginView = LoginView();
final verifyPhoneView = VerifyPhoneView();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: 'Delivery client',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => loginView,
        '/verify': (BuildContext context) => verifyPhoneView,
        '/map': (BuildContext context) => mapView,
        '/main': (BuildContext context) => MainScreen(),
        '/productDetails': (BuildContext context) => ProductDetailsView(),
        '/orderDetails': (BuildContext context) => OrderDetailsView(),
        '/orderAssemble': (BuildContext context) => OrderAssembleView(),
        '/cardAttach': (BuildContext context) => CardAttachView(),
        '/orderSuccess': (BuildContext context) => OrderSuccessView(),
        '/orderFailed': (BuildContext context) => OrderFailedView(),
        '/attachedCard': (BuildContext context) => AttachedCardView(),
        '/info': (BuildContext context) => InfoView(
              infoType: InfoType.POLICY,
            ),
        '/offer': (BuildContext context) => InfoView(
              infoType: InfoType.OFFER,
            ),
        '/search': (BuildContext context) => SearchView(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SplashView();
  }
}

bool get isInDebugMode {
  // Assume you're in production mode.
  bool inDebugMode = false;

  // Assert expressions are only evaluated during development. They are ignored
  // in production. Therefore, this code only sets `inDebugMode` to true
  // in a development environment.
  assert(inDebugMode = true);

  return false;
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  // Print the exception to the console.
  print('Caught error: $error');
  if (isInDebugMode) {
    // Print the full stacktrace in debug mode.
    print(stackTrace);
    return;
  } else {
    print(stackTrace);
    String platformInfo;
    if (Platform.isAndroid) {
      platformInfo = (await DeviceInfoPlugin().androidInfo).device;
    } else {
      platformInfo = (await DeviceInfoPlugin().iosInfo).model;
    }

    final Event event = new Event(
        exception: error,
        stackTrace: stackTrace,
        extra: {'device': platformInfo});

    getIt<SentryClient>().capture(event: event);
  }
}
