import 'dart:async';
import 'dart:convert';

import 'package:delivery_client/data/local/LocalStorage.dart';
import 'package:delivery_client/data/web/models/AddressRequest.dart';
import 'package:delivery_client/data/web/models/EmptyResponse.dart';
import 'package:delivery_client/data/web/models/LoginResponse.dart';
import 'package:delivery_client/data/web/models/AuthResponse.dart';
import 'package:delivery_client/data/web/models/ResendSMSResponse.dart';
import 'package:delivery_client/data/web/models/card/PaymentCardsResponse.dart';
import 'package:delivery_client/data/web/models/cart/AddItemToCartResponse.dart';
import 'package:delivery_client/data/web/models/cart/CartResponse.dart';
import 'package:delivery_client/data/web/models/cart/DeleteFromCartResponse.dart';
import 'package:delivery_client/data/web/models/cart/DeliveryCostResponse.dart';
import 'package:delivery_client/data/web/models/categories/CategoriesResponse.dart';
import 'package:delivery_client/data/web/models/discounts/DiscountProductsResponse.dart';
import 'package:delivery_client/data/web/models/discounts/DiscountsResponse.dart';
import 'package:delivery_client/data/web/models/orders/OrdersResponse.dart';
import 'package:delivery_client/data/web/models/payment/AddPaymentCardResponse.dart';
import 'package:delivery_client/data/web/models/payment/OrderPaymentResponse.dart';
import 'package:delivery_client/data/web/models/product/ProductDetailsResponse.dart';
import 'package:delivery_client/data/web/models/search/search_response_entity.dart';
import 'package:delivery_client/data/web/models/shops/ShopsResponse.dart';
import 'package:http/http.dart' as http;

import 'WebException.dart';
import 'models/ErrorResponse.dart';
import 'models/orders/OrderConfirmResponse.dart';
import 'models/orders/OrderDetailsResponse.dart';
import 'models/privacypolicy/PolicyResponse.dart';
import 'models/products/ProductsResponse.dart';
import 'models/products/ProductsResponse.dart';

class WebService {
  final LocalStorage localStorage;

  WebService(this.localStorage);

  Future<LoginResponse> register(String phoneNumber) async {
    http.Response response = await http.post(
        'https://mops.swaxis.com/api/v1/register-client',
        body: {'phone': phoneNumber});

    var body = response.body;
    if (response.statusCode == 200) {
      var loginResponse = LoginResponse.fromJsonMap(json.decode(body));
      if (loginResponse.success) {
        return loginResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to login');
    }
  }

  Future<LoginResponse> login(String phoneNumber) async {
    http.Response response = await http.post(
        'https://auth0.swaxis.com/api/v1/login',
        body: {'phone': phoneNumber, 'method': 'sms'});

    var body = response.body;
    if (response.statusCode == 200) {
      var loginResponse = LoginResponse.fromJsonMap(json.decode(body));
      if (loginResponse.success) {
        return loginResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to login');
    }
  }

  Future<ResendSMSResponse> resendSms(bool isLogin, String checkToken) async {
    http.Response response = await http.post(
        'https://auth0.swaxis.com/api/v1/resend-sms-${isLogin ? 'login' : 'register'}-code',
        body: {'checkToken': checkToken});

    var body = response.body;
    if (response.statusCode == 200) {
      var loginResponse = ResendSMSResponse.fromJson(json.decode(body));
      if (loginResponse.success) {
        return loginResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to resend sms');
    }
  }

  ErrorResponse parseError(http.Response response) {
    return ErrorResponse.fromJsonMap(json.decode(response.body));
  }

  Future<AuthResponse> checkRegisterSmsCode(
      LoginResponse loginResponse, String code) async {
    return _checkSmsCode(true, loginResponse, code);
  }

  Future<AuthResponse> checkLoginSmsCode(
      LoginResponse loginResponse, String code) async {
    return _checkSmsCode(false, loginResponse, code);
  }

  Future<AuthResponse> _checkSmsCode(
      bool isRegister, LoginResponse loginResponse, String code) async {
    final registerPath =
        isRegister ? 'check-sms-register-code' : 'check-sms-login-code';
    http.Response response = await http.post(
        'https://auth0.swaxis.com/api/v1/$registerPath',
        body: {'checkToken': loginResponse.smsCheckToken, 'code': code});

    final accessToken1 = json.decode(response.body)['access'];
    final refreshToken1 = json.decode(response.body)['refresh'];

    http.Response getAppInfoResponse = await http.get(
        'https://auth0.swaxis.com/api/v1/get-app-info?appName=mops',
        headers: {'Authorization': 'Bearer $accessToken1'});

    Map<String, dynamic> appInfoBody = jsonDecode(getAppInfoResponse.body);

    final profileId = appInfoBody['profiles'][0]['id'];

    http.Response tokensResponse = await http.get(
        'https://mops.swaxis.com/api/v1/get-tokens?access=$accessToken1&refresh=$refreshToken1&profile=$profileId');

    final tokenBody = jsonDecode(tokensResponse.body);

    if (response.statusCode == 200) {
      final parsedResponse = AuthResponse.fromJsonMap(tokenBody);
      if (parsedResponse.success) {
        localStorage.saveAccessToken(parsedResponse.access);
        localStorage.saveRefreshToken(parsedResponse.refresh);
        localStorage.saveTokenExpirationDate(parsedResponse.expire);
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to confirm with sms');
    }
  }

  Future<EmptyResponse> updateAddress(AddressRequest addressRequest) async {
    await handleExpiredTokens();
    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.put(
        'https://mops.swaxis.com/api/v1/profile/address',
        body: addressRequest.toJson(),
        headers: {'Authorization': 'Bearer $accessToken'});

    if (response.statusCode == 200) {
      var loginResponse = EmptyResponse.fromJsonMap(json.decode(response.body));
      if (loginResponse.success) {
        return loginResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to update address');
    }
  }

  Future<ShopsResponse> getShops() async {
    await handleExpiredTokens();
    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        'https://mops.swaxis.com/api/v1/shops?city=Moscow&limit=1000&offset=0',
        headers: {'Authorization': 'Bearer $accessToken'});

    final body = jsonDecode(response.body);
    if (response.statusCode == 200) {
      final shopsResponse = ShopsResponse.fromJsonMap(body);
      if (shopsResponse.success) {
        return shopsResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to update address');
    }
  }

  Future<void> handleExpiredTokens() async {
    final expirationTime = await localStorage.getTokenExpirationDate();
    var minutesBeforeExpiration =
        DateTime.fromMillisecondsSinceEpoch(expirationTime)
            .difference(DateTime.now())
            .inMinutes;
    final isExpired = minutesBeforeExpiration < 2;
    if (isExpired) {
      await refreshTokens();
    }
  }

  Completer refreshTokensCompleter;

  Future<void> refreshTokens() async {
    if (refreshTokensCompleter != null) {
      await refreshTokensCompleter.future;
      refreshTokensCompleter = null;
      return null;
    }

    try {
      refreshTokensCompleter = Completer();

      final refreshToken = await localStorage.getRefreshToken();

      http.Response response = await http.post(
          'https://mops.swaxis.com/api/v1/refresh',
          body: {'token': refreshToken});

      final body = jsonDecode(response.body);

      if (response.statusCode == 200) {
        final parsedResponse = AuthResponse.fromJsonMap(body);
        if (parsedResponse.success) {
          await localStorage.saveAccessToken(parsedResponse.access);
          await localStorage.saveRefreshToken(parsedResponse.refresh);
          await localStorage.saveTokenExpirationDate(parsedResponse.expire);
          return parsedResponse;
        } else {
          throw WebException(parseError(response));
        }
      } else {
        throw Exception('Failed to refresh tokens');
      }
    } finally {
      if (refreshTokensCompleter != null) refreshTokensCompleter.complete();
    }
  }

  Future<CategoriesResponse> getCategories(int shopId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/shops/$shopId/categories",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = CategoriesResponse.fromJsonMap(body);
      return parsedResponse;
    } else {
      throw Exception('Failed to get categories');
    }
  }

  Future<ProductsResponse> getProducts(int shopId, int categoryId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/shops/$shopId/categories/$categoryId/goods",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = ProductsResponse.fromJson(body);
      return parsedResponse;
    } else {
      throw Exception('Failed to get products for category');
    }
  }

  Future<ProductDetailsResponse> getProductDetails(int productId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/goods/$productId",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = ProductDetailsResponse.fromJson(body);
      return parsedResponse;
    } else {
      throw Exception('Failed to get product details');
    }
  }

  Future<DeliveryCostResponse> getDeliveryCost(
      int shopId, double lat, double lng) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/shops/$shopId/delivery-cost?lat=$lat&lng=$lng",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = DeliveryCostResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get product details');
    }
  }

  Future<AddItemToCartResponse> addItemToCart(int productId, int shopId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.post(
        "https://mops.swaxis.com/api/v1/cart/goods/$productId",
        body: {'shopSID': '$shopId'},
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = AddItemToCartResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to add item to cart');
    }
  }

  Future<AddItemToCartResponse> removeItemFromCart(int productId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.delete(
        "https://mops.swaxis.com/api/v1/cart/goods/$productId",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = AddItemToCartResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to remove item from cart');
    }
  }

  Future<DeleteFromCartResponse> removeAllItemsFromCart(int productId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.delete(
        "https://mops.swaxis.com/api/v1/cart/goods/$productId/all",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = DeleteFromCartResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to remove all items from cart');
    }
  }

  Future<DeleteFromCartResponse> clearCart() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.delete(
        "https://mops.swaxis.com/api/v1/cart",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = DeleteFromCartResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to clear cart');
    }
  }

  Future<CartResponse> getCart() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/cart",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = CartResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get cart');
    }
  }

  Future<OrderConfirmResponse> confirmOrder(orderDetails) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.post(
        "https://mops.swaxis.com/api/v1/orders/confirm",
        headers: {'Authorization': 'Bearer $accessToken'},
        body: orderDetails);
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = OrderConfirmResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to confirm order');
    }
  }

  Future<EmptyResponse> cancelOrder(int orderId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.put(
        "https://mops.swaxis.com/api/v1/orders/$orderId/cancel",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = EmptyResponse.fromJsonMap(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to cancel order');
    }
  }

  Future<PaymentCardsResponse> getPaymentCards() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/cards/own",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = PaymentCardsResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get cards');
    }
  }

  Future<OrderPaymentResponse> orderPayment(
      int orderId, String paymentCardId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.put(
        "https://mops.swaxis.com/api/v1/orders/$orderId/pay",
        headers: {'Authorization': 'Bearer $accessToken'},
        body: {"cardSID": paymentCardId});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = OrderPaymentResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to pay order');
    }
  }

  Future<AddPaymentCardResponse> addPaymentCard(
      String number, String valid, String cvv) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.post(
        "https://mops.swaxis.com/api/v1/cards",
        headers: {'Authorization': 'Bearer $accessToken'},
        body: {"number": number, "valid": valid, "cvv": cvv});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = AddPaymentCardResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to add card');
    }
  }

  Future<AddPaymentCardResponse> removePaymentCard(int cardId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.delete(
        "https://mops.swaxis.com/api/v1/cards/$cardId",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = AddPaymentCardResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to remove card');
    }
  }

  Future<OrdersResponse> getOrders() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/orders/own",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = OrdersResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get orders');
    }
  }

  Future<OrderDetailsResponse> getOrderDetails(int orderId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/orders/$orderId/goods",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = OrderDetailsResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get order details');
    }
  }

  Future<PolicyResponse> getOfferText() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/contract-offer",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = PolicyResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get offer text');
    }
  }

  Future<PolicyResponse> getPrivacyText() async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/privacy-policy",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = PolicyResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get privacy text');
    }
  }

  Future<DiscountsResponse> getProductSpecials(int shopId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/discounts/goods",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = DiscountsResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to get discounts');
    }
  }

  Future<SearchResponse> searchProducts(int shopId, String searchPhrase) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/shops/$shopId/goods/search?search=$searchPhrase",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = SearchResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to search');
    }
  }

  Future<DiscountProductsResponse> getDiscountProducts(int discountId) async {
    await handleExpiredTokens();

    final accessToken = await localStorage.getAccessToken();
    http.Response response = await http.get(
        "https://mops.swaxis.com/api/v1/discounts/$discountId/goods",
        headers: {'Authorization': 'Bearer $accessToken'});
    final body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      final parsedResponse = DiscountProductsResponse.fromJson(body);
      if (parsedResponse.success) {
        return parsedResponse;
      } else {
        throw WebException(parseError(response));
      }
    } else {
      throw Exception('Failed to search');
    }
  }
}
