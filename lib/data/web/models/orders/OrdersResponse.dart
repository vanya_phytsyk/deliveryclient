class OrdersResponse {
  num total;
  List<OrdersResponseRecord> records;
  bool success;

  OrdersResponse({this.total, this.records, this.success});

  OrdersResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['records'] != null) {
      records = new List<OrdersResponseRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new OrdersResponseRecord.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class OrdersResponseRecord {
  num goodsCount;
  num goodsPrice;
  int sid;
  String status;
  num deliveryCost;
  String shopLogo;

  OrdersResponseRecord(
      {this.goodsCount,
      this.goodsPrice,
      this.sid,
      this.status,
      this.deliveryCost,
      this.shopLogo});

  OrdersResponseRecord.fromJson(Map<String, dynamic> json) {
    goodsCount = json['goodsCount'];
    goodsPrice = json['goodsPrice'];
    sid = json['sid'];
    status = json['status'];
    deliveryCost = json['deliveryCost'];
    shopLogo = json['shopLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['goodsCount'] = this.goodsCount;
    data['goodsPrice'] = this.goodsPrice;
    data['sid'] = this.sid;
    data['status'] = this.status;
    data['shopLogo'] = this.shopLogo;
    return data;
  }
}
