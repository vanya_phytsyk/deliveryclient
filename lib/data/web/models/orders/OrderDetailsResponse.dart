class OrderDetailsResponse {
  OrderDetailsResponseResult result;
  bool success;

  OrderDetailsResponse({this.result, this.success});

  OrderDetailsResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'] != null
        ? new OrderDetailsResponseResult.fromJson(json['result'])
        : null;
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    data['success'] = this.success;
    return data;
  }
}

class OrderDetailsResponseResult {
  OrderDetailsResponseResultCart cart;
  String status;

  OrderDetailsResponseResult({this.cart, this.status});

  OrderDetailsResponseResult.fromJson(Map<String, dynamic> json) {
    cart = json['cart'] != null
        ? new OrderDetailsResponseResultCart.fromJson(json['cart'])
        : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cart != null) {
      data['cart'] = this.cart.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class OrderDetailsResponseResultCart {
  int total;
  OrderDetailsResponseResultCartShop shop;
  List<OrderDetailsResponseResultCartRecord> records;
  num totalPrice;

  OrderDetailsResponseResultCart(
      {this.total, this.shop, this.records, this.totalPrice});

  OrderDetailsResponseResultCart.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    shop = json['shop'] != null
        ? new OrderDetailsResponseResultCartShop.fromJson(json['shop'])
        : null;
    if (json['records'] != null) {
      records = new List<OrderDetailsResponseResultCartRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new OrderDetailsResponseResultCartRecord.fromJson(v));
      });
    }
    totalPrice = json['totalPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.shop != null) {
      data['shop'] = this.shop.toJson();
    }
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['totalPrice'] = this.totalPrice;
    return data;
  }
}

class OrderDetailsResponseResultCartShop {
  String name;
  num deliveryPrice;
  int sid;

  OrderDetailsResponseResultCartShop({this.name, this.deliveryPrice, this.sid});

  OrderDetailsResponseResultCartShop.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    deliveryPrice = json['deliveryPrice'];
    sid = json['sid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['deliveryPrice'] = this.deliveryPrice;
    data['sid'] = this.sid;
    return data;
  }
}

class OrderDetailsResponseResultCartRecord {
  num goodPrice;
  String goodUnit;
  num goodCnt;
  String goodImg;
  num goodUnitStep;
  num goodDiscountPrice;
  int goodId;
  String goodName;

  OrderDetailsResponseResultCartRecord(
      {this.goodPrice,
      this.goodUnit,
      this.goodCnt,
      this.goodImg,
      this.goodUnitStep,
      this.goodDiscountPrice,
      this.goodId,
      this.goodName});

  OrderDetailsResponseResultCartRecord.fromJson(Map<String, dynamic> json) {
    goodPrice = json['good_price'];
    goodUnit = json['good_unit'];
    goodCnt = json['good_cnt'];
    goodImg = json['good_img'];
    goodUnitStep = json['good_unit_step'];
    goodDiscountPrice = json['good_discount_price'];
    goodId = json['good_id'];
    goodName = json['good_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_price'] = this.goodPrice;
    data['good_unit'] = this.goodUnit;
    data['good_cnt'] = this.goodCnt;
    data['good_img'] = this.goodImg;
    data['good_unit_step'] = this.goodUnitStep;
    data['good_discount_price'] = this.goodDiscountPrice;
    data['good_id'] = this.goodId;
    data['good_name'] = this.goodName;
    return data;
  }
}
