class CartResponse {
	num total;
	CartResponseShop shop;
	List<CartResponseRecord> records;
	num totalPrice;
	bool success;

	CartResponse({this.total, this.shop, this.records, this.totalPrice, this.success});

	CartResponse.fromJson(Map<String, dynamic> json) {
		total = json['total'];
		shop = json['shop'] != null ? new CartResponseShop.fromJson(json['shop']) : null;
		if (json['records'] != null) {
			records = new List<CartResponseRecord>();(json['records'] as List).forEach((v) { records.add(new CartResponseRecord.fromJson(v)); });
		}
		totalPrice = json['totalPrice'];
		success = json['success'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total'] = this.total;
		if (this.shop != null) {
      data['shop'] = this.shop.toJson();
    }
		if (this.records != null) {
      data['records'] =  this.records.map((v) => v.toJson()).toList();
    }
		data['totalPrice'] = this.totalPrice;
		data['success'] = this.success;
		return data;
	}
}

class CartResponseShop {
	String name;
	num deliveryPrice;
	int sid;

	CartResponseShop({this.name, this.deliveryPrice, this.sid});

	CartResponseShop.fromJson(Map<String, dynamic> json) {
		name = json['name'];
		deliveryPrice = json['deliveryPrice'];
		sid = json['sid'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['name'] = this.name;
		data['deliveryPrice'] = this.deliveryPrice;
		data['sid'] = this.sid;
		return data;
	}
}

class CartResponseRecord {
	num goodPrice;
	String goodUnit;
	num goodCnt;
	String goodImg;
	num goodUnitStep;
	num goodDiscountPrice;
	int goodId;
	String goodName;

	CartResponseRecord({this.goodPrice, this.goodUnit, this.goodCnt, this.goodImg, this.goodUnitStep, this.goodDiscountPrice, this.goodId, this.goodName});

	CartResponseRecord.fromJson(Map<String, dynamic> json) {
		goodPrice = json['good_price'];
		goodUnit = json['good_unit'];
		goodCnt = json['good_cnt'];
		goodImg = json['good_img'];
		goodUnitStep = json['good_unit_step'];
		goodDiscountPrice = json['good_discount_price'];
		goodId = json['good_id'];
		goodName = json['good_name'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['good_price'] = this.goodPrice;
		data['good_unit'] = this.goodUnit;
		data['good_cnt'] = this.goodCnt;
		data['good_img'] = this.goodImg;
		data['good_unit_step'] = this.goodUnitStep;
		data['good_discount_price'] = this.goodDiscountPrice;
		data['good_id'] = this.goodId;
		data['good_name'] = this.goodName;
		return data;
	}
}
