class DeliveryCostResponse {
	DeliveryCostResponseResult result;
	bool success;

	DeliveryCostResponse({this.result, this.success});

	DeliveryCostResponse.fromJson(Map<String, dynamic> json) {
		result = json['result'] != null ? new DeliveryCostResponseResult.fromJson(json['result']) : null;
		success = json['success'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.result != null) {
      data['result'] = this.result.toJson();
    }
		data['success'] = this.success;
		return data;
	}
}

class DeliveryCostResponseResult {
	num tariffInArea;
	num overDistance;
	num deliveryCost;

	DeliveryCostResponseResult({this.tariffInArea, this.overDistance, this.deliveryCost});

	DeliveryCostResponseResult.fromJson(Map<String, dynamic> json) {
		tariffInArea = json['tariffInArea'];
		overDistance = json['overDistance'];
		deliveryCost = json['deliveryCost'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['tariffInArea'] = this.tariffInArea;
		data['overDistance'] = this.overDistance;
		data['deliveryCost'] = this.deliveryCost;
		return data;
	}
}
