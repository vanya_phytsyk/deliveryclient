class AddItemToCartResponse {
  AddItemToCartResponseResult result;
  num totalPrice;
  bool success;

  AddItemToCartResponse({this.result, this.totalPrice, this.success});

  AddItemToCartResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'] != null
        ? new AddItemToCartResponseResult.fromJson(json['result'])
        : null;
    totalPrice = json['totalPrice'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    data['totalPrice'] = this.totalPrice;
    data['success'] = this.success;
    return data;
  }
}

class AddItemToCartResponseResult {
  num goodPrice;
  String goodUnit;
  num goodCnt;
  num goodUnitStep;
  num goodDiscountPrice;
  int goodId;
  String goodName;

  AddItemToCartResponseResult(
      {this.goodPrice,
      this.goodUnit,
      this.goodCnt,
      this.goodUnitStep,
      this.goodDiscountPrice,
      this.goodId,
      this.goodName});

  AddItemToCartResponseResult.fromJson(Map<String, dynamic> json) {
    goodPrice = json['good_price'];
    goodUnit = json['good_unit'];
    goodCnt = json['good_cnt'];
    goodUnitStep = json['good_unit_step'];
    goodDiscountPrice = json['good_discount_price'];
    goodId = json['good_id'];
    goodName = json['good_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_price'] = this.goodPrice;
    data['good_unit'] = this.goodUnit;
    data['good_cnt'] = this.goodCnt;
    data['good_unit_step'] = this.goodUnitStep;
    data['good_discount_price'] = this.goodDiscountPrice;
    data['good_id'] = this.goodId;
    data['good_name'] = this.goodName;
    return data;
  }
}
