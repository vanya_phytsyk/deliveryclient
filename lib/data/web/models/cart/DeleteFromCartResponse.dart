class DeleteFromCartResponse {
  num totalPrice;
  bool success;

  DeleteFromCartResponse({this.totalPrice, this.success});

  DeleteFromCartResponse.fromJson(Map<String, dynamic> json) {
    totalPrice = json['totalPrice'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalPrice'] = this.totalPrice;
    data['success'] = this.success;
    return data;
  }
}

