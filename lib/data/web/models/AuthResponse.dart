
class AuthResponse {

  final bool success;
  final String refresh;
  final String access;
  final int expire;

	AuthResponse.fromJsonMap(Map<String, dynamic> map):
		success = map["success"],
		refresh = map["refresh"],
		access = map["access"],
		expire = map["expire"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['success'] = success;
		data['refresh'] = refresh;
		data['access'] = access;
		data['expire'] = expire;
		return data;
	}
}
