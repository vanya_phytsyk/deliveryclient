
class EmptyResponse {

  final bool success;
  final String result;

	EmptyResponse.fromJsonMap(Map<String, dynamic> map): 
		success = map["success"],
		result = map["result"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['success'] = success;
		data['result'] = result;
		return data;
	}
}
