class DiscountProductsResponse {
	num total;
	List<DiscountProductsResponseRecord> records;
	num totalPrice;
	bool success;

	DiscountProductsResponse({this.total, this.records, this.totalPrice, this.success});

	DiscountProductsResponse.fromJson(Map<String, dynamic> json) {
		total = json['total'];
		if (json['records'] != null) {
			records = new List<DiscountProductsResponseRecord>();(json['records'] as List).forEach((v) { records.add(new DiscountProductsResponseRecord.fromJson(v)); });
		}
		totalPrice = json['totalPrice'];
		success = json['success'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total'] = this.total;
		if (this.records != null) {
      data['records'] =  this.records.map((v) => v.toJson()).toList();
    }
		data['totalPrice'] = this.totalPrice;
		data['success'] = this.success;
		return data;
	}
}

class DiscountProductsResponseRecord {
	num goodPrice;
	String goodUnit;
	num goodCnt;
	String goodImg;
	num goodUnitStep;
	num goodDiscountPrice;
	int goodId;
	String goodName;

	DiscountProductsResponseRecord({this.goodPrice, this.goodUnit, this.goodCnt, this.goodImg, this.goodUnitStep, this.goodDiscountPrice, this.goodId, this.goodName});

	DiscountProductsResponseRecord.fromJson(Map<String, dynamic> json) {
		goodPrice = json['good_price'];
		goodUnit = json['good_unit'];
		goodCnt = json['good_cnt'];
		goodImg = json['good_img'];
		goodUnitStep = json['good_unit_step'];
		goodDiscountPrice = json['good_discount_price'];
		goodId = json['good_id'];
		goodName = json['good_name'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['good_price'] = this.goodPrice;
		data['good_unit'] = this.goodUnit;
		data['good_cnt'] = this.goodCnt;
		data['good_img'] = this.goodImg;
		data['good_unit_step'] = this.goodUnitStep;
		data['good_discount_price'] = this.goodDiscountPrice;
		data['good_id'] = this.goodId;
		data['good_name'] = this.goodName;
		return data;
	}
}
