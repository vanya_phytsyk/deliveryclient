class DiscountsResponse {
  int total;
  List<DiscountsResponseRecord> records;
  bool success;

  DiscountsResponse({this.total, this.records, this.success});

  DiscountsResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['records'] != null) {
      records = new List<DiscountsResponseRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new DiscountsResponseRecord.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class DiscountsResponseRecord {
  String name;
  num discount;
  String description;
  List<DiscountsResponseRecordsGood> goods;
  int sid;

  DiscountsResponseRecord(
      {this.name, this.discount, this.description, this.goods, this.sid});

  DiscountsResponseRecord.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    discount = json['discount'];
    description = json['description'];
    if (json['goods'] != null) {
      goods = new List<DiscountsResponseRecordsGood>();
      (json['goods'] as List).forEach((v) {
        goods.add(new DiscountsResponseRecordsGood.fromJson(v));
      });
    }
    sid = json['sid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['discount'] = this.discount;
    data['description'] = this.description;
    if (this.goods != null) {
      data['goods'] = this.goods.map((v) => v.toJson()).toList();
    }
    data['sid'] = this.sid;
    return data;
  }
}

class DiscountsResponseRecordsGood {
  num goodPrice;
  String goodUnit;
  String goodImg;
  num goodUnitStep;
  num goodDiscountPrice;
  int goodId;
  String goodName;

  DiscountsResponseRecordsGood(
      {this.goodPrice,
      this.goodUnit,
      this.goodImg,
      this.goodUnitStep,
      this.goodDiscountPrice,
      this.goodId,
      this.goodName});

  DiscountsResponseRecordsGood.fromJson(Map<String, dynamic> json) {
    goodPrice = json['good_price'];
    goodUnit = json['good_unit'];
    goodImg = json['good_img'];
    goodUnitStep = json['good_unit_step'];
    goodDiscountPrice = json['good_discount_price'];
    goodId = json['good_id'];
    goodName = json['good_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_price'] = this.goodPrice;
    data['good_unit'] = this.goodUnit;
    data['good_img'] = this.goodImg;
    data['good_unit_step'] = this.goodUnitStep;
    data['good_discount_price'] = this.goodDiscountPrice;
    data['good_id'] = this.goodId;
    data['good_name'] = this.goodName;
    return data;
  }
}
