class ProductDetailsResponse {
  ProductDetailsResult result;
  bool success;

  ProductDetailsResponse({this.result, this.success});

  ProductDetailsResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'] != null
        ? new ProductDetailsResult.fromJson(json['result'])
        : null;
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    data['success'] = this.success;
    return data;
  }
}

class ProductDetailsResult {
  String goodImg;
  num goodDiscountPrice;
  List<Null> goodPrices;
  String brandName;
  int goodId;
  int brandId;
  List<Null> goodReviews;
  List<ProductDetailsResultGoodImage> goodImages;
  List<Null> discounts;
  List<ProductDetailsResultGoodAttr> goodAttrs;
  int goodReviewsCount;
  List<ProductDetailsResultCategory> categories;
  String goodName;
  int goodRating;

  ProductDetailsResult(
      {this.goodImg,
      this.goodDiscountPrice,
      this.goodPrices,
      this.brandName,
      this.goodId,
      this.brandId,
      this.goodReviews,
      this.goodImages,
      this.discounts,
      this.goodAttrs,
      this.goodReviewsCount,
      this.categories,
      this.goodName,
      this.goodRating});

  ProductDetailsResult.fromJson(Map<String, dynamic> json) {
    goodImg = json['good_img'];
    goodDiscountPrice = json['good_discount_price'];
    if (json['good_prices'] != null) {
      goodPrices = new List<Null>();
    }
    brandName = json['brand_name'];
    goodId = json['good_id'];
    brandId = json['brand_id'];
    if (json['good_reviews'] != null) {
      goodReviews = new List<Null>();
    }
    if (json['good_images'] != null) {
      goodImages = new List<ProductDetailsResultGoodImage>();
      (json['good_images'] as List).forEach((v) {
        goodImages.add(new ProductDetailsResultGoodImage.fromJson(v));
      });
    }
    if (json['discounts'] != null) {
      discounts = new List<Null>();
    }
    if (json['good_attrs'] != null) {
      goodAttrs = new List<ProductDetailsResultGoodAttr>();
      (json['good_attrs'] as List).forEach((v) {
        goodAttrs.add(new ProductDetailsResultGoodAttr.fromJson(v));
      });
    }
    goodReviewsCount = json['good_reviews_count'];
    if (json['categories'] != null) {
      categories = new List<ProductDetailsResultCategory>();
      (json['categories'] as List).forEach((v) {
        categories.add(new ProductDetailsResultCategory.fromJson(v));
      });
    }
    goodName = json['good_name'];
    goodRating = json['good_rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_img'] = this.goodImg;
    data['good_discount_price'] = this.goodDiscountPrice;
    if (this.goodPrices != null) {
      data['good_prices'] = [];
    }
    data['brand_name'] = this.brandName;
    data['good_id'] = this.goodId;
    data['brand_id'] = this.brandId;
    if (this.goodReviews != null) {
      data['good_reviews'] = [];
    }
    if (this.goodImages != null) {
      data['good_images'] = this.goodImages.map((v) => v.toJson()).toList();
    }
    if (this.discounts != null) {
      data['discounts'] = [];
    }
    if (this.goodAttrs != null) {
      data['good_attrs'] = this.goodAttrs.map((v) => v.toJson()).toList();
    }
    data['good_reviews_count'] = this.goodReviewsCount;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['good_name'] = this.goodName;
    data['good_rating'] = this.goodRating;
    return data;
  }
}

class ProductDetailsResultGoodImage {
  String photoType;
  String photoUrl;
  String photoDate;
  String barcode;

  ProductDetailsResultGoodImage(
      {this.photoType, this.photoUrl, this.photoDate, this.barcode});

  ProductDetailsResultGoodImage.fromJson(Map<String, dynamic> json) {
    photoType = json['photo_type'];
    photoUrl = json['photo_url'];
    photoDate = json['photo_date'];
    barcode = json['barcode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photo_type'] = this.photoType;
    data['photo_url'] = this.photoUrl;
    data['photo_date'] = this.photoDate;
    data['barcode'] = this.barcode;
    return data;
  }
}

class ProductDetailsResultGoodAttr {
  String attrValueType;
  String attrName;
  int valueId;
  String attrValue;
  int attrId;

  ProductDetailsResultGoodAttr(
      {this.attrValueType,
      this.attrName,
      this.valueId,
      this.attrValue,
      this.attrId});

  ProductDetailsResultGoodAttr.fromJson(Map<String, dynamic> json) {
    attrValueType = json['attr_value_type'];
    attrName = json['attr_name'];
    valueId = json['value_id'];
    attrValue = json['attr_value'];
    attrId = json['attr_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['attr_value_type'] = this.attrValueType;
    data['attr_name'] = this.attrName;
    data['value_id'] = this.valueId;
    data['attr_value'] = this.attrValue;
    data['attr_id'] = this.attrId;
    return data;
  }
}

class ProductDetailsResultCategory {
  String catName;
  int catId;

  ProductDetailsResultCategory({this.catName, this.catId});

  ProductDetailsResultCategory.fromJson(Map<String, dynamic> json) {
    catName = json['cat_name'];
    catId = json['cat_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cat_name'] = this.catName;
    data['cat_id'] = this.catId;
    return data;
  }
}
