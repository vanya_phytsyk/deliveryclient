class ProductSpecialsResponse {
  List<ProductSpecialsResponseRecord> records;
  bool success;

  ProductSpecialsResponse({this.records, this.success});

  ProductSpecialsResponse.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      records = new List<ProductSpecialsResponseRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new ProductSpecialsResponseRecord.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class ProductSpecialsResponseRecord {
  num goodPrice;
  String goodUnit;
  String goodImg;
  num goodUnitStep;
  num goodDiscountPrice;
  int goodId;
  String goodName;

  ProductSpecialsResponseRecord(
      {this.goodPrice,
      this.goodUnit,
      this.goodImg,
      this.goodUnitStep,
      this.goodDiscountPrice,
      this.goodId,
      this.goodName});

  ProductSpecialsResponseRecord.fromJson(Map<String, dynamic> json) {
    goodPrice = json['good_price'];
    goodUnit = json['good_unit'];
    goodImg = json['good_img'];
    goodUnitStep = json['good_unit_step'];
    goodDiscountPrice = json['good_discount_price'];
    goodId = json['good_id'];
    goodName = json['good_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_price'] = this.goodPrice;
    data['good_unit'] = this.goodUnit;
    data['good_img'] = this.goodImg;
    data['good_unit_step'] = this.goodUnitStep;
    data['good_discount_price'] = this.goodDiscountPrice;
    data['good_id'] = this.goodId;
    data['good_name'] = this.goodName;
    return data;
  }
}
