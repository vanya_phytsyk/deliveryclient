class SearchResponse {
  int total;
  List<SearchResponseRecord> records;
  bool success;

  SearchResponse({this.total, this.records, this.success});

  SearchResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['records'] != null) {
      records = new List<SearchResponseRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new SearchResponseRecord.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class SearchResponseRecord {
  String goodImg;
  double goodDiscountPrice;
  List<Null> goodPrices;
  String brandName;
  int goodId;
  int brandId;
  List<Null> goodReviews;
  List<SearchResponseRecordsGoodImage> goodImages;
  List<SearchResponseRecordsDiscount> discounts;
  List<SearchResponseRecordsGoodAttr> goodAttrs;
  num goodReviewsCount;
  List<SearchResponseRecordsCategory> categories;
  String goodName;
  num goodRating;

  SearchResponseRecord(
      {this.goodImg,
      this.goodDiscountPrice,
      this.goodPrices,
      this.brandName,
      this.goodId,
      this.brandId,
      this.goodReviews,
      this.goodImages,
      this.discounts,
      this.goodAttrs,
      this.goodReviewsCount,
      this.categories,
      this.goodName,
      this.goodRating});

  SearchResponseRecord.fromJson(Map<String, dynamic> json) {
    goodImg = json['good_img'];
    goodDiscountPrice = json['good_discount_price'];
    if (json['good_prices'] != null) {
      goodPrices = new List<Null>();
    }
    brandName = json['brand_name'];
    goodId = json['good_id'];
    brandId = json['brand_id'];
    if (json['good_reviews'] != null) {
      goodReviews = new List<Null>();
    }
    if (json['good_images'] != null) {
      goodImages = new List<SearchResponseRecordsGoodImage>();
      (json['good_images'] as List).forEach((v) {
        goodImages.add(new SearchResponseRecordsGoodImage.fromJson(v));
      });
    }
    if (json['discounts'] != null) {
      discounts = new List<SearchResponseRecordsDiscount>();
      (json['discounts'] as List).forEach((v) {
        discounts.add(new SearchResponseRecordsDiscount.fromJson(v));
      });
    }
    if (json['good_attrs'] != null) {
      goodAttrs = new List<SearchResponseRecordsGoodAttr>();
      (json['good_attrs'] as List).forEach((v) {
        goodAttrs.add(new SearchResponseRecordsGoodAttr.fromJson(v));
      });
    }
    goodReviewsCount = json['good_reviews_count'];
    if (json['categories'] != null) {
      categories = new List<SearchResponseRecordsCategory>();
      (json['categories'] as List).forEach((v) {
        categories.add(new SearchResponseRecordsCategory.fromJson(v));
      });
    }
    goodName = json['good_name'];
    goodRating = json['good_rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_img'] = this.goodImg;
    data['good_discount_price'] = this.goodDiscountPrice;
    if (this.goodPrices != null) {
      data['good_prices'] = [];
    }
    data['brand_name'] = this.brandName;
    data['good_id'] = this.goodId;
    data['brand_id'] = this.brandId;
    if (this.goodReviews != null) {
      data['good_reviews'] = [];
    }
    if (this.goodImages != null) {
      data['good_images'] = this.goodImages.map((v) => v.toJson()).toList();
    }
    if (this.discounts != null) {
      data['discounts'] = this.discounts.map((v) => v.toJson()).toList();
    }
    if (this.goodAttrs != null) {
      data['good_attrs'] = this.goodAttrs.map((v) => v.toJson()).toList();
    }
    data['good_reviews_count'] = this.goodReviewsCount;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['good_name'] = this.goodName;
    data['good_rating'] = this.goodRating;
    return data;
  }
}

class SearchResponseRecordsGoodImage {
  String photoType;
  String photoUrl;
  String photoDate;
  String barcode;

  SearchResponseRecordsGoodImage(
      {this.photoType, this.photoUrl, this.photoDate, this.barcode});

  SearchResponseRecordsGoodImage.fromJson(Map<String, dynamic> json) {
    photoType = json['photo_type'];
    photoUrl = json['photo_url'];
    photoDate = json['photo_date'];
    barcode = json['barcode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photo_type'] = this.photoType;
    data['photo_url'] = this.photoUrl;
    data['photo_date'] = this.photoDate;
    data['barcode'] = this.barcode;
    return data;
  }
}

class SearchResponseRecordsDiscount {
  String name;
  num discount;
  String description;
  int sid;

  SearchResponseRecordsDiscount(
      {this.name, this.discount, this.description, this.sid});

  SearchResponseRecordsDiscount.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    discount = json['discount'];
    description = json['description'];
    sid = json['sid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['discount'] = this.discount;
    data['description'] = this.description;
    data['sid'] = this.sid;
    return data;
  }
}

class SearchResponseRecordsGoodAttr {
  String attrValueType;
  String attrName;
  int valueId;
  String attrValue;
  int attrId;

  SearchResponseRecordsGoodAttr(
      {this.attrValueType,
      this.attrName,
      this.valueId,
      this.attrValue,
      this.attrId});

  SearchResponseRecordsGoodAttr.fromJson(Map<String, dynamic> json) {
    attrValueType = json['attr_value_type'];
    attrName = json['attr_name'];
    valueId = json['value_id'];
    attrValue = json['attr_value'];
    attrId = json['attr_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['attr_value_type'] = this.attrValueType;
    data['attr_name'] = this.attrName;
    data['value_id'] = this.valueId;
    data['attr_value'] = this.attrValue;
    data['attr_id'] = this.attrId;
    return data;
  }
}

class SearchResponseRecordsCategory {
  String catName;
  int catId;

  SearchResponseRecordsCategory({this.catName, this.catId});

  SearchResponseRecordsCategory.fromJson(Map<String, dynamic> json) {
    catName = json['cat_name'];
    catId = json['cat_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cat_name'] = this.catName;
    data['cat_id'] = this.catId;
    return data;
  }
}
