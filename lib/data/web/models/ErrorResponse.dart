
class ErrorResponse {

  final bool success;
  final String code;
  final String msg;

	ErrorResponse.fromJsonMap(Map<String, dynamic> map): 
		success = map["success"],
		code = map["code"],
		msg = map["msg"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['success'] = success;
		data['code'] = code;
		data['msg'] = msg;
		return data;
	}
}
