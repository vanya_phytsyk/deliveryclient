
class Tariff {

  final int inArea;
  final int outArea;

	Tariff.fromJsonMap(Map<String, dynamic> map): 
		inArea = map["inArea"],
		outArea = map["outArea"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['inArea'] = inArea;
		data['outArea'] = outArea;
		return data;
	}
}
