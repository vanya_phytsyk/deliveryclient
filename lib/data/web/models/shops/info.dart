
class Info {

  final String logoUrl;

	Info.fromJsonMap(Map<String, dynamic> map):
				logoUrl = map["logoUrl"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['logoUrl'] = logoUrl;
		return data;
	}
}
