import 'package:delivery_client/data/web/models/shops/info.dart';
import 'package:delivery_client/data/web/models/shops/contacts.dart';
import 'package:delivery_client/data/web/models/shops/tariff.dart';

class Records {

  final int sid;
  final String name;
  final Info info;
  final Contacts contacts;
  final Tariff tariff;

	Records.fromJsonMap(Map<String, dynamic> map): 
		sid = map["sid"],
		name = map["name"],
		info = Info.fromJsonMap(map["info"]),
		contacts = Contacts.fromJsonMap(map["contacts"]),
		tariff = Tariff.fromJsonMap(map["tariff"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['sid'] = sid;
		data['name'] = name;
		data['info'] = info == null ? null : info.toJson();
		data['contacts'] = contacts == null ? null : contacts.toJson();
		data['tariff'] = tariff == null ? null : tariff.toJson();
		return data;
	}
}
