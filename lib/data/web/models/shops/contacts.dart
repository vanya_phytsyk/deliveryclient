
class Contacts {

  final String city;
  final Object site;
  final String email;
  final String phone;
  final String address;

	Contacts.fromJsonMap(Map<String, dynamic> map): 
		city = map["city"],
		site = map["site"],
		email = map["email"],
		phone = map["phone"],
		address = map["address"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['city'] = city;
		data['site'] = site;
		data['email'] = email;
		data['phone'] = phone;
		data['address'] = address;
		return data;
	}
}
