
class Records {

  final int cat_id;
  final String cat_name;
  final int cat_parent_id;
  final int cat_level;

	Records.fromJsonMap(Map<String, dynamic> map): 
		cat_id = map["cat_id"],
		cat_name = map["cat_name"],
		cat_parent_id = map["cat_parent_id"],
		cat_level = map["cat_level"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['cat_id'] = cat_id;
		data['cat_name'] = cat_name;
		data['cat_parent_id'] = cat_parent_id;
		data['cat_level'] = cat_level;
		return data;
	}
}
