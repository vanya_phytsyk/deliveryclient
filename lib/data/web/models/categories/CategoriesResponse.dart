import 'package:delivery_client/data/web/models/categories/records.dart';

class CategoriesResponse {

  final bool success;
  final List<Records> records;
  final int total;

	CategoriesResponse.fromJsonMap(Map<String, dynamic> map): 
		success = map["success"],
		records = List<Records>.from(map["records"].map((it) => Records.fromJsonMap(it))),
		total = map["total"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['success'] = success;
		data['records'] = records != null ? 
			this.records.map((v) => v.toJson()).toList()
			: null;
		data['total'] = total;
		return data;
	}
}
