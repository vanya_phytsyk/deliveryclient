class CategoryProductsResponse {
  int total;
  List<CategoryProductsResponseRecord> records;
  bool success;

  CategoryProductsResponse({this.total, this.records, this.success});

  CategoryProductsResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['records'] != null) {
      records = new List<CategoryProductsResponseRecord>();
      (json['records'] as List).forEach((v) {
        records.add(new CategoryProductsResponseRecord.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class CategoryProductsResponseRecord {
  String goodPrice;
  String goodImg;
  int goodId;
  String goodName;

  CategoryProductsResponseRecord(
      {this.goodPrice, this.goodImg, this.goodId, this.goodName});

  CategoryProductsResponseRecord.fromJson(Map<String, dynamic> json) {
    goodPrice = json['good_price'];
    goodImg = json['good_img'];
    goodId = json['good_id'];
    goodName = json['good_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['good_price'] = this.goodPrice;
    data['good_img'] = this.goodImg;
    data['good_id'] = this.goodId;
    data['good_name'] = this.goodName;
    return data;
  }
}
