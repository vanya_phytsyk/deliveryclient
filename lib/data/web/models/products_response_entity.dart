class ProductsResponseEntity {
	int total;
	List<ProductsResponseRecord> records;
	bool success;

	ProductsResponseEntity({this.total, this.records, this.success});

	ProductsResponseEntity.fromJson(Map<String, dynamic> json) {
		total = json['total'];
		if (json['records'] != null) {
			records = new List<ProductsResponseRecord>();(json['records'] as List).forEach((v) { records.add(new ProductsResponseRecord.fromJson(v)); });
		}
		success = json['success'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['total'] = this.total;
		if (this.records != null) {
      data['records'] =  this.records.map((v) => v.toJson()).toList();
    }
		data['success'] = this.success;
		return data;
	}
}

class ProductsResponseRecord {
	int goodPrice;
	String goodUnit;
	String goodImg;
	int goodUnitStep;
	int goodId;
	String goodName;

	ProductsResponseRecord({this.goodPrice, this.goodUnit, this.goodImg, this.goodUnitStep, this.goodId, this.goodName});

	ProductsResponseRecord.fromJson(Map<String, dynamic> json) {
		goodPrice = json['good_price'];
		goodUnit = json['good_unit'];
		goodImg = json['good_img'];
		goodUnitStep = json['good_unit_step'];
		goodId = json['good_id'];
		goodName = json['good_name'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['good_price'] = this.goodPrice;
		data['good_unit'] = this.goodUnit;
		data['good_img'] = this.goodImg;
		data['good_unit_step'] = this.goodUnitStep;
		data['good_id'] = this.goodId;
		data['good_name'] = this.goodName;
		return data;
	}
}
