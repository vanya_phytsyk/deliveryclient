
class AddressRequest {

  final String address;
  final double lat;
  final double lng;

	AddressRequest.fromJsonMap(Map<String, dynamic> map): 
		address = map["address"],
		lat = map["lat"],
		lng = map["lng"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, String>();
		data['address'] = address.toString();
		data['lat'] = lat.toString();
		data['lng'] = lng.toString();
		return data;
	}
}
