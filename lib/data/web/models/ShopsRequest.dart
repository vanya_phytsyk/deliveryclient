
class ShopsRequest {

  final int limit;
  final int offset;

	ShopsRequest.fromJsonMap(Map<String, dynamic> map): 
		limit = map["limit"],
		offset = map["offset"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['limit'] = limit;
		data['offset'] = offset;
		return data;
	}
}
