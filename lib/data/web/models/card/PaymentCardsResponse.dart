class PaymentCardsResponse {
  int total;
  List<PaymantCardsResponseRecords> records;
  bool success;

  PaymentCardsResponse({this.total, this.records, this.success});

  PaymentCardsResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['records'] != null) {
      records = new List<PaymantCardsResponseRecords>();
      (json['records'] as List).forEach((v) {
        records.add(new PaymantCardsResponseRecords.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class PaymantCardsResponseRecords {
  String number;
  int sid;

  PaymantCardsResponseRecords({this.number, this.sid});

  PaymantCardsResponseRecords.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    sid = json['sid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this.number;
    data['sid'] = this.sid;
    return data;
  }
}
