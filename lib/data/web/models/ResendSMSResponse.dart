class ResendSMSResponse {
  int resendActivation;
  bool success;

  ResendSMSResponse({this.resendActivation, this.success});

  ResendSMSResponse.fromJson(Map<String, dynamic> json) {
    resendActivation = json['resendActivation'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resendActivation'] = this.resendActivation;
    data['success'] = this.success;
    return data;
  }
}
