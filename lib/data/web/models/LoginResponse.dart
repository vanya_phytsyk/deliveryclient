class LoginResponse {
  final bool success;
  final String confirmCheckCode;
  final String smsCheckToken;
  final int resendActivation;

  LoginResponse.fromJsonMap(Map<String, dynamic> map)
      : success = map["success"],
        confirmCheckCode = map["confirmCheckCode"],
        smsCheckToken = map["checkToken"],
        resendActivation = map["resendActivation"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = success;
    data['confirmCheckCode'] = confirmCheckCode;
    data['checkToken'] = smsCheckToken;
    data['resendActivation'] = resendActivation;
    return data;
  }
}
