import 'models/ErrorResponse.dart';

class WebException implements Exception {
  final ErrorResponse errorResponse;

  WebException(this.errorResponse);

  @override
  String toString() {
    return errorResponse.msg;
  }
}
