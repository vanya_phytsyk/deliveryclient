import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  final Future<SharedPreferences> _sharedPreferences =
      SharedPreferences.getInstance();

  saveAccessToken(String token) async {
    (await _sharedPreferences).setString('accessToken', token);
  }

  saveTokenExpirationDate(int expirationDate) async {
    (await _sharedPreferences).setInt('tokenExpirationDate', expirationDate);
  }

  saveRefreshToken(String token) async {
    (await _sharedPreferences).setString('refreshToken', token);
  }

  saveLocationLon(double lon) async {
    (await _sharedPreferences).setDouble('lon', lon);
  }

  saveLocationLat(double lat) async {
    (await _sharedPreferences).setDouble('lat', lat);
  }

  saveAddress(String address) async {
    (await _sharedPreferences).setString('address', address);
  }

  saveProfileId(String profileId) async {
    (await _sharedPreferences).setString('profileId', profileId);
  }

  Future<String> getAccessToken() async {
    return (await _sharedPreferences).getString('accessToken');
  }

  Future<String> getRefreshToken() async {
    return (await _sharedPreferences).getString('refreshToken');
  }

  Future<int> getTokenExpirationDate() async {
    return (await _sharedPreferences).getInt('tokenExpirationDate');
  }

  Future<double> getLocationLon() async {
    return (await _sharedPreferences).getDouble('lon');
  }

  Future<double> getLocationLat() async {
    return (await _sharedPreferences).getDouble('lat');
  }

  Future<String> getAddress() async {
    return (await _sharedPreferences).getString('address');
  }

  clear() async {
    (await _sharedPreferences).clear();
  }
}
